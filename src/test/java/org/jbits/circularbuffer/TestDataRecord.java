package org.jbits.circularbuffer;

import java.io.IOException;
import java.io.PrintWriter;

public class TestDataRecord implements MergeableStatDataRecord<Object> {
	protected boolean b;
	private double d;
	public float f;
	public byte bb;
	public short s;
	transient public int i=0;
	public long l;
	
	@Override
	public void reset() {
	}
	public double getD() {
		return d;
	}
	public void setD(double d) {
		this.d = d;
	}
	@Override
	public void dump(PrintWriter writer) throws IOException {
	}
	@Override
	public TestDataRecord copy() {
		TestDataRecord ret = new TestDataRecord();
		ret.b=b;
		ret.bb=bb;
		ret.d=d;
		ret.f=f;
		ret.i=i;
		ret.l=l;
		ret.s=s;
		return ret;
	}
	@Override
	public void put(Object value) {
	}
	@Override
	public void merge(MergeableStatDataRecord<Object> other) {
		if (other instanceof TestDataRecord) {
			TestDataRecord or = (TestDataRecord) other;
			b = or.b;
			d = (d+or.d)/2.0;
			f = (f+or.f)/2.0F;
			bb = or.bb;
			s = or.s;
			i = or.i;
			l = or.l;
		} else {
			throw new IllegalArgumentException("can merge only instance of "+TestDataRecord.class);
		}
	}
	@Override
	public double getRelativeCounterFor(Object s) {
		return 0;
	}
	@Override
	public int getAbsoluteCounterFor(Object s) {
		return 0;
	}
	
}
