package org.jbits.circularbuffer;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Path;
import java.nio.file.Paths;

import junit.framework.TestCase;

import org.jbits.time.TimeInterval;
import org.junit.Test;

public class TestCircularBufferGroup extends TestCase {

	@Test
	public void testCreateOpen() throws IOException, IllegalArgumentException, IllegalAccessException, InstantiationException, CircularBufferException, InvocationTargetException, NoSuchMethodException, SecurityException {
		Path path = Paths.get("/scratch/tmp/cb");
		long major = TimeInterval.DAY;
		long minor = 6*TimeInterval.MIN;
		long base = System.currentTimeMillis()-major-minor;
		
		CircularBufferGroup<TestDataRecord,Object> group = CircularBufferGroup.openOrCreateCircularBufferGroup(
				TestDataRecord.class, 
				path, 
				"test-buffer", 
				major, 
				minor, 
				base, 
				10, 
				4, 
				TimeInterval.valueOf("6 min"));
		assertNotNull(group);
		
		int loops = 1000000;
		
		long nanosTotal = 0;
		
		for (int counter=0;counter<loops;counter++) {
			long timestamp = getRandomTimestamp(System.currentTimeMillis()-(major/2), major/2);
			TestDataRecord record = getRandomData();
			long nanosStart = System.nanoTime();
			group.put(record,timestamp);
			nanosTotal+=(System.nanoTime()-nanosStart);
			group.flush();
		}
		
		group.flush();
		
		double totalNanos = (double) nanosTotal;
		System.out.println("Put rate 1/s: " + 1E9 * (double)loops / totalNanos);
		long now = System.currentTimeMillis();
		for (TimestampedRecord<TestDataRecord> r:group.getRecordsForInterval(base, now, now)) {
			if (r!=null && r.getRecord()!=null) {
				System.out.println(""+r.getTimestamp()+":"+r.getRecord().f);
			}
		}
	}
	
	private TestDataRecord getRandomData() {
		TestDataRecord r1 = new TestDataRecord();
		r1.b=false;
		r1.bb = (byte)(11+(int)(Math.random()*11));
		r1.setD(12.12+Math.random()*200.11);
		r1.f = 14.14f+(float)(Math.random()*400.12);
		r1.i = 15+(int)(Math.random()*57814);
		r1.l = 16L+(long)(Math.random()*2416342);
		r1.s = (short)(17+(long)(Math.random()*4536));
		return r1;
	}
	private long getRandomTimestamp(long base, long max) {
		return base+(long)(Math.random()*(max-1));
	}
//	
//	@Test
//	public void testPut() throws IOException, IllegalArgumentException, IllegalAccessException, InstantiationException, CircularBufferException {
//		Path path = Paths.get("/scratch/tmp/file1.data");
//		long major = 60*DAY;
//		long minor = MIN;
//		CircularBuffer<TestDataRecord> fd = CircularBuffer.createNewCircularBuffer(TestDataRecord.class, path, major, minor,System.currentTimeMillis());
//		fd.setExecutor(new ScheduledThreadPoolExecutor(10));
//		fd.setDirtyBufferSaveTimeout(TimeInterval.valueOf("5 sec"));
//		long base = System.currentTimeMillis();
//		long start = System.nanoTime();
//		int counter=0;
////		fd.getRecords(0, fd.getCapasity());
//		for (;System.currentTimeMillis()-base<60*SEC;counter++) {
//			TestDataRecord r = getRandomData();
//			long timestamp = getRandomTimestamp(base, major);
//			fd.putRecord(r,timestamp);
//			TimestampedRecord<TestDataRecord> r1 = fd.getRecord(timestamp);
//			assertNotNull(r1.getRecord());
//			assertEquals(r1.getRecord().bb,r.bb);
//			assertEquals(r1.getRecord().b,r.b);
//			assertEquals(r1.getRecord().f,r.f);
//			assertEquals(r1.getRecord().l,r.l);
//		}
//		long total=System.nanoTime()-start;
//		double opPerSec = 1 / ((double)total * 1E-9 / (double)counter);
//		fd.close();
//		System.out.println("op/s="+opPerSec);
//		System.out.println("flush:"+fd.getFlushCount()+" cache hits:"+fd.getCacheHits()+" cache miss:"+fd.getCacheMiss());
//	}
}
