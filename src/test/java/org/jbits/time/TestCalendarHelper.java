package org.jbits.time;

import java.util.Calendar;

import junit.framework.TestCase;

import org.junit.Test;


public class TestCalendarHelper extends TestCase {

	@Test
	public void testAdd() {
		TimeInterval v = TimeInterval.valueOf("365 days");
		assertNotNull(v);
		Calendar cal = CalendarHelper.today();
		assertNotNull(cal);
		int year = cal.get(Calendar.YEAR);
		CalendarHelper.rollForward(cal, v);
		int year2 = cal.get(Calendar.YEAR);
		assertTrue(year2-year==1);
		CalendarHelper.rollBacward(cal, v);
		int year3 = cal.get(Calendar.YEAR);
		assertTrue(year3-year==0);
	}
}
