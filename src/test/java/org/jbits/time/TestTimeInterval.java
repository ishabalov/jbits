package org.jbits.time;

import org.jbits.time.TimeInterval.Fractions;
import org.junit.Test;

import junit.framework.TestCase;


public class TestTimeInterval extends TestCase {

	@Test
	public void testFormatter() {
		TimeInterval v = TimeInterval.valueOf("10 days 11 hours 12 minutes 13.123 seconds");
		assertNotNull(v);
		String f = TimeIntervalFormat.getDefaultInstance().format(v);
		assertNotNull(f);
		Fractions ff = v.fractions();
		assertTrue(ff.getDays()==10);
		assertTrue(ff.getHours()==11);
		assertTrue(ff.getMinutes()==12);
		assertTrue(ff.getSeconds()==13);
		assertTrue(ff.getMilliseconds()==123);
		assertEquals("10d 11h 12m 13.123s",f);
		TimeInterval nv = TimeInterval.valueOf(-567123L);
		String fn = TimeIntervalFormat.getDefaultInstance().format(nv);
		assertEquals("-9m 27.123s",fn);
	}
}
