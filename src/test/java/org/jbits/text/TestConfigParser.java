package org.jbits.text;

import static org.junit.Assert.*;

import org.junit.Test;


public class TestConfigParser {
	@Test
	public void testCase1() {
		String c = 
"#\n" +
"# somecomments are here\n"+
"#\t\n\n\n"+
"root_property=1\n\n"+
"[node]   \n"+
"#x=y\n"+
"a=b\n"+
" #y=x"+
"cs=\"d\"\n"+
"dd=abc def xyz\n"+
"[node2]\n"+
"ef='abc def xyz'\n"+
"qw =   '123  \\' 456 \\\" 123456'\n"+
"as = \n" +
"'asdffgfg sdfsjdhf jshdfj as\n"+
"asjdhajd asjhda asjhd \n"+
"sdfhsdj'\n"+
"    ";
		ConfigParser p = ConfigParser.parse(c);
		assertNotNull(p);
		assertFalse(p.getEntries().isEmpty());
	}
}
