package org.jbits.math;

public interface BooleanOp {
	boolean op(double o1, double o2);
}
