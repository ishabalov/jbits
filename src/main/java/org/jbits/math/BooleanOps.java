package org.jbits.math;

public enum BooleanOps implements BooleanOp {
	Equal{
		@Override
		public boolean op(double o1, double o2) {
			return o1==o2;
		}
		
	},
	GraterThan{
		@Override
		public boolean op(double o1, double o2) {
			return o1>o2;
		}
		
	},
	LessThan{
		@Override
		public boolean op(double o1, double o2) {
			return o1<o2;
		}
		
	},
	GraterOrEqualThan{
		@Override
		public boolean op(double o1, double o2) {
			return o1>=o2;
		}
		
	},
	LessOrEqualThan{
		@Override
		public boolean op(double o1, double o2) {
			return o1<=o2;
		}
		
	};

	public boolean op(double o1, double o2) {
		throw new IllegalOperationException();
	}
}
