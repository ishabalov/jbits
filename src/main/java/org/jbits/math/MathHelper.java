package org.jbits.math;

public class MathHelper {
	
	
	private static final boolean nullSafeBooleanOp(Double a, Double b, BooleanOp op, boolean defaultValue) {
		if (a!=null && b!=null & !a.isNaN() && !b.isNaN()) {
			return op.op(a.doubleValue(), b.doubleValue());
		} else {
			return defaultValue;
		}
	}
	
	public static final boolean nullSafeGraterOrEqual(Double a, Double b) {
		return nullSafeBooleanOp(a, b, BooleanOps.GraterOrEqualThan,false);
	}
	
	public static final Double nullSafeMultiply(Double a, Double b) {
		if (a!=null && b!=null & !a.isNaN() && !b.isNaN()) {
			return a * b;
		} else {
			return Double.NaN;
		}
	}
	public static final Float nullSafeMultiply(Float a, Float b) {
		if (a!=null && b!=null & !a.isNaN() && !b.isNaN()) {
			return a * b;
		} else {
			return Float.NaN;
		}
	}
	public static final double nullSafeDoubleValue(Number n) {
		if (n!=null) {
			return n.doubleValue();
		} else {
			return Double.NaN;
		}
	}
	public static final float nullSafeFloatValue(Number n) {
		if (n!=null) {
			return n.floatValue();
		} else {
			return Float.NaN;
		}
	}
	public static final float floatMinIgnoreNaN(float... args) {
		float ret = Float.NaN;
		for (float v:args) {
			if (!Float.isNaN(v)) {
				if (!Float.isNaN(ret)) {
					ret = Math.min(ret, v);
				} else {
					ret = v;
				}
			}
		}
		return ret;
	}
	public static final double doubleMinIgnoreNaN(double... args) {
		double ret = Double.NaN;
		for (double v:args) {
			if (!Double.isNaN(v)) {
				if (!Double.isNaN(ret)) {
					ret = Math.min(ret, v);
				} else {
					ret = v;
				}
			}
		}
		return ret;
	}
	public static final float floatMaxIgnoreNaN(float... args) {
		float ret = Float.NaN;
		for (float v:args) {
			if (!Float.isNaN(v)) {
				if (!Float.isNaN(ret)) {
					ret = Math.max(ret, v);
				} else {
					ret = v;
				}
			}
		}
		return ret;
	}
	public static final double doubleMaxIgnoreNaN(double... args) {
		double ret = Double.NaN;
		for (double v:args) {
			if (!Double.isNaN(v)) {
				if (!Double.isNaN(ret)) {
					ret = Math.max(ret, v);
				} else {
					ret = v;
				}
			}
		}
		return ret;
	}
	public static final float floatSumIgnoreNaN(float... args) {
		float ret = Float.NaN;
		for (float v:args) {
			if (!Float.isNaN(v)) {
				if (!Float.isNaN(ret)) {
					ret+=v;
				} else {
					ret=v;
				}
			}
		}
		return ret;
	}
	public static final double doubleSumIgnoreNaN(double... args) {
		double ret = Double.NaN;
		for (double v:args) {
			if (!Double.isNaN(v)) {
				if (!Double.isNaN(ret)) {
					ret+=v;
				} else {
					ret=v;
				}
			}
		}
		return ret;
	}

	public static final boolean nullSafeEqualsAny(Number[] any, Number x) {
		if (x!=null && any!=null) {
			for (Number y:any) {
				if (y!=null) {
					if (x.equals(y)) {
						return true;
					}
				}
			}
			return false;
		} else {
			return false;
		}
	}

	public static final long randomLong(long from, long to) {
		double delta = Math.abs((double)to - (double)from);
		double plus = Math.floor(delta * Math.random());
		long plusL = (long)plus;
		long ret = from + plusL;
		return ret;
	}

}
