package org.jbits.math;

public class IllegalOperationException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6769449741580251283L;

	protected IllegalOperationException() {
		super();
	}

	protected IllegalOperationException(String message) {
		super(message);
	}

}
