package org.jbits.collections;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;

public final class CollectionsHelper {

	private static final <T> void addAll(Collection<T> collection, T[] array) {
		if (array!=null) {
			for (T e:array) {
				collection.add(e);
			}
		}
	}

	@SafeVarargs
	public static final <T> List<T> asList(T... array) {
		List<T> ret = new ArrayList<>();
		addAll(ret, array);
		return ret;
	}

	@SafeVarargs
	public static final <T> List<T> asListOrNull(T... array) {
		if (array!=null) {
			List<T> ret = new ArrayList<>();
			addAll(ret, array);
			return ret;
		} else {
			return null;
		}
	}

	@SafeVarargs
	public static final <T> Set<T> asSet(T... array) {
		Set<T> ret = new HashSet<>();
		addAll(ret, array);
		return ret;
	}

	@SafeVarargs
	public static final <T> Set<T> asSetOrNull(T... array) {
		if (array!=null) {
			Set<T> ret = new HashSet<>();
			addAll(ret, array);
			return ret;
		} else {
			return null;
		}
	}

	public static final <T> Set<T> asSet(Collection<T> collection) {
		Set<T> ret = new HashSet<>();
		ret.addAll(collection);
		return ret;
	}

	public static final Filter<?> FILTER_NO_NULLS = new Filter<Object>(){

		@Override
		public boolean accept(Object object) {
			return object!=null;
		}
	};

	public static final <T> Collection<T> filter(Iterable<T> source, Collection<T> destination, Filter<T> filter) {
		destination.clear();
		for (T object:source) {
			if (filter.accept(object)) {
				destination.add(object);
			}
		}
		return destination;
	}

	public static final <S,D> Collection<D> filterAndTransform(Iterable<S> source, Collection<D> destination, Filter<S> filter, Transformer<S,D> transformer) {
		destination.clear();
		for (S object:source) {
			if (filter.accept(object)) {
				destination.add(transformer.transform(object));
			}
		}
		return destination;
	}
	public static final <S,D> Collection<D> transform(Iterable<S> source, Collection<D> destination, Transformer<S,D> transformer) {
		destination.clear();
		for (S object:source) {
			destination.add(transformer.transform(object));
		}
		return destination;
	}


	public static final <T> List<T> asList(Collection<T> collection) {
		List<T> ret = new ArrayList<T>();
		ret.addAll(collection);
		return ret;
	}

	public static final <S,D> List<D> asList(Collection<S> collection, Transformer<S, D> transforner) {
		List<D> ret = new ArrayList<D>();
		for (S entry:collection) {
			ret.add(transforner.transform(entry));
		}
		return ret;
	}

	public static final <S,D> List<D> asList(Collection<S> collection, Transformer<S, D> transforner, Comparator<D> comparator) {
		List<D> ret = new ArrayList<D>();
		for (S entry:collection) {
			ret.add(transforner.transform(entry));
		}
		Collections.sort(ret,comparator);
		return ret;
	}

	public static final <T> List<T> asList(Collection<T> collection, Comparator<T> comparator) {
		List<T> ret = new ArrayList<>();
		ret.addAll(collection);
		Collections.sort(ret,comparator);
		return ret;
	}

	public static final <T> List<T> asList(Collection<T> collection, Filter<T> filter) {
		List<T> ret = new ArrayList<>();
		for (T entry:collection) {
			if (filter.accept(entry)) {
				ret.add(entry);
			}
		}
		return ret;
	}

	public static final <T> Set<T> emptySet(Class<T> T) {
		return new HashSet<>();
	}

	public static final class GroupByEntry<K,V> {
		private K key;
		private V value;
		private GroupByEntry(K key, V value) {
			this.key = key;
			this.value = value;
		}
		public K getKey() {
			return key;
		}
		public V getValue() {
			return value;
		}
	}

	public static final <K,V> List<Map.Entry<K,V>> asEntriesList(Map<K,V> map) {
		List<Map.Entry<K,V>> ret = new ArrayList<>();
		ret.addAll(map.entrySet());
		return ret;
	}
	public static final <K,V> List<Map.Entry<K,V>> asEntriesList(Map<K,V> map, Comparator<Map.Entry<K,V>> comparator) {
		List<Map.Entry<K,V>> ret = asEntriesList(map);
		Collections.sort(ret, comparator);
		return ret;
	}

	@SuppressWarnings("unchecked")
	public static final <T> T[] asArray(Class<T> clasz, Collection<T> collection) {
		T[] ret = (T[])Array.newInstance(clasz, collection.size());
		return collection.toArray(ret);
	}

	public static final <T> T[] collectionAsArray(Class<T> clasz, Collection<T> collection) {
		return asArray(clasz, collection);
	}
	
	public static final <T> T[] asArray(@SuppressWarnings("unchecked") T... arr) {
		return arr;
	}

	@SuppressWarnings("unchecked")
	public static final <S,D> D[] asTransformedArray(Class<D> clasz, Collection<S> collection, Transformer<S, D> transformer) {
		D[] ret = (D[])Array.newInstance(clasz, collection.size());
		int i=0;
		for (S e:collection) {
			ret[i] = transformer.transform(e);
			i++;
		}
		return ret;
	}

	public static final <T extends Comparable<T>> List<T> sorted(List<T> aList) {
		List<T> ret = new ArrayList<>();
		ret.addAll(aList);
		Collections.sort(ret);
		return ret;
	}

	public static final <T> List<T> sorted(List<T> aList, Comparator<T> comparator) {
		List<T> ret = new ArrayList<>();
		ret.addAll(aList);
		Collections.sort(ret,comparator);
		return ret;
	}

	public static final <T> List<T> createImmutableEmptyList(Class<T> clasz) {
		return new List<T>() {

			private Object[] emptyArray = new Object[0];

			private ListIterator<T> emptyIterator = new ListIterator<T>() {

				@Override
				public boolean hasNext() {
					return false;
				}

				@Override
				public T next() {
					return null;
				}

				@Override
				public boolean hasPrevious() {
					return false;
				}

				@Override
				public T previous() {
					return null;
				}

				@Override
				public int nextIndex() {
					return 0;
				}

				@Override
				public int previousIndex() {
					return 0;
				}

				@Override
				public void remove() {
				}

				@Override
				public void set(T e) {
				}

				@Override
				public void add(T e) {
				}
			};

			@Override
			public int size() {
				return 0;
			}

			@Override
			public boolean isEmpty() {
				return true;
			}

			@Override
			public boolean contains(Object o) {
				return false;
			}

			@Override
			public Iterator<T> iterator() {
				return emptyIterator;
			}

			@Override
			public Object[] toArray() {
				return emptyArray;
			}

			@SuppressWarnings({ "hiding", "unchecked" })
			@Override
			public <T> T[] toArray(T[] a) {
				return (T[])emptyArray;
			}

			@Override
			public boolean add(T e) {
				return false;
			}

			@Override
			public boolean remove(Object o) {
				return false;
			}

			@Override
			public boolean containsAll(Collection<?> c) {
				return false;
			}

			@Override
			public boolean addAll(Collection<? extends T> c) {
				return false;
			}

			@Override
			public boolean addAll(int index, Collection<? extends T> c) {
				return false;
			}

			@Override
			public boolean removeAll(Collection<?> c) {
				return false;
			}

			@Override
			public boolean retainAll(Collection<?> c) {
				return false;
			}

			@Override
			public void clear() {
			}

			@Override
			public T get(int index) {
				return null;
			}

			@Override
			public T set(int index, T element) {
				return null;
			}

			@Override
			public void add(int index, T element) {
			}

			@Override
			public T remove(int index) {
				return null;
			}

			@Override
			public int indexOf(Object o) {
				return -1;
			}

			@Override
			public int lastIndexOf(Object o) {
				return -1;
			}

			@Override
			public ListIterator<T> listIterator() {
				return emptyIterator;
			}

			@Override
			public ListIterator<T> listIterator(int index) {
				return emptyIterator;
			}

			@Override
			public List<T> subList(int fromIndex, int toIndex) {
				return this;
			}
		};
	}

	public static final boolean isNullOrEmpty(Collection<?> collection) {
		return collection==null || collection.isEmpty();
	}

	public static final boolean isNullOrEmpty(Map<?,?> map) {
		return map==null || map.isEmpty();
	}

	public static final <T> void addIfNotNull(Collection<T> collection, T value) {
		if (value!=null) {
			collection.add(value);
		}
	}

	public static final <K,V> V nullSafeGet(Map<K,V> map, K key) {
		if(map == null || map.isEmpty()) {
			return null;
		}
		return map.get(key);
	}

	public static final <T> T firstOrNull(List<T> list) {
		if (list!=null && !list.isEmpty()) {
			return list.get(0);
		} else {
			return null;
		}
	}

	public static final <T> Set<T> emptySetOf(Class<T> clasz) {
		return new HashSet<>();
	}

}