package org.jbits.collections;

public class ObjectsPoolTimeoutException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7865743746038060580L;

	public ObjectsPoolTimeoutException(String message) {
		super(message);
	}

}
