package org.jbits.collections;

public interface Filter<T> {
	boolean accept(T object);
}
