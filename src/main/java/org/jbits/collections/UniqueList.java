package org.jbits.collections;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class UniqueList<T extends Comparable<T>> implements Iterable<T>, Serializable { 
	
	private static final long serialVersionUID = 6788076394752976620L;

	private ArrayList<T> list = new ArrayList<T>();
	private HashMap<T, Integer> map = new HashMap<T, Integer>();
	public T addUnique(T item) {
		// PRECONDITION
		assert item != null;
		
		Integer index = map.get( item );
		if ( index == null ) {
			index = Integer.valueOf(list.size());
			map.put( item, index );
			list.add( item );
			return item;
		}
		return list.get( index );
	}
	
	public UniqueList() {};
	
	public UniqueList(List<T> list) {
		for (T item : list) {
			addUnique(item);
		}
	}
	
	public Iterator<T> iterator() {
		return list.iterator();
	}

	public int indexOf(T element ) {
		Integer i = map.get(element);
		return i == null ? -1 : i;
	}
	
	public T getByIndex(int index) {
		return list.get(index);
	}
	
	private void adjustIndex() {
		map.clear();
		
		int i = 0;
		for( T d : list ) {
			map.put(d, i++);
		}
	}
	
	public void sort(Comparator<T> comparator) {
		Collections.sort(list, comparator);
		adjustIndex();
	}
	
	public void sort() {
		Collections.sort(list);
		adjustIndex();
	}
	
	public ArrayList<T> asArrayList() {
		return list;
	}

	public boolean contains(T obj) {
		return map.containsKey(obj);
	}

	public void addUniqueAll(Collection<T> objects) {
		Iterator<T> it = objects.iterator();
		while (it.hasNext()) {
			addUnique(it.next());
		}
	}

	public String remove(String clasz) {
		Integer index = map.remove(clasz);
		if (index!=null) {
			list.remove(index);
			return clasz;
		} else {
			return null;
		}
	}
}
