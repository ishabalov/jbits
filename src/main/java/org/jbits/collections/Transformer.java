package org.jbits.collections;

public interface  Transformer<S,D> {
	D transform(S source);
}
