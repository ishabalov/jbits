package org.jbits.collections;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.atomic.AtomicInteger;

import org.jbits.cache.TimedData;
import org.jbits.time.TimeInterval;

public class ObjectsPool<T> {
	private ObjectsFactory<T> factory;
	private LinkedBlockingDeque<TimedData<T>> queue;
	private int limit = 0;
	private TimeInterval timeToLive;
	private Map<T, TimedData<T>> reverseMap = new ConcurrentHashMap<>();
	private AtomicInteger loop = new AtomicInteger();

	public ObjectsPool(ObjectsFactory<T> factory) {
		this(factory,Integer.MAX_VALUE,TimeInterval.MAX_VALUE_INTERVAL);
	}

	public ObjectsPool(ObjectsFactory<T> factory, int limit) {
		this(factory,limit,TimeInterval.MAX_VALUE_INTERVAL);
	}

	public ObjectsPool(ObjectsFactory<T> factory, int limit, TimeInterval timeToLive) {
		this.factory=factory;
		this.limit=limit;
		this.queue=new LinkedBlockingDeque<>(this.limit);
		this.timeToLive = timeToLive;
	}

	public int getTotalSize() {
		return reverseMap.size();
	}

	public T getOrCreate(TimeInterval poolTimeout) {
		long deadline = System.nanoTime()+poolTimeout.nanoseconds();
		TimedData<T> wrapper = queue.pollFirst();
		T ret = null;
		do {
			if (wrapper==null) {
				if (reverseMap.size()<limit) {
					wrapper = new TimedData<>(timeToLive);
					ret = factory.create();
					wrapper.set(ret);
					reverseMap.put(ret, wrapper);
				}
			} else {
				ret = wrapper.get();
				if (ret==null) {
					T objectToDiscard = wrapper.clean();
					discardObject(objectToDiscard);
				}
			}
			if (ret==null) {
				if (System.nanoTime()>deadline) {
					throw new ObjectsPoolTimeoutException("Unable to obtain object from pool in "+poolTimeout.toString());
				} else {
					loop.incrementAndGet();
					synchronized (queue) {
						try {
							queue.wait(1000);
						} catch (InterruptedException e) {
							// ignore it;
						}
					}
				}
				wrapper = queue.pollFirst();
			}
		} while (ret==null);
		return ret;
	}

	public void discardObject(T object) {
		if (object!=null) {
			reverseMap.remove(object);
			factory.discard(object);
		}
	}

	public void release(T object) {
		if (object!=null) {
			TimedData<T> wrapper = reverseMap.get(object);
			if (wrapper!=null) {
				if (wrapper.isExpired()) {
					expire(wrapper);
				} else if (!queue.offerLast(wrapper)) {
					discardObject(object);
				}
				synchronized (queue) {
					queue.notify();
				}
			} else {
				factory.discard(object);
			}
		}
	}

	public void discardAll() {
		TimedData<T> wrapper = queue.poll();
		while (wrapper!=null) {
			T object = wrapper.clean();
			discardObject(object);
			wrapper = queue.poll();
		}
	}
	
	private void expire(TimedData<T> wrapper) {
		T object = wrapper.clean();
		discardObject(object);
	}

	public void invalidateAllExpiredObjects() {
		List<TimedData<T>> allWrappers = new ArrayList<>();
		allWrappers.addAll(queue);
		for (TimedData<T> wrapper:allWrappers) {
			if (wrapper.isExpired()) {
				expire(wrapper);
			}
		}
	}
	public int getLoop() {
		return loop.get();
	}
	public int getPoolFree() {
		return queue.size();
	}
	@Override
	protected void finalize() throws Throwable {
		discardAll();
	}
}
