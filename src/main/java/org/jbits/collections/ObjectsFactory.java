package org.jbits.collections;

public interface  ObjectsFactory<T> {
	T create();
	void discard(T object);
}
