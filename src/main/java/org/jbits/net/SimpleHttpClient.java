package org.jbits.net;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.StringTokenizer;

import javax.imageio.ImageIO;

import org.jbits.text.Base64;
import org.jbits.text.Charsets;
import org.jbits.text.TextHelper;
import org.jbits.time.TimeInterval;

public class SimpleHttpClient {
	public static final String METHOD_GET = "GET";
	public static final String METHOD_POST = "POST";
	private static final int MAX_BUFFER = 4096;
	private String url = null;
	private String method;
	private int timeout=0;
	private byte[] content = null;
	private int responseCode = 0;
	private long contentLoadTime = 0;
	private String postData;
	private String authorization = null;
	private int maxBuffer = Integer.MAX_VALUE;

	@SuppressWarnings("unused")
	private SimpleHttpClient() {};

	public SimpleHttpClient(String url, String method) {
		this.url = url;
		this.method = method;
	}

	public SimpleHttpClient(String url) {
		this(url, METHOD_GET);
	}
	private void postData(HttpURLConnection cnn) throws IOException {
		if (!TextHelper.isNullOrEmpty(postData)) {
			OutputStream os = null;
			try {
				os = cnn.getOutputStream();
				os.write(postData.getBytes());
			} finally {
				if (os!=null) {
					os.close();
				}
			}
		}
	}

	private void loadContent(HttpURLConnection cnn) throws IOException {
		InputStream is = cnn.getInputStream();
		try {
			int contentLength = cnn.getContentLength();
			int bufSize;
			if (contentLength>0) {
				bufSize = contentLength>MAX_BUFFER?MAX_BUFFER:contentLength;
			} else {
				bufSize=MAX_BUFFER;
			}
			if (bufSize>maxBuffer) {
				bufSize=maxBuffer;
			}
			ByteArrayOutputStream os = new ByteArrayOutputStream(bufSize);
			byte buf[] = new byte[bufSize];
			int read = is.read(buf);
			while (read>0 && os.size()<maxBuffer) {
				os.write(buf, 0, read);
				read = is.read(buf);
			}
			os.close();
			content = os.toByteArray();
			if (content==null) {
				content = new byte[0];
			}
		} finally {
			if (is!=null) {
				is.close();
				cnn.disconnect();
			}
		}
	}

	private HttpURLConnection setupConnection() throws IOException {
		URL url = new URL(this.url);
		HttpURLConnection cnn = (HttpURLConnection) url.openConnection();
		cnn.setRequestMethod(this.method);
		if (timeout>0) {
			cnn.setReadTimeout(timeout);
			cnn.setConnectTimeout(timeout);
		}
		if (authorization!=null) {
			cnn.addRequestProperty("Authorization", authorization);
		}
		if (METHOD_POST.equalsIgnoreCase(method)) {
			cnn.setDoOutput(true);
			postData(cnn);
		}
		this.responseCode = cnn.getResponseCode();
		return cnn;
	}
	
	public InputStream getInputStream() throws IOException {
		HttpURLConnection cnn = setupConnection();
		return cnn.getInputStream();
	}

	private synchronized void load() throws IOException {
		HttpURLConnection cnn = setupConnection();
		long start = System.currentTimeMillis();
		try {
			if (this.responseCode>=200&&this.responseCode<300) {
				loadContent(cnn);
			} else {
				this.content = new byte[0];
			}
		} finally {
			if( cnn != null )
				cnn.disconnect();
			contentLoadTime = System.currentTimeMillis()-start;
		}

	}

	public void setPostdata(String postData) {
		this.postData=postData;
	}

	public String getUrl() {
		return this.url;
	}

	public byte[] getContent() throws IOException {
		if (content==null) {
			load();
		}
		return content;
	}

	public long getContentLoadTime() throws IOException {
		if (content==null) {
			load();
		}
		return contentLoadTime;
	}
	public int getResponseCode() throws IOException {
		if (content==null) {
			load();
		}
		return responseCode;
	}

	public String getContentAsString() throws IOException {
		return new String(getContent());
	}

	public BufferedImage getContentAsJpegImage() throws IOException {
		ByteArrayInputStream is = new ByteArrayInputStream(getContent());
		BufferedImage ret = ImageIO.read(is);
		return ret;
	}

	public String[] getContentAsArray(String separator) throws IOException {
		String ct = new String(getContent());
		if( ct.length() > 0 ) {
			StringTokenizer tokenizer = new StringTokenizer(ct, separator);
			int size = tokenizer.countTokens();
			if( size > 0 ) {
				String[] ret = new String [size];
				for( int i = 0; i < size; i++ ) {
					ret[i] = tokenizer.nextToken();
				}
				return ret;
			} else {
				return new String[0];
			}
		} else {
			return new String[0];
		}

	}

	public int getTimeout() {
		return timeout;
	}

	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}

	public void setTimeout(TimeInterval timeout) {
		this.timeout = (int)timeout.milliseconds();
	}

	public void addBasicAuthorization(String user, String password) {
		try {
			String key = user+":"+password;
			this.authorization = "Basic "+Base64.encodeBytes(key.getBytes(Charsets.UTF_8.getName()));
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException(e);
		}
	}

	public void setMaxBuffer(int maxBuffer) {
		this.maxBuffer=maxBuffer;
	}
}