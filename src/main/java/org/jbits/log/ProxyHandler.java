package org.jbits.log;

import java.util.logging.Handler;
import java.util.logging.LogRecord;

public class ProxyHandler extends Handler {
	private Handler handler = LogHelper.createHandler();

	@Override
	public void publish(LogRecord record) {
		handler.publish(record);
	}

	@Override
	public void flush() {
		handler.flush();
	}

	@Override
	public void close() throws SecurityException {
		handler.close();
	}

}
