package org.jbits.log;

import java.io.PrintWriter;
import java.util.logging.Handler;
import java.util.logging.LogRecord;

public class PrintWriterHandler extends Handler {
	private PrintWriter writer = null;

	public PrintWriterHandler() {
		super();
	}


	public PrintWriterHandler(PrintWriter writer) {
		super();
		this.writer=writer;
	}

	
	@Override
	public void close() throws SecurityException {
		if (writer!=null) {
			writer.close();
		}
	}

	@Override
	public void flush() {
		if (writer!=null) {
			writer.flush();
		}
	}

	@Override
	public void publish(LogRecord record) {
		String message = getFormatter().format(record);
		if (writer!=null) {
			writer.println(message);
		} else {
			System.out.println(message);
		}
	}
}
