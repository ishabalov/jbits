package org.jbits.log;

import java.io.File;
import java.io.FileFilter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Handler;
import java.util.logging.LogRecord;

import org.jbits.text.TextHelper;
import org.jbits.time.TimeInterval;

public class ManagedBufferedHandler extends Handler {
	private File logFolder = null;
	private boolean useConsole = false;

	public ManagedBufferedHandler(File logFolder, int keepLogs, boolean useConsole) {
		super();
		this.logFolder = logFolder;
		this.keepLogs=keepLogs;
		this.useConsole=useConsole;
		logFolder.mkdirs();
		start();
	}


	@Override
	public void close() throws SecurityException {
		stopped.set(true);
		synchronized(buffer) {
			buffer.notify();
		}
	}

	@Override
	public void flush() {
		synchronized(buffer) {
			buffer.notify();
		}
	}


	private Queue<LogRecord> buffer = new ConcurrentLinkedQueue<>();
	private Thread loggerThread;
	private AtomicBoolean stopped = new AtomicBoolean(false);
	private int keepLogs = 10;
	private TimeInterval logRotationInterval = TimeInterval.valueOf("12 hour");
	private long lastLogRotation = Long.MIN_VALUE;

	private void doLogRotationIfNeeded() {
		if (System.currentTimeMillis()-logRotationInterval.milliseconds()>lastLogRotation) {
			rotateLogs();
			lastLogRotation = System.currentTimeMillis();
		}
	}

	private void rotateLogs() {
		List<File> currentLogFiles = new ArrayList<>();
		for (File f:logFolder.listFiles(new FileFilter() {

			@Override
			public boolean accept(File pathname) {
				return pathname.isFile() && pathname.getName().endsWith(LOG_EXTENTION);
			}
		})) {
			currentLogFiles.add(f);
		}
		Collections.sort(currentLogFiles, new Comparator<File>() {

			@Override
			public int compare(File o1, File o2) {
				return o1.getName().compareTo(o2.getName());
			}
		});
		for (int i=0;i<currentLogFiles.size()-keepLogs;i++) {
			currentLogFiles.get(i).delete();
		}
	}

	private void start() {
		loggerThread = new Thread(new Runnable(){@Override
			public void run() {

			while(!stopped.get()) {
				dumpQueue();
				doLogRotationIfNeeded();
				synchronized(buffer) {
					try {
						buffer.wait(1000L);
					} catch (InterruptedException e) {

					}
				}
			}

		}}, "jbits-logger");
		loggerThread.setDaemon(true);
		loggerThread.start();
	}


	private void writeMessageToWriter(LogRecord record, PrintStream stream) {
		String message = getFormatter().format(record);
		stream.println(message);
	}

	private static final Format FILE_NAME_FORMAT = new SimpleDateFormat("yyyy-MM-dd"); 
	private static final String LOG_EXTENTION = ".log";

	private File getLogFile() {
		String fileName = FILE_NAME_FORMAT.format(new Date())+LOG_EXTENTION;
		File ret = new File(logFolder,fileName);
		return ret;
	}

	private LogRecord lastLogMessage = null;
	private int lastLogMessageCounter = 0;
	private static final TimeInterval LOG_REPEAT_INTERVAL = TimeInterval.valueOf("10 min");
	private long lastLogMessageTimestamp = -1;
	
	private void logRecord(PrintStream stream, LogRecord record) {
		writeMessageToWriter(record, stream);
		if (useConsole) {
			writeMessageToWriter(record, System.out);
		}
	}
	
	private void logRecordIfNeeded(PrintStream stream, LogRecord record) {
		if (lastLogMessage!=null && TextHelper.nullSafeEquals(lastLogMessage.getMessage(), record.getMessage())) {
			if (System.currentTimeMillis()-lastLogMessageTimestamp>LOG_REPEAT_INTERVAL.milliseconds()) {
				LogRecord wrapper = LogHelper.wrapLogRecord(record, "repeated "+lastLogMessageCounter+" times: "+record.getMessage());
				logRecord(stream, wrapper);
				lastLogMessageCounter=0;
				lastLogMessageTimestamp = System.currentTimeMillis();
			} else {
				lastLogMessageCounter++;
			}
		} else {
			if (lastLogMessageCounter>1) {
				LogRecord wrapper = LogHelper.wrapLogRecord(lastLogMessage, "repeated "+lastLogMessageCounter+" times: "+lastLogMessage.getMessage());
				logRecord(stream, wrapper);
			}
			logRecord(stream, record);
			lastLogMessageCounter=1;
			lastLogMessageTimestamp = System.currentTimeMillis();
			lastLogMessage = record;
		}
	}
	
	private void dumpLogRecords(Iterable<LogRecord> records) {
		try (PrintStream stream = new PrintStream(new FileOutputStream(getLogFile(), true))) {
			for (LogRecord record:records) {
				logRecordIfNeeded(stream, record);
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	private void dumpQueue() {
		List<LogRecord> records = new ArrayList<>();
		LogRecord record = buffer.poll();
		while (record!=null) {
			records.add(record);
			record = buffer.poll();
		}
		dumpLogRecords(records);
	}

	@Override
	public void publish(LogRecord record) {
		if (getLevel().intValue()<=record.getLevel().intValue()) {
			if (!stopped.get()) {
				if (!buffer.offer(record)) {
					writeMessageToWriter(record, System.out);
				} else {
					synchronized(buffer) {
						buffer.notify();
					}
				}
			} else {
				writeMessageToWriter(record, System.out);
			}
		}
	}

	@Override
	public boolean equals(Object obj) {
		if( obj == null )
			return false;
		// We do need to use getName() below, because getClass() returns different things for different class loaders, but getName() doesn't.
		// We know that this isn't the way equals() is usually implemented, but unless we use getName() we will get duplicated log messages.
		if (getClass().getName().equals(obj.getClass().getName())) {
			return hashCode() == obj.hashCode();
		} else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		if (logFolder!=null) {
			return logFolder.getAbsolutePath().hashCode();
		} else {
			return getClass().getName().hashCode();
		}
	}


}
