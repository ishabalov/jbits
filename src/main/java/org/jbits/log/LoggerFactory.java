package org.jbits.log;

import java.util.logging.Logger;

public interface LoggerFactory {
	Logger getOrCreateLogger(Class<?> clasz);
}
