package org.jbits.log;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.LogManager;

import org.jbits.beans.BeansFactory;

public class LogConfig {

	private static String rootHandlerClassName = ProxyHandler.class.getName();
	public static void setRootHandlerClass(Class<?> clasz) {
		rootHandlerClassName = clasz.getName();
	}
	public static void setRootHandlerClass(String name) {
		rootHandlerClassName = name;
	}
	
	public static void resetLogging() {
		System.setProperty("java.util.logging.config.class", "org.jbits.log.LogConfig");
		reloadLoggingConfiguration();
	}

	private static String getConfig() {
		StringWriter out = new StringWriter();
		out.write("handlers="+rootHandlerClassName+"\n");
		out.write(".level="+BeansFactory.getOrCreateBean(Level.class,LogHelper.DEFAULT_LOG_LEVEL_FACTORY).getName());
		return out.toString();
	}
	
	private static final void reloadLoggingConfiguration() {
		System.out.println("loading logging properties...");
		System.out.println(getConfig());
		try (InputStream is  = new ByteArrayInputStream(getConfig().getBytes())) {
			LogManager.getLogManager().readConfiguration(is);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	public  LogConfig() {
		reloadLoggingConfiguration();
	}
}
