package org.jbits.log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.logging.Handler;
import java.util.logging.LogRecord;

public class PlainHandler extends Handler {
	private File logFile = null;

	public PlainHandler() {
		super();
	}


	public PlainHandler(String path) {
		super();
		logFile = new File(path);
	}

	public PlainHandler(File logFile) {
		super();
		this.logFile = logFile;
	}

	
	@Override
	public void close() throws SecurityException {
	}

	@Override
	public void flush() {
	}

	@Override
	public void publish(LogRecord record) {
		String message = getFormatter().format(record);
		if (logFile!=null) {
			try {
				OutputStream os = new FileOutputStream(logFile, true);
				PrintWriter ow = new PrintWriter(os);
				ow.println(message);
				ow.close();
			} catch (FileNotFoundException e) {
				throw new RuntimeException(e);
			}
		} else {
			System.out.println(message);
		}
	}

	public File getLogFile() {
		return logFile;
	}

	@Override
	public boolean equals(Object obj) {
		if( obj == null )
			return false;
		// We do need to use getName() below, because getClass() returns different things for different class loaders, but getName() doesn't.
		// We know that this isn't the way equals() is usually implemented, but unless we use getName() we will get duplicated log messages.
		if (getClass().getName().equals(obj.getClass().getName())) {
			return hashCode() == obj.hashCode();
		} else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		if (logFile!=null) {
			return logFile.getAbsolutePath().hashCode();
		} else {
			return getClass().getName().hashCode();
		}
	}


}
