package org.jbits.log;

import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

import org.jbits.beans.BeanFactory;
import org.jbits.beans.BeansFactory;

public class LogHelper {
	public static final Logger getLogger(Class<?> clasz) {
		return getLogger(clasz.getName());
	}

	static {
		System.setProperty("java.util.logging.config.class", "org.jbits.log.LogConfig");
	}
	
	private static final Object loggerCreationMutext = new Object();

	public static final Logger getLogger(String logName) {
		LogManager manager = LogManager.getLogManager();
		Logger logger = manager.getLogger(logName);
		if (logger == null) {
			synchronized (loggerCreationMutext) {
				logger = manager.getLogger(logName);
				if (logger == null) {
					logger = createLogger(logName);
					manager.addLogger(logger);
				}
			}
		}
		return logger;
	}

	public static final BeanFactory<Formatter> DEFAULT_FORMATTER_FACTORY = new BeanFactory<Formatter>() {

		@Override
		public Formatter getOrCreateBean(Class<Formatter> fieldClass, Class<?> injectedIntoClass) throws SecurityException, NoSuchMethodException, IllegalArgumentException, InstantiationException, IllegalAccessException, InvocationTargetException {
			return new PlainFormatter();
		}
	};
	
	public static final BeanFactory<Handler> DEFAULT_HANDLER_FACTORY = new BeanFactory<Handler>() {

		@Override
		public Handler getOrCreateBean(Class<Handler> fieldClass, Class<?> injectedIntoClass) throws SecurityException, NoSuchMethodException, IllegalArgumentException, InstantiationException, IllegalAccessException, InvocationTargetException {
			return new PlainHandler();
		}
	};

	public static final BeanFactory<Level> DEFAULT_LOG_LEVEL_FACTORY = new BeanFactory<Level>() {

		@Override
		public Level getOrCreateBean(Class<Level> fieldClass, Class<?> injectedIntoClass) throws SecurityException, NoSuchMethodException, IllegalArgumentException, InstantiationException, IllegalAccessException, InvocationTargetException {
			return Level.INFO;
		}
	};

	public static final Handler createHandler() {
		Formatter formatter = BeansFactory.getOrCreateBean(Formatter.class, DEFAULT_FORMATTER_FACTORY);
		Handler handler = BeansFactory.getOrCreateBean(Handler.class, DEFAULT_HANDLER_FACTORY);
		Level level = BeansFactory.getOrCreateBean(Level.class, DEFAULT_LOG_LEVEL_FACTORY);
		handler.setFormatter(formatter);
		handler.setLevel(level);
		return handler;
	}
	
	private static final synchronized Logger createLogger(String logName) {
		Logger logger = Logger.getLogger(logName);
		Handler handler = createHandler();
		logger.addHandler(handler);
		logger.setLevel(handler.getLevel());
		logger.setUseParentHandlers(false);
		return logger;
	}
	
	public static final Logger getAnonymousLogger(PrintWriter writer) {
		Logger logger = Logger.getAnonymousLogger();
		Handler handler = new PrintWriterHandler(writer);
		Formatter formatter = BeansFactory.getOrCreateBean(Formatter.class, DEFAULT_FORMATTER_FACTORY);
		Level level = BeansFactory.getOrCreateBean(Level.class, DEFAULT_LOG_LEVEL_FACTORY);
		handler.setFormatter(formatter);
		handler.setLevel(level);
		logger.addHandler(handler);
		logger.setLevel(handler.getLevel());
		logger.setUseParentHandlers(false);
		return logger;
	}
	
	public static final LogRecord wrapLogRecord(LogRecord record, String message) {
		LogRecord wrapper = new LogRecord(record.getLevel(), message);
		wrapper.setInstant(record.getInstant());
		wrapper.setLoggerName(record.getLoggerName());
		wrapper.setParameters(record.getParameters());
		wrapper.setSequenceNumber(record.getSequenceNumber());
		wrapper.setSourceClassName(record.getSourceClassName());
		wrapper.setThrown(record.getThrown());
		return wrapper;
	}

	public static final void log(Logger logger, Level level, String format, Object... parameters) {
		logger.log(level, new LoggerFormattedMessageSupplier(format, parameters));
	}

	public static final void logException(Logger logger, Level level, Throwable t, String format, Object... parameters) {
		logger.log(level, String.format(format, parameters), t);
	}

	public static final void log(Class<?> clasz, Level level, String format, Object... parameters) {
		getLogger(clasz).log(level, new LoggerFormattedMessageSupplier(format, parameters));
	}

	public static final void logException(Class<?> clasz, Level level, Throwable t, String format, Object... parameters) {
		getLogger(clasz).log(level, String.format(format, parameters), t);
	}

}
