package org.jbits.log;

import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;


public class PlainFormatter extends Formatter {

	public static final char DELIM = '|';
	private Format dateFormat = new SimpleDateFormat("yyyy-MM-dd"+DELIM+"HH:mm:ss.SSS");
	
	@Override
	public String format(LogRecord record) {
		try {
			StringBuffer buf = new StringBuffer();
			formatHeader(buf, record);
			buf.append(record.getMessage());
			if (record.getThrown()!=null) {
				buf.append("\n");
				formatTrowable(buf, record.getThrown());
			}
			return buf.toString();
		} catch (Exception e) {
			ByteArrayOutputStream os = new ByteArrayOutputStream();
			PrintWriter pr = new PrintWriter(os);
			pr.println("Exception during log formatting:");
			e.printStackTrace(pr);
			pr.close();
			return os.toString();
		}
	}

	protected void formatHeader(StringBuffer buf, LogRecord record) {
		buf.append(record.getLevel().getName());
		buf.append(DELIM);
		buf.append(dateFormat.format(new Date(record.getMillis())));
		buf.append(DELIM);
		buf.append(record.getLoggerName());
		buf.append(DELIM);
		buf.append(record.getThreadID());
		buf.append(DELIM);
	}

	protected void formatTrowable(StringBuffer buf, Throwable t) {
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		PrintWriter pw = new PrintWriter(os);
		t.printStackTrace(pw);
		pw.close();
		buf.append(os.toString());
	}

}
