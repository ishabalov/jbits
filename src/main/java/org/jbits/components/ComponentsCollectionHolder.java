package org.jbits.components;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ComponentsCollectionHolder<C extends Component> implements ComponentsCollection<C,ComponentsCollectionHolder<C>> {

	protected List<C> members = new ArrayList<>();

	public List<C> getMembers() {
		return members;
	}

	@Override
	@SuppressWarnings("unchecked")
	public void add(C... cs) {
		if (cs!=null) {
			for (C c:cs) {
				if (c!=null) {
					members.add(c);
				}
			}
		}
	}

	@Override
	public void addAll(Collection<C> col) {
		for (C c:col) {
			if (c!=null) {
				members.add(c);
			}
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public ComponentsCollectionHolder<C> with(C... cs) {
		add(cs);
		return this;
	}

	@Override
	public ComponentsCollectionHolder<C> withAll(Collection<C> col) {
		addAll(col);
		return this;
	}

	public boolean isEmpty() {
		return members.isEmpty();
	}

}
