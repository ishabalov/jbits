package org.jbits.components;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.jbits.text.TextHelper;

public abstract class AbstractComponentBase implements Component {
	
	private Map<String, Object> attributes = new HashMap<>();
	
	public Object getAttribute(String name) {
		return attributes.get(name);
	}
	
	public <T> void setAttribute(String name, T value) {
		attributes.put(name, value);
	}
	
	public void removeAttribute(String name) {
		attributes.remove(name);
	}
			
	protected static final String getRenderedClasses(List<String> classes, String... classPrepend) {
		StringBuilder ret = new StringBuilder();
		List<String> allClasses = new ArrayList<>();
		if (classPrepend!=null) {
			for (String s:classPrepend) {
				if (s!=null) {
					allClasses.add(s);
				}
			}
		}
		allClasses.addAll(classes);
		Iterator<String> it = allClasses.iterator();
		while (it.hasNext()) {
			ret.append(it.next());
			if (it.hasNext()) {
				ret.append(' ');
			}
		}
		return ret.toString();
	}

	protected static final String getNamedAttribute(String attribute, String value) {
		if (value!=null) {
			return attribute+"=\""+value+"\"";
		} else {
			return "";
		}
	}

	protected void appendFloatProperty(StringBuilder buffer, String name, float value) {
		if (!Float.isNaN(value)) {
			buffer.append(' ');
			buffer.append(name);
			buffer.append("=\"");
			buffer.append(value);
			buffer.append("\"");
		}
	}
	protected void appendFloatPropertyNotZero(StringBuilder buffer, String name, float value) {
		if (!Float.isNaN(value) && value!=0.0F) {
			buffer.append(' ');
			buffer.append(name);
			buffer.append("=\"");
			buffer.append(value);
			buffer.append("\"");
		}
	}
	protected void appendProperty(StringBuilder buffer, String name, Object value) {
		if (value!=null) {
			buffer.append(' ');
			buffer.append(name);
			buffer.append("=\"");
			buffer.append(value.toString());
			buffer.append("\"");
		}
	}

	protected void appendListProperty(StringBuilder buffer, String name, List<?> values, char separator) {
		if (values!=null && !values.isEmpty()) {
			buffer.append(' ');
			buffer.append(name);
			buffer.append("=\"");
			TextHelper.renderCollectionAsSeparatedString(values, separator);
			buffer.append("\"");
		}
	}
	protected void appendListProperty(StringBuilder buffer, String name, List<?> values) {
		appendListProperty(buffer, name, values, ';');
	}
	
	public static final String renderComponent(Component component) {
		StringBuilder ret = new StringBuilder();
		component.render(ret);
		return ret.toString();
	}

	public static final String renderComponents(Component... components) {
		StringBuilder ret = new StringBuilder();
		for (Component c:components) {
			c.render(ret);
		}
		return ret.toString();
	}

	@Override
	public String toString() {
		if (Component.class.isAssignableFrom(getClass())) {
			return renderComponent((Component)this);
		} else {
			return super.toString();
		}
	}

	
}
