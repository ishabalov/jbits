package org.jbits.components;

public interface UrlProvider<T> {
	String provideUrl(T value);
}
