package org.jbits.components;

import java.util.List;

public class Div extends AbstractCompositeComponent<Component,Div> {

	public static final String TAG = "div";
	
	protected Div() {
		super(TAG);
	}

	public static final Div newDiv() {
		return new Div();
	}

	public static final Div newDiv(String id) {
		return new Div()
				.withId(id);
	}

	public static final Div newLiteralDiv(String id, String literal) {
		return new Div()
				.withId(id)
				.with(new Literal(literal));
		
	}

	public static final Div newLiteralDiv(String literal) {
		return new Div()
				.with(new Literal(literal));
		
	}

	public static final Div newDiv(String id, Component... components) {
		return new Div()
				.withId(id)
				.with(components);
	}

	public static final Div newDiv(String id, List<Component> components) {
		return new Div()
				.withId(id)
				.withAll(components);
	}

}
