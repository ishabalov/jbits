package org.jbits.components;

public abstract class AbstractTagComponent extends AbstractComponentBase {
	private String tag;

	protected AbstractTagComponent(String tag) {
		this.tag=tag;
	}

	protected String getTag() {
		return tag;
	}

	protected abstract void appendBody(StringBuilder buffer);
	protected abstract boolean hasBody();

	protected void appendAllProperties(StringBuilder buffer) {};
	
	public void render(StringBuilder buffer) {
		buffer.append('<');
		buffer.append(tag);
		appendAllProperties(buffer);
		if (hasBody()) {
			buffer.append(">");
			appendBody(buffer);
			buffer.append("</");
			buffer.append(tag);
			buffer.append('>');
		} else {
			buffer.append("/>");
		}
	}

}
