package org.jbits.components;

public interface Component {
	
	public static final Component EMPTY_COMPONENT = new Component() {
		
		@Override
		public void render(StringBuilder buffer) {
		}

		@Override
		public String toString() {
			return "";
		}
		
		
	};
	
	void render(StringBuilder buffer);
}
