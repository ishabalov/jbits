package org.jbits.components;

import java.util.Collection;
import java.util.List;

public abstract class AbstractCompositeComponent<C extends Component,V> extends AbstractCompositionBasedComponent<C,V> implements ComponentsCollection<C,V> {

	private ComponentsCollectionHolder<C> holder = new ComponentsCollectionHolder<>();
	
	protected AbstractCompositeComponent(String tag) {
		super(tag);
	}
	
	protected List<C> getMembers() {
		return holder.getMembers();
	}
	
	public boolean isEmpty() {
		return holder.isEmpty();
	}
	
	@Override
	public void add(@SuppressWarnings("unchecked") C... cs) {
		holder.add(cs);
	}

	@Override
	public void addAll(Collection<C> col) {
		holder.addAll(col);
	}

	@SuppressWarnings("unchecked")
	@Override
	public V with(C... cs) {
		holder.with(cs);
		return (V) this;
	}

	@SuppressWarnings("unchecked")
	@Override
	public V withAll(Collection<C> col) {
		holder.withAll(col);
		return (V) this;
	}
	
	
}
