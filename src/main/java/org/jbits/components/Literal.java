package org.jbits.components;

import org.jbits.text.TextHelper;

public class Literal implements Component {
	private String value;
	
	public Literal(String value) {
		this.value = value;
	}
	
	@Override
	public void render(StringBuilder buffer) {
		if (!TextHelper.isNullOrEmpty(value)) {
			buffer.append(value);
		}
	}

	@Override
	public String toString() {
		return value;
	}
	
	

}
