package org.jbits.components;

import org.jbits.text.Formatter;

public final class ValueHolderHelper {

	public static final String DEFAULT_NULL_VALUE = "*null*";
	public static final String EMPTY_NULL_VALUE = "";
	
	protected static <T> void renderValue(StringBuilder buffer, T value, Formatter<T> format,  String nullValue) {
		if (value!=null) {
			if (format!=null) {
				buffer.append(format.format(value));
			} else {
				buffer.append(value.toString());
			}
		} else {
			buffer.append(nullValue);
		}
	}

	protected static <T> String valueToString(T value, Formatter<T> format,  String nullValue) {
		StringBuilder buffer = new StringBuilder();
		renderValue(buffer, value, format, nullValue);
		return buffer.toString();
	}
	
	protected static <T,V> void renderElement(AbstractComponent<?> component, StringBuilder buffer, AbstractValueHolder<T,V> value, UrlProvider<T> urlProvider, boolean renderSpan, String nullValue) {
		String url = null;
		if (urlProvider!=null) {
			url = urlProvider.provideUrl(value.getValue());
		}

		if (url!=null) {
			buffer.append("<a "+component.getIdString()+" href=\""+url+"\" >");
			value.renderValue(buffer, nullValue);
			buffer.append("</a>");
		} else if (renderSpan){
			buffer.append("<span "+component.getIdString()+">");
			value.renderValue(buffer, nullValue);
			buffer.append("</span>");
		} else {
			value.renderValue(buffer, nullValue);
		}
	}

	protected static <T,V> void renderElement(AbstractComponent<?> component, StringBuilder buffer, AbstractValueHolder<T,V> value, UrlProvider<T> urlProvider, boolean renderSpan) {
		renderElement(component, buffer, value, urlProvider, renderSpan, DEFAULT_NULL_VALUE);
	}
	protected static <T,V> void renderElement(AbstractValueHolder<T,V> value, StringBuilder buffer, UrlProvider<T> urlProvider, boolean renderSpan) {
		renderElement(value, buffer, value, urlProvider, renderSpan, DEFAULT_NULL_VALUE);
	}

}
