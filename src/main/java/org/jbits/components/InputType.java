package org.jbits.components;

public enum InputType {
	button,
	checkbox,
	color,
	date,
	datetime,
	datetime_local {
		@Override
		public String toString() {
			return "datetime-local";
		}
	},
	email,
	file,
	hidden,
	image,
	month,
	number,
	password,
	radio,
	range,
	reset,
	search,
	submit,
	tel,
	text,
	time,
	url,
	week;
	
	@Override
	public String toString() {
		return name();
	}
}
