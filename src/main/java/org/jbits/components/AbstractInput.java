package org.jbits.components;

import org.jbits.text.Formatter;

public abstract class AbstractInput<T,V> extends AbstractValueHolder<T,V> {

	private static final String TAG = "input";
	
	protected AbstractInput(InputType type, Formatter<T> format) {
		this(type, null, format, null);
	}
	protected AbstractInput(InputType type, T value, Formatter<T> format) {
		this(type, value, format, null);
	}
	protected AbstractInput(InputType type, T value, Formatter<T> format, String name) {
		super(TAG, value, format, name);
		this.type = type;
	}

	private InputType type = InputType.text; 
	private boolean autocomplete = true;

	public boolean isAutocomplete() {
		return autocomplete;
	}

	public void setAutocomplete(boolean autocomplete) {
		this.autocomplete = autocomplete;
	}

	@Override
	protected void appendAllProperties(StringBuilder buffer) {
		super.appendAllProperties(buffer);
		appendProperty(buffer, "type", type.toString());
		if (!autocomplete) {
			appendProperty(buffer, "autocomplete", "off");
		}
		appendProperty(buffer, "value", valueToString(ValueHolderHelper.EMPTY_NULL_VALUE));
	}
	@Override
	protected void appendBody(StringBuilder buffer) {
	}
	@Override
	protected boolean hasBody() {
		return false;
	}
}
