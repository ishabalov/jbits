package org.jbits.components;

import org.jbits.text.Formatter;
import org.jbits.text.StandardFormatters;

public class Input<T> extends AbstractInput<T,Input<T>>{

	public Input(InputType type, T value, Formatter<T> format, String name) {
		super(type, value, format, name);
	}
	public static final Input<String> newButton(String value) {
		return new Input<String>(InputType.button, value, StandardFormatters.STRING, null);
	}

	public static final Input<String> newButton(String value, String onClick) {
		return new Input<String>(InputType.button, value, StandardFormatters.STRING, null).withOnClick(onClick);
	}

	public static final Input<String> newButton(String id, String value, String onClick) {
		return new Input<String>(InputType.button, value, StandardFormatters.STRING, null).withId(id).withOnClick(onClick);
	}

	public static final Input<String> newSubmitButton(String id, String value, String onClick) {
		return new Input<String>(InputType.submit, value, StandardFormatters.STRING, null).withId(id).withOnClick(onClick);
	}

	public static final Input<String> newTextInput(String id, String name, String value) {
		return new Input<String> (InputType.text, value, StandardFormatters.NULL_SAFE_STRING, null).withId(id).withName(name);
	}
	
	public static final Input<String> newTextInput(String id, String name, String value, String onClick) {
		return new Input<String> (InputType.text, value, StandardFormatters.NULL_SAFE_STRING, null).withId(id).withOnClick(onClick).withName(name);
	}

}
