package org.jbits.components;

import java.util.Collection;
import java.util.List;

import org.jbits.collections.UniqueList;

public class CssDataHolder {
	private UniqueList<String> classes = new UniqueList<>();
	private String style=null;

	public void addClasses(String... clasz) {
		if (clasz!=null) {
			for (String s:clasz) {
				if (s!=null) {
					classes.addUnique(s);
				}
			}
		}
	}
	
	public boolean hasClass(String clasz) {
		return classes.contains(clasz);
	}
	
	public List<String> getClasses() {
		return classes.asArrayList();
	}

	public String getStyle() {
		return style;
	}

	public void setStyle(String style) {
		this.style = style;
	}

	public void addClasses(Collection<String> classes) {
		this.classes.addUniqueAll(classes);
	}

	public String removeClass(String clasz) {
		return this.classes.remove(clasz);
	}
}
