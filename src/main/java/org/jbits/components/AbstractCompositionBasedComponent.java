package org.jbits.components;

import java.util.List;

import org.jbits.collections.CollectionsHelper;

public abstract class AbstractCompositionBasedComponent<C extends Component,V> extends AbstractComponent<V> {

	protected AbstractCompositionBasedComponent(String tag) {
		super(tag);
	}

	protected abstract List<C> getMembers();

	private void renderSingleComponent(StringBuilder buffer, Component c, int index) {
		c.render(buffer);
	}

	@Override
	protected void appendBody(StringBuilder buffer) {
		List<C> lst = getMembers();
		if (!CollectionsHelper.isNullOrEmpty(lst)) {
			for (int index=0;index<lst.size();index++) {
				renderSingleComponent(buffer, lst.get(index), index);
			}
		}
	}

	@Override
	protected boolean hasBody() {
		return !CollectionsHelper.isNullOrEmpty(getMembers());
	}
}
