package org.jbits.components;

public abstract class AbstractIdHolderComponent<V> extends AbstractTagComponent {
	
	private String id=null;
	private static final String ID = "id";

	protected AbstractIdHolderComponent(String tag) {
		super(tag);
	}
	
	public void setId(String id) {
		this.id=id;
	}
	@SuppressWarnings("unchecked")
	public V withId(String id) {
		setId(id);
		return (V)this;
	}
	
	protected String getId() {
		return id;
	}
	
	protected boolean hasId() {
		return id!=null;
	}

	protected static final String getIdStr(String id) {
		return getNamedAttribute(ID, id);
	}

	protected String getIdString() {
		return getIdStr(id);
	}

	@Override
	protected void appendAllProperties(StringBuilder buffer) {
		super.appendAllProperties(buffer);
		appendProperty(buffer, ID, id);
	}
	
}
