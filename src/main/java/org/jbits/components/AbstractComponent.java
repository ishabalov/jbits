package org.jbits.components;

import java.util.Collection;
import java.util.List;

public abstract class AbstractComponent<V> extends AbstractIdHolderComponent<V> {

	private static final String CLASS = "class";
	private static final String STYLE = "style";
	private static final String ONCLICK = "onclick";
	private static final String ONMOUSEOVER = "onmouseover";
	private static final String ONMPUSEOUT = "onmouseout";
	private static final String TITLE = "title";
	
	private CssDataHolder css = new CssDataHolder();
	private String onClick = null;
	private String onMouseover = null;
	private String onMouseout = null;
	private String title = null;

	protected AbstractComponent(String tag) {
		super(tag);
	}
	
	public void addClasses(String... classes) {
		css.addClasses(classes);
	}
	
	@SuppressWarnings("unchecked")
	public V withClasses(String... classes) {
		addClasses(classes);
		return (V)this;
	}

	public void addClasses(Collection<String> classes) {
		css.addClasses(classes);
	}
	
	public boolean hasClass(String clasz) {
		return css.hasClass(clasz);
	}
	public String removeClass(String clasz) {
		return css.removeClass(clasz);
	}
	
	public List<String> getClasses() {
		return css.getClasses();
	}

	protected String getRenderedClasses(String... classPrepend) {
		return getRenderedClasses(getClasses(), classPrepend);
	}

	protected boolean hasClasses() {
		return !getClasses().isEmpty();
	}

	public String getStyle() {
		return css.getStyle();
	}

	public void setStyle(String style) {
		this.css.setStyle(style);
	}
	
	@SuppressWarnings("unchecked")
	public V withStyle(String style) {
		setStyle(style);
		return (V)this;
	}

	public String getOnClick() {
		return onClick;
	}

	public void setOnClick(String onClick) {
		this.onClick = onClick;
	}
	@SuppressWarnings("unchecked")
	public V withOnClick(String onClick) {
		setOnClick(onClick);
		return (V)this;
	}

	public String getOnMouseover() {
		return onMouseover;
	}

	public void setOnMouseover(String onMouseover) {
		this.onMouseover = onMouseover;
	}
	@SuppressWarnings("unchecked")
	public V withOnMouseover(String onMouseover) {
		setOnMouseover(onMouseover);
		return (V)this;
	}

	public String getOnMouseout() {
		return onMouseout;
	}

	public void setOnMouseout(String onMouseout) {
		this.onMouseout = onMouseout;
	}
	@SuppressWarnings("unchecked")
	public V withOnMouseout(String onMouseout) {
		setOnMouseout(onMouseout);
		return (V)this;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	@SuppressWarnings("unchecked")
	public V withTitle(String title) {
		setTitle(title);
		return (V)this;
	}
	
	protected String getClassString(String... classPrepend) {
		return getNamedAttribute(CLASS, getRenderedClasses(classPrepend));
	}
	
	@Override
	protected void appendAllProperties(StringBuilder buffer) {
		super.appendAllProperties(buffer);
		appendProperty(buffer, STYLE, css.getStyle());
		if (hasClasses()) {
			buffer.append(' ');
			buffer.append(getClassString());
		}
		appendProperty(buffer, ONCLICK, onClick);
		appendProperty(buffer, ONMOUSEOVER, onMouseover);
		appendProperty(buffer, ONMPUSEOUT, onMouseout);
		appendProperty(buffer, TITLE, title);

	}

}
