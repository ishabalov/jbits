package org.jbits.components;

import org.jbits.text.Formatter;

public abstract class AbstractValueHolder<T,V> extends AbstractComponent<V> {
	
	private static final String NAME = "name";
	private static final String ONCHANGE = "onchange";
	private static final String DISABLED = "disabled";
	
	private T value = null;
	private Formatter<T> format;
	private String name;
	private String onChange = null;
	private boolean disabled = false;

	protected AbstractValueHolder(String tag, T value, Formatter<T> format) {
		this(tag,value,format,null);
	}

	protected AbstractValueHolder(String tag, T value, Formatter<T> format, String name) {
		super(tag);
		this.value = value;
		this.format = format;
		this.name = name;
	}
	
	public T getValue() {
		return value;
	}

	public void setValue(T value) {
		this.value = value;
	}

	public void setName(String name) {
		this.name = name;
	}
	@SuppressWarnings("unchecked")
	public V withName(String name) {
		setName(name);
		return (V)this;
	}
	public void setDisabled(boolean disabled) {
		this.disabled=disabled;
	}
	@SuppressWarnings("unchecked")
	public V withDisabled(boolean disabled) {
		setDisabled(disabled);
		return (V)this;
	}

	
	protected String getNameString() {
		return getNamedAttribute("name", name);
	}

	protected String getValueString(String nullValue) {
		return getNamedAttribute("value", valueToString(nullValue));
	}
	
	protected void renderValue(StringBuilder buffer, String nullValue) {
		ValueHolderHelper.renderValue(buffer, value, format, nullValue);
	}

	protected void renderValue(StringBuilder buffer) {
		renderValue(buffer,"");
	}
	
	protected String valueToString(String nullValue) {
		StringBuilder buffer = new StringBuilder();
		renderValue(buffer, nullValue);
		return buffer.toString();
	}

	protected String valueToString() {
		return valueToString("");
	}

	protected Formatter<T> getFormat() {
		return format;
	}

	public String getOnChange() {
		return onChange;
	}

	public void setOnChange(String onChange) {
		this.onChange = onChange;
	}
	@SuppressWarnings("unchecked")
	public V withOnChange(String onChange) {
		setOnChange(onChange);
		return (V)this;
	}
	
	@Override
	protected void appendAllProperties(StringBuilder buffer) {
		super.appendAllProperties(buffer);
		appendProperty(buffer, NAME, name);
		appendProperty(buffer, ONCHANGE, onChange);
		if (disabled) {
			buffer.append(' ');
			buffer.append(DISABLED);
		}
	}
}
