package org.jbits.components;

import java.util.Collection;

public interface ComponentsCollection<C extends Component,V> {
	@SuppressWarnings("unchecked")
	public void add(C... cs);
	public void addAll(Collection<C> col);
	@SuppressWarnings("unchecked")
	public V with(C... cs); 
	public V withAll(Collection<C> col); 
}
