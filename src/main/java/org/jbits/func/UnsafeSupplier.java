package org.jbits.func;

@FunctionalInterface
public interface UnsafeSupplier<T> {
	T get() throws Exception;
}
