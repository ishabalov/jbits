package org.jbits.utils;

public interface Filter<T> {
	boolean accept(T o);
}
