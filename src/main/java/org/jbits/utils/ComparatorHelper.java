package org.jbits.utils;

import java.util.Comparator;

public class ComparatorHelper {
	public static <T> int nullSafeCompare(T o1, T o2, Comparator<T> comparator) {
		if (o1==null && o2==null) {
			return 0;
		} else if (o2==null) {
			return 1;
		} else if (o1==null) {
			return -1;
		} else {
			return comparator.compare(o1, o2);
		}
	}
}
