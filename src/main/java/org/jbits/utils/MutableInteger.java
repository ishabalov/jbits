package org.jbits.utils;

public class MutableInteger {
	private int value=0;
	
	public int get() {
		return value;
	}
	public void set(int value) {
		this.value=value;
	}
	public int addAndGet(int delta) {
		value+=delta;
		return value;
	}
	public int IncAndGet() {
		return value++;
	}
}
