package org.jbits.utils;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

public class AtomicReferencePlus<V> extends java.util.concurrent.atomic.AtomicReference<V>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6998299484023556668L;

	
	
	public AtomicReferencePlus() {
		super();
	}

	public AtomicReferencePlus(V initialValue) {
		super(initialValue);
	}

	/*
	 * NOT ATOMIC
	 */
	public <R> R orElse(Function<V,R> func, R defaultValue) {
		V obj = get();
		if (obj!=null) {
			return func.apply(obj);
		} else {
			return defaultValue;
		}
	}

	/*
	 * NOT ATOMIC
	 */
	public <R> R orElse(Function<V,R> func) {
		return orElse(func, null);
	}

	/*
	 * NOT ATOMIC
	 */
	public void consumeIfNotEmpty(Consumer<V> consumer) {
		V obj = get();
		if (obj!=null) {
			consumer.accept(obj);
		}
	}
	
	public V computeIfEmpty(Supplier<V> supplier) {
		return updateAndGet((V o)->{
			if (o!=null) {
				return o;
			} else {
				return supplier.get();
			}
		});
	}
}
