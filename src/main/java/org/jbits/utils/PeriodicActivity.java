package org.jbits.utils;

import org.jbits.time.TimeInterval;

public class PeriodicActivity {
	private Runnable action;
	private long lastRun = -1;
	private TimeInterval interval;
	
	public PeriodicActivity(Runnable action, TimeInterval interval) {
		this.action = action;
		this.interval = interval;
	}

	public void mayRun() {
		if (lastRun+interval.milliseconds()<System.currentTimeMillis()) {
			action.run();
			lastRun = System.currentTimeMillis();
		}
	}
	
	public void run() {
		action.run();
		lastRun = System.currentTimeMillis();
	}
	
}
