package org.jbits.utils;

public interface Named {
	String name();
}
