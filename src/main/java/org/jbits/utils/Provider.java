package org.jbits.utils;

public interface Provider <T> {
	T provide();
}
