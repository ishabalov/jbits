package org.jbits.utils;

import org.jbits.text.TextHelper;

public class NumberHelper {
	public static final double nullSafeToDouble(String str, double defaultValue) {
		if (!TextHelper.isNullOrEmpty(str)) {
			return Double.valueOf(str);
		} else {
			return defaultValue;
		}
	}
}
