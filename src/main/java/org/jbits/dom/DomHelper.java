package org.jbits.dom;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class DomHelper {
	public static final Iterable<Node> elementsByName(Element e, String name) {
		List<Node> ret = new ArrayList<>();
		NodeList lst = e.getElementsByTagName(name);
		for (int index=0;index<lst.getLength();index++) {
			ret.add(lst.item(index));
		}
		return ret;
	}

	public static final Collection<Node> elementsByName(Node n, String name) {
		List<Node> ret = new ArrayList<>();
		NodeList lst = n.getChildNodes();
		for (int index=0;index<lst.getLength();index++) {
			Node nn = lst.item(index);
			if (nn.getNodeType()==Node.ELEMENT_NODE && name.equals(nn.getNodeName())) {
				ret.add(nn);
			}
		}
		return ret;
	}


	public static final String attributeValue(Node node, String name) {
		return attributeValue(node,name,null);
	}

	public static final String attributeValue(Node node, String name, String defaultValue) {
		Node item = node.getAttributes().getNamedItem(name);
		if (item==null) {
			return defaultValue;
		} else {
			return item.getNodeValue();
		}
	}

}
