package org.jbits.reflect;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.regex.Pattern;

public class ReflectionHelper {

	public static final String LIST_DELIMITER = ",";

	public static final Class<?> getClass(String name) throws ClassNotFoundException {
		return Thread.currentThread().getContextClassLoader().loadClass(name);
	}

	@SuppressWarnings("unchecked")
	public static final <T> Class<? extends T> getClass(Class<T> clasz, String name) throws ClassNotFoundException {
		return (Class<? extends T>) Thread.currentThread().getContextClassLoader().loadClass(name);
	}

	public static final boolean hasClass(String name) {
		try {
			return getClass(name)!=null;
		} catch (ClassNotFoundException e) {
			return false;
		}
	}

	public static final <T> T newInstance(Class<T> clasz) throws SecurityException, NoSuchMethodException, IllegalArgumentException, InstantiationException, IllegalAccessException, InvocationTargetException {
		Constructor<T> c = clasz.getDeclaredConstructor(new Class[] {});
		return c.newInstance(new Object[] {});
	}

	public static final Object newInstance(String className) throws SecurityException, NoSuchMethodException, IllegalArgumentException, InstantiationException, IllegalAccessException, InvocationTargetException, ClassNotFoundException {
		Class<?> clasz = getClass(className);
		Constructor<?> c = clasz.getDeclaredConstructor(new Class[] {});
		return c.newInstance(new Object[] {});

	}
	
	@SuppressWarnings("unchecked")
	public static final <T> T newInstance(Class<T> clasz, Object params[], Class<?> paramClasses[]) throws SecurityException, NoSuchMethodException, IllegalArgumentException, InstantiationException, IllegalAccessException, InvocationTargetException, ClassNotFoundException {
		Constructor<?> c = clasz.getDeclaredConstructor(paramClasses);
		c.setAccessible(true);
		return (T) c.newInstance(params);
	}

	@SuppressWarnings("unchecked")
	public static final <T> T newInstance(Class<T> clasz, Object... params) throws SecurityException, NoSuchMethodException, IllegalArgumentException, InstantiationException, IllegalAccessException, InvocationTargetException, ClassNotFoundException {
		Class<?> paramClasses[];
		if (params != null) {
			paramClasses = new Class[params.length];
			for (int index = 0; index < paramClasses.length; index++) {
				paramClasses[index] = params[index].getClass();
			}
		} else {
			paramClasses = new Class[0];
		}
		Constructor<?> c = clasz.getDeclaredConstructor(paramClasses);
		c.setAccessible(true);
		return (T) c.newInstance(params);
	}

	public static Object evaluateMethod(Object object, String name, Object params[]) throws SecurityException, NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException, ClassNotFoundException {
		return evaluateMethod(object.getClass(), object, name, params);
	}

	public static final Method getMethod(Class<?> clasz, String methodName) throws NoSuchMethodException {
		Class<?> paramsClasses[] = new Class[0];
		return getMethod(clasz, methodName, paramsClasses);
	}

	public static final Method getMethod(Class<?> clasz, String methodName, Class<?> paramsClasses[]) throws NoSuchMethodException {
		try {
			Method method = clasz.getDeclaredMethod(methodName, paramsClasses);
			method.setAccessible(true);
			return method;
		} catch (NoSuchMethodException e) {
			Class<?> superClass = clasz.getSuperclass();
			if (superClass != null) {
				return getMethod(superClass, methodName, paramsClasses);
			} else {
				throw e;
			}
		}
	}

	public static final Object evaluateMethod(Class<?> clasz, Object object, String name, Object params[]) throws SecurityException, NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException, ClassNotFoundException {
		Class<?> paramsClasses[];
		if (params != null && params.length > 0) {
			paramsClasses = new Class[params.length];
			for (int index = 0; index < params.length; index++) {
				if (params[index] != null) {
					paramsClasses[index] = params[index].getClass();
				} else {
					paramsClasses[index] = Object.class;
				}
			}
		} else {
			paramsClasses = new Class[0];
		}
		try {
			Method method = clasz.getDeclaredMethod(name, paramsClasses);
			method.setAccessible(true);
			Object ret = method.invoke(object, params);
			return ret;
		} catch (NoSuchMethodException e) {
			Class<?> superClass = clasz.getSuperclass();
			if (superClass != null) {
				return evaluateMethod(superClass, object, name, params);
			} else {
				throw e;
			}
		}
	}

	public static final Object evaluateMethod(Class<?> clasz, Object object, String name, Object params[], Class<?> paramsClasses[]) throws SecurityException, NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException, ClassNotFoundException {
		try {
			Method method = clasz.getDeclaredMethod(name, paramsClasses);
			Object ret = method.invoke(object, params);
			return ret;
		} catch (NoSuchMethodException e) {
			Class<?> superClass = clasz.getSuperclass();
			if (superClass != null) {
				return evaluateMethod(superClass, object, name, params, paramsClasses);
			} else {
				throw e;
			}
		}
	}

	public static final Object evaluateMethod(Class<?> clasz, Object object, String name) throws SecurityException, NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException, ClassNotFoundException {
		Object[] params = new Object[0];
		Class<?>[] paramsClasses = new Class[0];
		return evaluateMethod(clasz,object,name,params,paramsClasses);
	}

	public static final boolean hasMethod(Class<?> clasz, String name, Class<?> paramsClasses[]) {
		try {
			return clasz.getDeclaredMethod(name, paramsClasses) != null;
		} catch (NoSuchMethodException e) {
			Class<?> superClass = clasz.getSuperclass();
			if (superClass != null) {
				return hasMethod(superClass, name, paramsClasses);
			} else {
				return false;
			}
		}
	}

	public static final boolean hasMethod(Class<?> clasz, String name) {
		Class<?>[] paramsClasses = new Class[0];
		return hasMethod(clasz, name, paramsClasses);
	}

	public static final Object getField(Class<?> clasz, Object object, String name) throws SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
		Field field = clasz.getDeclaredField(name);
		field.setAccessible(true);
		Object fieldValue = field.get(object);
		return fieldValue;
	}

	public static final Object getField(Object object, String name) throws SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
		Class<?> clasz = object.getClass();
		Field field = clasz.getDeclaredField(name);
		field.setAccessible(true);
		Object fieldValue = field.get(object);
		return fieldValue;
	}

	public static final Object castTo(Object src, Class<?> resultClass) throws SecurityException, IllegalArgumentException, IllegalAccessException, InvocationTargetException, ClassNotFoundException {

		if (resultClass.isInstance(src)) {
			return resultClass.cast(src);
		} else if (resultClass.getName().equals("long")) {
			return ((Long) castTo(src, Long.class)).longValue();
		} else if (resultClass.getName().equals("int")) {
			return ((Integer) castTo(src, Integer.class)).intValue();
		} else if (resultClass.getName().equals("float")) {
			return ((Float) castTo(src, Float.class)).floatValue();
		} else if (resultClass.getName().equals("double")) {
			return ((Double) castTo(src, Double.class)).doubleValue();
		} else if (resultClass.getName().equals("boolean")) {
			return ((Boolean) castTo(src, Boolean.class)).booleanValue();
		} else if (resultClass.getName().equals("short")) {
			return ((Short) castTo(src, Short.class)).shortValue();
		} else if (resultClass.getName().equals("char")) {
			return ((Character) castTo(src, Character.class)).charValue();
		} else if (resultClass.getName().equals("byte")) {
			return ((Byte) castTo(src, Byte.class)).byteValue();
		} else if (Pattern.class.equals(resultClass)) {
			return Pattern.compile(src.toString());
		} else if (Class.class.equals(resultClass)) {
			return Thread.currentThread().getContextClassLoader().loadClass(src.toString());
		} else {
			Object params[] = new Object[1];
			params[0] = src;
			try {
				return evaluateMethod(resultClass, null, "valueOf", params);
			} catch (NoSuchMethodException e) {
				try {
					Constructor<?> c = resultClass.getConstructor(src.getClass());
					return c.newInstance(src);
				} catch (NoSuchMethodException ee) {
					throw new RuntimeException("Object " + src + " cannot be instatiated because " + resultClass.getName() + " has no constructor with string argument");
				} catch (InstantiationException ee) {
					throw new RuntimeException("Object " + src + " cannot be cast to " + resultClass.getName());
				}
			}
		}
	}

	public static final void setField(Object object, String name, Object value) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InvocationTargetException, SecurityException, NoSuchFieldException {
		Class<?> clasz = getClass(object.getClass().getName());
		setField(clasz, object, name, value);
	}

	public static final void setField(Class<?> clazz, Object object, String name, Object value) throws ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InvocationTargetException, SecurityException, NoSuchFieldException {

		Field field = getField(clazz, name);
		Class<?> fieldType = field.getType();

		String setter = "set" + name.substring(0, 1).toUpperCase() + name.substring(1);
		Class<?> methodParams[] = new Class[1];
		methodParams[0] = fieldType;
		try {
			Method method = clazz.getDeclaredMethod(setter, methodParams);
			method.setAccessible(true);
			method.invoke(object, castTo(value, fieldType));
		} catch (NoSuchMethodException e) {
			field.setAccessible(true);
			field.set(object, castTo(value, fieldType));
		}
	}

	@SuppressWarnings("unchecked")
	public static final void setCollectionField(Object containingObject, String fieldName, Object value) throws SecurityException, IllegalArgumentException, IllegalAccessException, InvocationTargetException, ClassNotFoundException, NoSuchFieldException {
		Class<?> containingClass = getClass(containingObject.getClass().getName());
		Field field = getField(containingClass, fieldName);
		// get the collection from the containing object
		Collection<Object> resultCollection = null;
		String getter = "get" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
		try {
			Method method = containingClass.getDeclaredMethod(getter, new Class[0]);
			resultCollection = (Collection<Object>) method.invoke(containingObject, new Object[0]);
		} catch (NoSuchMethodException e) {
			field.setAccessible(true);
			resultCollection = (Collection<Object>) field.get(containingObject);
		}

		// it must be initialized by the containing object
		if (resultCollection == null) {
			throw new RuntimeException("Error in ReflectionHelper.setCollectionField: " + fieldName + " is null. In order to set a Collection field using" + "this facility, the field must be initialized (as a concrete object) in the containing bean.");
		}

		// get the type for the generic, split the list, cast its items, and
		// add them to collection
		Type fieldType = ((ParameterizedType) field.getGenericType()).getActualTypeArguments()[0];
		Class<?> fieldClazz = ReflectionHelper.getClass(fieldType.toString().replaceFirst(".+ ", ""));

		String[] itemList = ((String) castTo(value, java.lang.String.class)).split(LIST_DELIMITER);
		for (String item : itemList) {
			resultCollection.add(castTo(item, fieldClazz));
		}

	}

	private static final Field getField(Class<?> clazz, String name) throws SecurityException, NoSuchFieldException, ClassNotFoundException {

		try {
			Field field = clazz.getDeclaredField(name);
			return field;
		} catch (NoSuchFieldException e) {
			Class<?> superClass = clazz.getSuperclass();
			if (superClass != null) {
				return getField(superClass, name);
			} else {
				throw e;
			}
		}
	}

	public static final Class<?> getFieldClass(Class<?> clazz, String name) throws SecurityException, NoSuchFieldException, ClassNotFoundException {
		return getField(clazz, name).getType();
	}

	private static final void loadAllFields(Class<?> clasz, Collection<Field> collection) {
		for (Field f : clasz.getDeclaredFields()) {
			collection.add(f);
		}
		Class<?> superclass = clasz.getSuperclass();
		if (!Object.class.equals(superclass)) {
			loadAllFields(superclass, collection);
		}
	}

	public static final Iterable<Field> getFields(Class<?> clasz) {
		List<Field> ret = new ArrayList<Field>();
		loadAllFields(clasz, ret);
		return ret;
	}

	public static final Iterable<Field> getFields(Object object) {
		return getFields(object.getClass());
	}

	private static final <T extends Annotation> void loadAllMethods(Class<?> clasz, Collection<Method> collection, Class<T> annotationClass) {
		for (Method m : clasz.getDeclaredMethods()) {
			Annotation p = m.getAnnotation(annotationClass);
			if (p != null) {
				collection.add(m);
			}
		}
		Class<?> superclass = clasz.getSuperclass();
		if (!Object.class.equals(superclass)) {
			loadAllMethods(superclass, collection, annotationClass);
		}
	}

	public static final <T extends Annotation> Iterable<Method> getAnnotatedMethods(Class<?> clasz, Class<T> annotationClass) {
		List<Method> ret = new ArrayList<Method>();
		loadAllMethods(clasz, ret, annotationClass);
		return ret;
	}

	public static final <T extends Annotation> Iterable<Method> getAnnotatedMethods(Object object, Class<T> annotationClass) {
		return getAnnotatedMethods(object.getClass(), annotationClass);
	}

}
