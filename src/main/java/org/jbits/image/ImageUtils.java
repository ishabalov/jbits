package org.jbits.image;

import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Transparency;
import java.awt.image.BufferedImage;
import java.awt.image.Raster;
import java.awt.image.RenderedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;

public class ImageUtils {
	public static BufferedImage loadScaledImage(File imageFile, TransformerDescriptor descriptor) throws IOException {
		BufferedImage image = ImageIO.read(imageFile);
		double w = image.getWidth(null);
		double h = image.getHeight(null);
		double scale;
		if (w>h) {
			scale = ((double)descriptor.getBox())/(double)w;
		} else {
			scale = ((double)descriptor.getBox())/(double)h;
		}
		double stepscale = stepScale(scale, 25);
		return getScaledInstance(image, (int)(w*scale),(int)(h*scale),RenderingHints.VALUE_INTERPOLATION_BICUBIC,true,stepscale);
	}

	public static BufferedImage scaleImage(BufferedImage image, int box, int numberOfSteps) throws IOException {
		return scaleImage(image, box, box, numberOfSteps);
	}	

	public static BufferedImage scaleImage(BufferedImage image, int maxWidth, int masHeight, int numberOfSteps) throws IOException {
		if (image!=null) {
			double w = image.getWidth(null);
			double h = image.getHeight(null);
			double scaleWidth = ((double)maxWidth)/(double)w;
			double scaleHeight = ((double)masHeight)/(double)h;
			double scale = Math.min(scaleWidth,scaleHeight);
			double stepscale = stepScale(scale, numberOfSteps);
			return getScaledInstance(image, (int)(w*scale),(int)(h*scale),RenderingHints.VALUE_INTERPOLATION_BICUBIC,true,stepscale);
		} else {
			return image;
		}
	}	
	
	/**
	 * Convenience method that returns a scaled instance of the
	 * provided {@code BufferedImage}.
	 *
	 * @param img the original image to be scaled
	 * @param targetWidth the desired width of the scaled instance,
	 *    in pixels
	 * @param targetHeight the desired height of the scaled instance,
	 *    in pixels
	 * @param hint one of the rendering hints that corresponds to
	 *    {@code RenderingHints.KEY_INTERPOLATION} (e.g.
	 *    {@code RenderingHints.VALUE_INTERPOLATION_NEAREST_NEIGHBOR},
	 *    {@code RenderingHints.VALUE_INTERPOLATION_BILINEAR},
	 *    {@code RenderingHints.VALUE_INTERPOLATION_BICUBIC})
	 * @param higherQuality if true, this method will use a multi-step
	 *    scaling technique that provides higher quality than the usual
	 *    one-step technique (only useful in downscaling cases, where
	 *    {@code targetWidth} or {@code targetHeight} is
	 *    smaller than the original dimensions, and generally only when
	 *    the {@code BILINEAR} hint is specified)
	 * @return a scaled version of the original {@code BufferedImage}
	 */
	public static BufferedImage getScaledInstance(BufferedImage img,
			int targetWidth,
			int targetHeight,
			Object hint,
			boolean higherQuality, double stepscale)
	{
		int type = (img.getTransparency() == Transparency.OPAQUE) ?
				BufferedImage.TYPE_INT_RGB : BufferedImage.TYPE_INT_ARGB;
		BufferedImage ret = (BufferedImage)img;
		int w, h;
		if (higherQuality) {
			// Use multi-step technique: start with original size, then
			// scale down in multiple passes with drawImage()
			// until the target size is reached
			w = img.getWidth();
			h = img.getHeight();
		} else {
			// Use one-step technique: scale directly from original
			// size to target size with a single drawImage() call
			w = targetWidth;
			h = targetHeight;
		}
		int loopBreaker=0;
		do {
			if (higherQuality && w != targetWidth) {
				w = (int)(((float)w)*stepscale);
				if (w < targetWidth) {
					w = targetWidth;
				}
			}

			if (higherQuality && h != targetHeight) {
				h = (int)(((float)h)*stepscale);
				if (h < targetHeight) {
					h = targetHeight;
				}
			}

			BufferedImage tmp = new BufferedImage(w, h, type);

			Graphics2D g2 = tmp.createGraphics();
			g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, hint);
			g2.drawImage(ret, 0, 0, w, h, null);
			g2.dispose();
			ret = tmp;
			loopBreaker++;
		} while (loopBreaker<100 && (w != targetWidth || h != targetHeight));

		return ret;
	}

	private static final double stepScale(double scale, int numberOfSteps) {
		return Math.pow(scale, 1.0/(double)numberOfSteps);
	}

	public static void sendBufferedImageToStream(RenderedImage image, TransformerDescriptor descriptor, OutputStream stream) throws IOException  {
		try {
			ImageIO.write(image, descriptor.getFormat(), stream);
		} finally {
			stream.close();
		}
	}
	public static byte[] imageToByteArray(BufferedImage image, TransformerDescriptor descriptor) throws IOException  {
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		try {
			ImageIO.write(image, descriptor.getFormat(), stream);
		} finally {
			stream.close();
		}
		return stream.toByteArray();
	}

	public static TransformerDescriptor getTransformerDescriptor(File resource) {
		return TransformerDescriptor.getTransformerDescriptor(resource);
	}

	public static final class TransformerDescriptor {
		private static final Pattern PNG_SCALE_PATTERN=Pattern.compile("^.*\\.png.x([0-9]+)$", Pattern.CASE_INSENSITIVE);
		private static final Pattern JPG_SCALE_PATTERN=Pattern.compile("^.*\\.jpe?g.x([0-9]+)$", Pattern.CASE_INSENSITIVE);
		private String format;
		private String path;
		private int box;
		private TransformerDescriptor(String path, String format, int box) {
			this.path=path;
			this.format = format;
			this.box = box;
		}
		public String getFormat() {
			return format;
		}
		public int getBox() {
			return box;
		}
		public String getPath() {
			return path;
		}

		private static TransformerDescriptor getTransformerDescriptor(File resource) {
			String name = resource.getName();
			Matcher m = PNG_SCALE_PATTERN.matcher(name);
			if (m.matches()) {
				int box = Integer.valueOf(m.group(1));
				String path = resource.getPath().replaceFirst("\\.[xX][0-9]+", "");
				return new TransformerDescriptor(path, "png", box);
			} else {
				m = JPG_SCALE_PATTERN.matcher(name);
				if (m.matches()) {
					int box = Integer.valueOf(m.group(1));
					String fileName = resource.getPath().replaceFirst("\\.[xX][0-9]+", "");
					return new TransformerDescriptor(fileName,"jpeg", box);
				}
			}
			return null;
		}
	}

	private static float luminance(float r, float g, float b) {
		return Math.round(0.299f * r + 0.587f * g + 0.114f * b);
	}

	public static float getNormalizedAveragePixelLuminance(BufferedImage sourceImage) {
		int type = sourceImage.getType();
		int width = sourceImage.getWidth();
		int height = sourceImage.getHeight();
		int picsize = width * height;
		float minLum = Float.MAX_VALUE;
		double accummulatedLum = 0.0;

		if (type == BufferedImage.TYPE_INT_RGB || type == BufferedImage.TYPE_INT_ARGB) {
			int[] pixels = (int[])sourceImage.getData().getDataElements(0, 0, width, height, null);
			for (int i = 0; i < picsize; i++) {
				int p = pixels[i];
				int r = (p & 0xff0000) >> 16;
			int g = (p & 0xff00) >> 8;
				int b = p & 0xff;
				float lum = luminance(r, g, b);
				if (lum<minLum) {
					minLum=lum;
				}
				accummulatedLum+=lum;
			}
		} else if (type == BufferedImage.TYPE_BYTE_GRAY) {
			byte[] pixels = (byte[]) sourceImage.getData().getDataElements(0, 0, width, height, null);
			for (int i = 0; i < picsize; i++) {
				float lum = (float)(pixels[i] & 0xff);
				minLum = Math.min(minLum,lum);
				accummulatedLum+=lum;
			}
		} else if (type == BufferedImage.TYPE_USHORT_GRAY) {
			short[] pixels = (short[]) sourceImage.getData().getDataElements(0, 0, width, height, null);
			for (int i = 0; i < picsize; i++) {
				float lum = (float)(pixels[i] & 0xffff) / 256;
				minLum = Math.min(minLum,lum);
				accummulatedLum+=lum;
			}
		} else if (type == BufferedImage.TYPE_3BYTE_BGR) {
			byte[] pixels = (byte[]) sourceImage.getData().getDataElements(0, 0, width, height, null);
			int offset = 0;
			for (int i = 0; i < picsize; i++) {
				int b = pixels[offset++] & 0xff;
				int g = pixels[offset++] & 0xff;
				int r = pixels[offset++] & 0xff;
				float lum = luminance(r, g, b);
				minLum = Math.min(minLum,lum);
				accummulatedLum+=lum;
			}
		} else {
			throw new IllegalArgumentException("Unsupported image type: " + type);
		}
		float ret = (float)(accummulatedLum/(double)picsize)-minLum; 
		return ret;
	}	

	private static final boolean checkTile(int tx, int tw, int ty, int th, float data[], int w, int h) {
		boolean hasEdge = false;
		for (int px=tx;!hasEdge&&px<tx+tw;px++) {
			for (int py=ty;!hasEdge&&py<ty+th;py++) {
				int i = py*w*3+px*3;
				if (i+2<data.length) {
					float rv = data[i]+data[i+1]+data[i+2];
					if (rv>0.0F) {
						hasEdge=true;
					}
				}
			}
		}
		return hasEdge;
	}

	public static float getVideoDetailsLevel(BufferedImage image, int box) {
		CannyEdgeDetector detector = new CannyEdgeDetector(image);
		detector.setLowThreshold(0.4f);
		detector.setHighThreshold(2f);
		detector.setContrastNormalized(false);
		Raster raster = detector.getEdgesImage().getData(); 
		int rw = raster.getWidth();
		int rh = raster.getHeight();
		int nbands = raster.getNumBands();
		float data[] = new float[rw*rh*nbands];
		data = raster.getPixels(0, 0, rw, rh, data);
		int tw=rw/box;
		int th=rh/box;
		int numTilesWithEdge = 0;
		for (int tx=0;tx<rw;tx+=tw) {
			for (int ty=0;ty<rh;ty+=th) {
				if (checkTile(tx, tw, ty, th, data, rw, rh)) {
					numTilesWithEdge++;
				}
			}
		}
		return numTilesWithEdge;
	}
}
