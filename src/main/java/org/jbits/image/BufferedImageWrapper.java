package org.jbits.image;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;

import javax.imageio.ImageIO;

public class BufferedImageWrapper implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6804766434815153661L;
	private byte[] buffer;
	private String format;
	
	public static final String DEFAULT_FORMAT = "jpeg";
	
	private void loadImage(BufferedImage image) throws IOException {
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		ImageIO.write(image, format, os);
		os.close();
		this.buffer = os.toByteArray();
	}
	
	public BufferedImageWrapper(BufferedImage image) throws IOException {
		if (image!=null) {
			this.format = DEFAULT_FORMAT;
			loadImage(image);
		} else {
			this.buffer = null;
			this.format = null;
		}
	}

	public BufferedImageWrapper(BufferedImage image, String format) throws IOException {
		if (image!=null) {
			this.format = format;
			loadImage(image);
		} else {
			this.buffer = null;
			this.format = null;
		}
	}
	
	public BufferedImage getImage() throws IOException {
		if (buffer==null) {
			return null;
		} else {
			ByteArrayInputStream is = new ByteArrayInputStream(buffer);
			return ImageIO.read(is);
		}
	}
	
	public String getFormat() {
		return format;
	}
	
	
}
