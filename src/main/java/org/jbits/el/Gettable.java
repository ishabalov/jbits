package org.jbits.el;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public abstract class Gettable<K,V> implements Map<K,V> {

	@Override
	public void clear() {
	}

	@Override
	public boolean containsKey(Object arg0) {
		return true;
	}

	@Override
	public boolean containsValue(Object arg0) {
		return true;
	}

	@Override
	public Set<java.util.Map.Entry<K, V>> entrySet() {
		return null;
	}

	@Override
	public abstract V get(Object arg0);

	@Override
	public boolean isEmpty() {
		return false;
	}

	@Override
	public Set<K> keySet() {
		return new HashSet<K>();
	}

	@Override
	public V put(K arg0, V arg1) {
		return null;
	}

	@Override
	public void putAll(Map<? extends K, ? extends V> arg0) {
	}

	@Override
	public V remove(Object arg0) {
		return null;
	}

	@Override
	public int size() {
		return 0;
	}

	@Override
	public Collection<V> values() {
		return null;
	}

}
