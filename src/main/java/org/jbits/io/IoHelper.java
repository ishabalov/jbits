package org.jbits.io;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.jbits.text.TextHelper;

public class IoHelper {
	public static final String readFile(Path path, Charset charset) throws IOException {
		return new String(Files.readAllBytes(path),charset);
	}
	public static final String readFile(File path, Charset charset) throws IOException {
		return readFile(path, charset.name());
	}
	public static final String readFile(File path, String charset) throws IOException {
		return FileUtils.readFileToString(path, charset);
	}
	public static final File getFile(String root, String... path) {
		if (TextHelper.isNullOrEmpty(root)) {
			throw new IllegalArgumentException("Root path cannot be null");
		}
		if (path!=null && path.length>0) {
			StringBuilder fullPath = new StringBuilder();
			fullPath.append(root);
			fullPath.append('/');
			for (int index=0;index<path.length;index++) {
				String pathElement = path[index];
				if (!TextHelper.isNullOrEmpty(pathElement)) {
					fullPath.append(pathElement);
					if (index<path.length-1) {
						fullPath.append('/');
					}
				}
			}
			return new File(fullPath.toString());
		} else {
			return new File(root);
		}
	}

	private static final int BUF_LENGTH=32000;
	public static final void copyStream(InputStream is, WritableByteChannel ch) throws IOException {
		ByteBuffer buf = ByteBuffer.allocate(BUF_LENGTH);
		byte[] bufArray = buf.array();
		int read = is.read(bufArray);
		while (read>0) {
			buf.flip();
			ch.write(buf);
			buf.clear();
			read = is.read(bufArray);
		}
	}
	public static final void saveStringToFile(Path path, String data) throws IOException {
		try (OutputStream os = Files.newOutputStream(path)) {
			os.write(data.getBytes(StandardCharsets.UTF_8));
		}
	}

	public static final byte[] readInputStream(InputStream inx) throws IOException {
		try (ByteArrayOutputStream os = new ByteArrayOutputStream(); InputStream in = inx) {
			IOUtils.copy(in, os);
			return os.toByteArray();
		}
	}

	public static final String asUTF8String(byte[] buf) {
		return new String(buf,StandardCharsets.UTF_8);
	}

	public static final String readFile(String path) throws IOException {
		return loadFileToString(path);
	}

	public static final String loadStreamToString(InputStream in) throws IOException {
		return asUTF8String(readInputStream(in));
	}

	public static final String loadFileToString(String path) throws IOException {
		return loadFileToString(new File(path));
	}

	public static final String loadFileToString(File file) throws IOException {
		return asUTF8String(readInputStream(Files.newInputStream(file.toPath())));
	}

	public static final String loadPathToString(Path path) throws IOException {
		return asUTF8String(readInputStream(Files.newInputStream(path)));
	}

	public static final byte[] loadPath(Path path) throws IOException {
		return readInputStream(Files.newInputStream(path));
	}

	public static final void saveStringToFile(File file, String data) throws IOException {
		try (OutputStream os = Files.newOutputStream(file.toPath())) {
			os.write(data.getBytes(StandardCharsets.UTF_8));
		}
	}

}
