package org.jbits.beans;

import java.lang.reflect.InvocationTargetException;

public interface BeanFactory<T> {
	T getOrCreateBean(Class<T> fieldClass, Class<?> injectedIntoClass) throws SecurityException, NoSuchMethodException, IllegalArgumentException, InstantiationException, IllegalAccessException, InvocationTargetException;
}
