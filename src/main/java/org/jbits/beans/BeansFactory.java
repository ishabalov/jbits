package org.jbits.beans;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Logger;

import org.jbits.log.LogHelper;
import org.jbits.reflect.ReflectionHelper;

public class BeansFactory {

	private static final Map<Class<?>,Object> beans = new HashMap<>();

	private static final Object beansCreationMutex = new Object();

	private static final Map<Class<?>,BeanFactory<?>> knownBeansFactories = new ConcurrentHashMap<>();

	static {
		knownBeansFactories.put(Logger.class, new BeanFactory<Logger>() {

			@Override
			public Logger getOrCreateBean(Class<Logger> fieldClass, Class<?> beanClass) {
				return LogHelper.getLogger(beanClass);
			}
		});
	}

	public static void setKnownBeanFactory(Class<?> beandClass, BeanFactory<?> factory) {
		knownBeansFactories.put(beandClass,factory);
	}

	public static final void clear() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		synchronized(beansCreationMutex) {
			for (Map.Entry<Class<?>,Object> b:beans.entrySet()) {
				for (Method method:ReflectionHelper.getAnnotatedMethods(b.getKey(), PreDestroy.class)) {
					Object params[] = new Object[0];
					method.setAccessible(true);
					method.invoke(b.getValue(), params);
				}
			}
			beans.clear();
		}
	}

	@SuppressWarnings("unchecked")
	public static final <T> T getOrCreateBean(Class<T> clasz) {
		BeanFactory<T> factory = (BeanFactory<T>)knownBeansFactories.get(clasz);
		if (factory==null) {
			factory = new BeanFactory<T>() {

				@Override
				public T getOrCreateBean(Class<T> fieldClass, Class<?> injectedIntoClass) throws SecurityException, NoSuchMethodException, IllegalArgumentException, InstantiationException, IllegalAccessException, InvocationTargetException {
					return ReflectionHelper.newInstance(fieldClass);
				}
			};
		}
		return getOrCreateBean(clasz, factory);
	}

	@SuppressWarnings("unchecked")
	public static final <T> T getOrCreateBean(Class<T> clasz, BeanFactory<T> defaultInstanceFactory) {
		try {
			synchronized (beansCreationMutex) {
				Object ret = beans.get(clasz);
				if (ret==null) {
					ret = createBean(clasz, defaultInstanceFactory);
					beans.put(clasz, ret);
					initBean(ret);
				}
				return (T)ret;
			}
		} catch (Exception e) {
			throw new RuntimeException("Unable to create bean of class "+clasz.toString(),e);
		}
	}
	private static final <T> T createBean(Class<T> clasz, BeanFactory<T> defaultFactory) throws SecurityException, NoSuchMethodException, IllegalArgumentException, InstantiationException, IllegalAccessException, InvocationTargetException {
		T ret = attemptToCreateKnownBean(clasz, BeansFactory.class);
		if (ret!=null) {
			return ret;
		} else {
			return defaultFactory.getOrCreateBean(clasz, BeansFactory.class);
		}
	}
	private static final <T> T attemptToCreateKnownBean(Class<T> fieldClass, Class<?> beanClass) throws SecurityException, NoSuchMethodException, IllegalArgumentException, InstantiationException, IllegalAccessException, InvocationTargetException {
		for (Map.Entry<Class<?>, BeanFactory<?>> entry:knownBeansFactories.entrySet()) {
			Class<?> resolverClass = entry.getKey();
			if (resolverClass.isAssignableFrom(fieldClass)) {
				@SuppressWarnings("unchecked")
				BeanFactory<T> factory = (BeanFactory<T>)entry.getValue(); 
				return factory.getOrCreateBean(fieldClass, beanClass);
			}
		}
		return null;
	}

	private static final void initBean(Object bean) throws IllegalArgumentException, IllegalAccessException, SecurityException, NoSuchMethodException, InvocationTargetException, ClassNotFoundException, NoSuchFieldException, InstantiationException {
		for (Field field:ReflectionHelper.getFields(bean)) {
			Annotation inject = field.getAnnotation(Inject.class);
			Class<?> fieldClass = field.getType();
			if (inject!=null) {
				Object beanToInject = attemptToCreateKnownBean(fieldClass, bean.getClass());
				if (beanToInject==null) {
					beanToInject = getOrCreateBean(fieldClass);
				}
				ReflectionHelper.setField(bean, field.getName(), beanToInject);
			}

		}
		for (Method method:bean.getClass().getDeclaredMethods()) {
			Annotation postConstruct = method.getAnnotation(PostConstruct.class);
			if (postConstruct!=null) {
				ReflectionHelper.evaluateMethod(bean, method.getName(), null);
			}
		}
	}
}
