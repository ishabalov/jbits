package org.jbits.os;

import java.io.IOException;
import java.net.InetAddress;

public class Os {
	
	public static final String HOST_NAME = initHostName();
	
	private static String initHostName() {
		try {
			return InetAddress.getLocalHost().getCanonicalHostName();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}		
	}
	
}
