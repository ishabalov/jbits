package org.jbits.linux;

public interface DeviceStatValue {
	long get(DeviceStats s);
}
