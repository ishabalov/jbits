package org.jbits.linux;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;

public final class ProcHelper {

	public static final BufferedReader readProcFile(String fileName) throws IOException {
		RandomAccessFile rf=null;
		ByteArrayOutputStream buf = new ByteArrayOutputStream();
		try {
			File in = new File(fileName);
			rf = new RandomAccessFile(in, "r");
			byte b[] = new byte[1000];
			int read = rf.read(b);
			while (read>0) {
				buf.write(b,0,read);
				read = rf.read(b);
			}
		} finally {
			if (rf!=null) {
				rf.close();
			}
		}
		buf.close();
		ByteArrayInputStream is = new ByteArrayInputStream(buf.toByteArray());
		return new BufferedReader(new InputStreamReader(is));
	}

	public static final String hostname() {
		try {
			return readProcFile("/proc/sys/kernel/hostname").readLine().trim();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	private static final String readProcStatLine() throws IOException {
		return readProcFile("/proc/stat").readLine().trim();
	}

	private static final class CpuStat {
		private long total1;
		private long total2;
		private long idle1;
		private long idle2;
		private long iowait1;
		private long iowait2;

		private CpuStat() {
			try {
				String l1 = readProcStatLine().replaceAll("cpu +", "").trim();
				Thread.sleep(1000);
				String l2 = readProcStatLine().replaceAll("cpu +", "").trim();;
				String d1[] = l1.split(" ");
				String d2[] = l2.split(" ");
				total1=0L;
				total2=0L;
				for (String l:d1) {
					total1+=Long.valueOf(l);
				}
				for (String l:d2) {
					total2+=Long.valueOf(l);
				}
				idle1 = Long.valueOf(d1[3]);
				idle2 = Long.valueOf(d2[3]);
				iowait1 = Long.valueOf(d1[4]);
				iowait2 = Long.valueOf(d2[4]);
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
		
		
		private float cpuUsage() {
			long unused = idle2-idle1+iowait2-iowait1;
			long used = total2-total1-unused;
			return ((float)used)/(float)(used+unused);
		}
		private float ioWait() {
			long iowait = iowait2-iowait1;
			long total = total2-total1;
			return ((float)iowait)/(float)(total);
		}
	}
	
	public static final float cpuUsage() {
		return new CpuStat().cpuUsage();
	}

	public static final float ioWait() {
		return new CpuStat().ioWait();
	}

	public static final float memoryUsage() {
		try {
			BufferedReader r = readProcFile("/proc/meminfo");
			long total=-1;
			long free=-1;
			long buffers=-1;
			long cached=-1;
			String line = r.readLine();
			while (line!=null && (total==-1 || free==-1 || buffers==-1 || cached==-1)) {
				String split[]=line.split(" +");
				if ("MemTotal:".equals(split[0])) {
					total = Long.valueOf(split[1]);
				} else if ("MemFree:".equals(split[0])) {
					free = Long.valueOf(split[1]);
				} else if ("Buffers:".equals(split[0])) {
					buffers = Long.valueOf(split[1]);
				} else if ("Cached:".equals(split[0])) {
					cached = Long.valueOf(split[1]);
				}
				line = r.readLine();
			}
			if (total>=0 && free>=0 && buffers>=0 && cached>=0) {
				return 1.0F - (float)(free+buffers+cached)/(float)total;
			} else {
				throw new RuntimeException("unable to obtain memory usage");
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public static final float load() {
		try {
			BufferedReader r = readProcFile("/proc/stat");
			int procs=-1;
			String line = r.readLine();
			while (line!=null && (procs==-1)) {
				String split[]=line.split(" +");
				if ("procs_running".equals(split[0])) {
					procs = Integer.valueOf(split[1]);
				}
				line = r.readLine();
			}
			if (procs>=0) {
				return (float)procs;
			} else {
				throw new RuntimeException("unable to obtain memory usage");
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public static final long uptime() {
		try {
			BufferedReader r = readProcFile("/proc/stat");
			long btime=-1;
			String line = r.readLine();
			while (line!=null && (btime==-1)) {
				String split[]=line.split(" +");
				if ("btime".equals(split[0])) {
					btime = Long.valueOf(split[1]);
				}
				line = r.readLine();
			}
			if (btime>=0) {
				return System.currentTimeMillis()/1000-btime;
			} else {
				throw new RuntimeException("unable to obtain memory usage");
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public static final DiskStats diskstats(String disk) {
		try {
			BufferedReader r = readProcFile("/proc/diskstats");
			String line = r.readLine();
			while (line!=null) {
				DiskStats ret = DiskStats.parse(line);
				if (disk.endsWith(ret.getDeviceName())) {
					return ret;
				}
				line = r.readLine();
			}
			return null;
		} catch (IOException e) {
			throw new RuntimeException(e);
		}		
	}


	public static final NetStats netstats(String interfaceName) {
		try {
			BufferedReader r = readProcFile("/proc/net/dev");
			String line = r.readLine();
			while (line!=null) {
				if (!line.contains("|")) {
					NetStats ret = NetStats.parse(line);
					if (interfaceName.equalsIgnoreCase(ret.getDeviceName())) {
						return ret;
					}
				}
				line = r.readLine();
			}
			return null;
		} catch (IOException e) {
			throw new RuntimeException(e);
		}		
	}
}