package org.jbits.linux;

import org.jbits.text.TextHelper;

public final class DiskStats extends DeviceStats {
	private int major;
	private int minor;
	private long readsCompleted;
	private long readsMerged;
	private long readsSectors;
	private long readsMilliseconds;
	private long writesCompleted;
	private long writesSectors;
	private long writesMilliseconds;
	private long ioCurrentlyInProgress;
	private long ioMilliseconds;
	private long weightedIoMilliseconds;
	private DiskStats() {};
	
	protected static final DiskStats parse(String data) {
		if (!TextHelper.isNullOrEmpty(data)) {
			DiskStats ret = new DiskStats();
			String sp[] = data.trim().split(" +");
			int i=0;
			if (sp.length>i && !TextHelper.isNullOrEmpty(sp[i])) {
				ret.major=Integer.valueOf(sp[i]);
			}
			i++;
			if (sp.length>i && !TextHelper.isNullOrEmpty(sp[i])) {
				ret.minor=Integer.valueOf(sp[i]);
			}
			i++;
			if (sp.length>i && !TextHelper.isNullOrEmpty(sp[i])) {
				ret.deviceName=sp[i];
			}
			i++;
			if (sp.length>i && !TextHelper.isNullOrEmpty(sp[i])) {
				ret.readsCompleted=Long.valueOf(sp[i]);
			}
			i++;
			if (sp.length>i && !TextHelper.isNullOrEmpty(sp[i])) {
				ret.readsMerged=Long.valueOf(sp[i]);
			}
			i++;
			if (sp.length>i && !TextHelper.isNullOrEmpty(sp[i])) {
				ret.readsSectors=Long.valueOf(sp[i]);
			}
			i++;
			if (sp.length>i && !TextHelper.isNullOrEmpty(sp[i])) {
				ret.readsMilliseconds=Long.valueOf(sp[i]);
			}
			i++;
			if (sp.length>i && !TextHelper.isNullOrEmpty(sp[i])) {
				ret.writesCompleted=Long.valueOf(sp[i]);
			}
			i++;i++;
			if (sp.length>i && !TextHelper.isNullOrEmpty(sp[i])) {
				ret.writesSectors=Long.valueOf(sp[i]);
			}
			i++;
			if (sp.length>i && !TextHelper.isNullOrEmpty(sp[i])) {
				ret.writesMilliseconds=Long.valueOf(sp[i]);
			}
			i++;
			if (sp.length>i && !TextHelper.isNullOrEmpty(sp[i])) {
				ret.ioCurrentlyInProgress=Long.valueOf(sp[i]);
			}
			i++;
			if (sp.length>i && !TextHelper.isNullOrEmpty(sp[i])) {
				ret.ioMilliseconds=Long.valueOf(sp[i]);
			}
			i++;
			if (sp.length>i && !TextHelper.isNullOrEmpty(sp[i])) {
				ret.weightedIoMilliseconds=Long.valueOf(sp[i]);
			}
			return ret;
		} else {
			return null;
		}
	}


	public int getMajor() {
		return major;
	}
	public int getMinor() {
		return minor;
	}
	public long getReadsCompleted() {
		return readsCompleted;
	}
	public long getReadsMerged() {
		return readsMerged;
	}
	public long getReadsSectors() {
		return readsSectors;
	}
	public long getReadsMilliseconds() {
		return readsMilliseconds;
	}
	public long getWritesCompleted() {
		return writesCompleted;
	}
	public long getWritesSectors() {
		return writesSectors;
	}
	public long getWritesMilliseconds() {
		return writesMilliseconds;
	}
	public long getIoCurrentlyInProgress() {
		return ioCurrentlyInProgress;
	}
	public long getIoMilliseconds() {
		return ioMilliseconds;
	}
	public long getWeightedIoMilliseconds() {
		return weightedIoMilliseconds;
	}
}

