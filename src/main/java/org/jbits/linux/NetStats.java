package org.jbits.linux;

import org.jbits.text.TextHelper;

public class NetStats extends DeviceStats {
	private long receiveBytes;
	private long receivePackets;
	private long receiveErrors;
	private long receiveDrops;
	private long receiveFifo;
	private long receiveFrames;
	private long receiveCompressed;
	private long receiveMulticast;
	private long transmitBytes;
	private long transmitPackets;
	private long transmitErrors;
	private long transmitDrops;
	private long transmitFifo;
	private long transmitCollisions;
	private long transmitCarrier;
	private long transmitCompressed;
	private NetStats() {};

	protected static final NetStats parse(String data) {
		if (!TextHelper.isNullOrEmpty(data)) {
			NetStats ret = new NetStats();
			String sp[] = data.trim().split("[\t :]+");
			int i=0;
			if (sp.length>i && !TextHelper.isNullOrEmpty(sp[i])) {
				ret.deviceName=sp[i];
			}
			i++;
			if (sp.length>i && !TextHelper.isNullOrEmpty(sp[i])) {
				ret.receiveBytes=Long.valueOf(sp[i]);
			}
			i++;
			if (sp.length>i && !TextHelper.isNullOrEmpty(sp[i])) {
				ret.receivePackets=Long.valueOf(sp[i]);
			}
			i++;
			if (sp.length>i && !TextHelper.isNullOrEmpty(sp[i])) {
				ret.receiveErrors=Long.valueOf(sp[i]);
			}
			i++;
			if (sp.length>i && !TextHelper.isNullOrEmpty(sp[i])) {
				ret.receiveDrops=Long.valueOf(sp[i]);
			}
			i++;
			if (sp.length>i && !TextHelper.isNullOrEmpty(sp[i])) {
				ret.receiveFifo=Long.valueOf(sp[i]);
			}
			i++;
			if (sp.length>i && !TextHelper.isNullOrEmpty(sp[i])) {
				ret.receiveFrames=Long.valueOf(sp[i]);
			}
			i++;
			if (sp.length>i && !TextHelper.isNullOrEmpty(sp[i])) {
				ret.receiveCompressed=Long.valueOf(sp[i]);
			}
			i++;
			if (sp.length>i && !TextHelper.isNullOrEmpty(sp[i])) {
				ret.receiveMulticast=Long.valueOf(sp[i]);
			}
			i++;
			if (sp.length>i && !TextHelper.isNullOrEmpty(sp[i])) {
				ret.transmitBytes=Long.valueOf(sp[i]);
			}
			i++;
			if (sp.length>i && !TextHelper.isNullOrEmpty(sp[i])) {
				ret.transmitPackets=Long.valueOf(sp[i]);
			}
			i++;
			if (sp.length>i && !TextHelper.isNullOrEmpty(sp[i])) {
				ret.transmitErrors=Long.valueOf(sp[i]);
			}
			i++;
			if (sp.length>i && !TextHelper.isNullOrEmpty(sp[i])) {
				ret.transmitDrops=Long.valueOf(sp[i]);
			}
			i++;
			if (sp.length>i && !TextHelper.isNullOrEmpty(sp[i])) {
				ret.transmitFifo=Long.valueOf(sp[i]);
			}
			i++;
			if (sp.length>i && !TextHelper.isNullOrEmpty(sp[i])) {
				ret.transmitCollisions=Long.valueOf(sp[i]);
			}
			i++;
			if (sp.length>i && !TextHelper.isNullOrEmpty(sp[i])) {
				ret.transmitCarrier=Long.valueOf(sp[i]);
			}
			i++;
			if (sp.length>i && !TextHelper.isNullOrEmpty(sp[i])) {
				ret.transmitCompressed=Long.valueOf(sp[i]);
			}
			return ret;
		} else {
			return null;
		}

	}
	public long getReceiveBytes() {
		return receiveBytes;
	}
	public long getReceivePackets() {
		return receivePackets;
	}
	public long getReceiveErrors() {
		return receiveErrors;
	}
	public long getReceiveDrops() {
		return receiveDrops;
	}
	public long getReceiveFifo() {
		return receiveFifo;
	}
	public long getReceiveFrames() {
		return receiveFrames;
	}
	public long getReceiveCompressed() {
		return receiveCompressed;
	}
	public long getReceiveMulticast() {
		return receiveMulticast;
	}
	public long getTransmitBytes() {
		return transmitBytes;
	}
	public long getTransmitPackets() {
		return transmitPackets;
	}
	public long getTransmitErrors() {
		return transmitErrors;
	}
	public long getTransmitDrops() {
		return transmitDrops;
	}
	public long getTransmitFifo() {
		return transmitFifo;
	}
	public long getTransmitCollisions() {
		return transmitCollisions;
	}
	public long getTransmitCarrier() {
		return transmitCarrier;
	}
	public long getTransmitCompressed() {
		return transmitCompressed;
	}
}

