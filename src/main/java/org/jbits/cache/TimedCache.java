package org.jbits.cache;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.jbits.time.TimeInterval;

public class TimedCache<K,T> implements Map<K,T> {
	private static final class Data<T> {
		private T data=null;
		private long expirationTime=0;
		private Data (T data, long expirationTimeout) {
			this.data = data;
			this.expirationTime = System.currentTimeMillis()+expirationTimeout;
		}
		private T get() {
			if (System.currentTimeMillis()<expirationTime) {
				return data;
			} else {
				return null;
			}
		}
		@Override
		public int hashCode() {
			return data.hashCode();
		}
		@Override
		public boolean equals(Object obj) {
			if (data!=null) {
				return data.equals(obj);
			} else {
				return false;
			}
		}
		
	}
	private class Entry implements Map.Entry<K, T> {
		private K key;
		private T value;
		private Entry(K key, T value) {
			this.key = key;
			this.value = value;
		}

		@Override
		public K getKey() {
			return key;
		}

		@Override
		public T getValue() {
			return value;
		}

		@Override
		public T setValue(T value) {
			T oldValue = this.value;
			this.value=value;
			return oldValue;
		}
		
	}
	
	private Map<K,Data<T>> delegate;
	private long timeout = Long.MAX_VALUE;
	public TimedCache(long timeout) {
		this.timeout=timeout;
		this.delegate=new ConcurrentHashMap<K, Data<T>>();
	}
	public TimedCache(TimeInterval timeout) {
		this.timeout=timeout.milliseconds();
		this.delegate=new ConcurrentHashMap<K, Data<T>>();
	}
	
	@Override
	public void clear() {
		delegate.clear();
	}

	@Override
	public boolean containsKey(Object key) {
		return delegate.containsKey(key);
	}

	@Override
	public boolean containsValue(Object value) {
		return delegate.containsValue(value);
	}

	@Override
	public Set<java.util.Map.Entry<K, T>> entrySet() {
		Set<java.util.Map.Entry<K, T>> ret = new HashSet<java.util.Map.Entry<K,T>>();
		for (Map.Entry<K,Data<T>> item:delegate.entrySet()) {
			T data = item.getValue().get();
			if (data!=null) {
				ret.add(new Entry(item.getKey(),data));
			}
		}
		return ret;
	}

	@Override
	public T get(Object key) {
		Data<T> data = delegate.get(key);
		return data!=null?data.get():null;
	}

	@Override
	public boolean isEmpty() {
		return delegate.isEmpty();
	}

	@Override
	public Set<K> keySet() {
		return delegate.keySet();
	}

	@Override
	public T put(K key, T value) {
		Data<T> data = new Data<T>(value, timeout);
		Data<T> oldValue = delegate.put(key, data); 
		return oldValue!=null?oldValue.get():null;
	}

	@Override
	public void putAll(Map<? extends K, ? extends T> m) {
		for (java.util.Map.Entry<? extends K, ? extends T> entry:m.entrySet()) {
			put(entry.getKey(),entry.getValue());
		}
	}

	@Override
	public T remove(Object key) {
		Data<T> ret = delegate.remove(key); 
		return ret!=null?ret.get():null;
	}

	@Override
	public int size() {
		return delegate.size();
	}

	@Override
	public Collection<T> values() {
		Collection<T> ret = new ArrayList<T>();
		for (java.util.Map.Entry<K, T> entry:entrySet()) {
			ret.add(entry.getValue());
		}
		return ret;
	}
	
}
