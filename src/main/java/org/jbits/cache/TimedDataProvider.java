package org.jbits.cache;

import org.jbits.time.TimeInterval;

public  class  TimedDataProvider<T> {
	private T data;
	private long expirationTime=Long.MIN_VALUE;
	private TimeInterval timeout;
	private DataProvider<T> provider;
	
	public TimedDataProvider (TimeInterval timeout, DataProvider<T> provider) {
		this.timeout=timeout;
		this.provider=provider;
	}
	
	public T get() {
		if (System.currentTimeMillis()>expirationTime) {
			data = provider.provide();
			resetTimer();
		}
		return data;
	}
	public void resetTimer() {
		expirationTime=System.currentTimeMillis()+timeout.milliseconds();
	}

	public void clean() {
		expirationTime=Long.MIN_VALUE;
		data=null;
	}
	public boolean isExpired() {
		return System.currentTimeMillis()>expirationTime;
	}
}
