package org.jbits.cache;

import org.jbits.time.TimeInterval;

public  class  TimedData<T> {
	private T data;
	private long expirationTime=Long.MIN_VALUE;
	private TimeInterval timeout;

	public TimedData (TimeInterval timeout, T data) {
		this.timeout=timeout;
		this.data = data;
	}
	
	public TimedData (TimeInterval timeout) {
		this.timeout=timeout;
	}
	
	public T get() {
		if (System.currentTimeMillis()>expirationTime) {
			return null;
		} else {
			return data;
		}
	}
	public void set(T data) {
		this.data=data;
		expirationTime=System.currentTimeMillis()+timeout.milliseconds();
	}
	public void resetTimer() {
		expirationTime=System.currentTimeMillis()+timeout.milliseconds();
	}

	public T clean() {
		expirationTime=Long.MIN_VALUE;
		T oldData = data;
		data=null;
		return oldData;
	}
	public boolean isExpired() {
		return System.currentTimeMillis()>expirationTime;
	}
}
