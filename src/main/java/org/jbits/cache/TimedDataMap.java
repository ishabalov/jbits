package org.jbits.cache;

import java.util.HashMap;
import java.util.Map;

import org.jbits.time.TimeInterval;

public class  TimedDataMap <K,V> {
	private Map<K,TimedDataProvider<V>> cache = new HashMap<K, TimedDataProvider<V>>();

	public V put(K key, DataProvider<V> dataProvider, TimeInterval timeout) {
		TimedDataProvider<V> oldData = cache.put(key, new TimedDataProvider<V>(timeout,dataProvider));
		if (oldData!=null) {
			return oldData.get();
		} else {
			return null;
		}
	}

	public V get(K key) {
		TimedDataProvider<V> data = cache.get(key);
		if (data!=null) {
			return data.get();
		} else {
			return null;
		}
	}

	public V getOrPutAndGet(K key, DataProvider<V> dataProvider, TimeInterval timeout) {
		TimedDataProvider<V> data = cache.get(key);
		if (data==null) {
			synchronized(this) {
				data = cache.get(key);
				if (data==null) {
					data = new TimedDataProvider<V>(timeout,dataProvider);
					cache.put(key,data);
					return data.get();
				} else {
					return data.get();
				}
			}
		} else {
			return data.get();
		}
	}

}
