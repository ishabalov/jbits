package org.jbits.cache;

public interface DataProvider<T> {
	T provide();
}
