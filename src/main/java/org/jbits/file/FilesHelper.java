package org.jbits.file;

import java.io.IOException;
import java.nio.file.FileVisitOption;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

public final class FilesHelper {
	public static final List<Path> allFiles(final Path directory) throws IOException {
		final List<Path> ret = new ArrayList<>();
		Files.walkFileTree(directory, EnumSet.noneOf(FileVisitOption.class), 1, new SimpleFileVisitor<Path>(){
			
			@Override
			public FileVisitResult visitFile(Path file,	BasicFileAttributes attrs) throws IOException {
				ret.add(file);
				return FileVisitResult.CONTINUE;
			}
			
		});
		return ret;
	}
	public static final boolean isRegularReadableExistingFile(Path file, LinkOption ... linkOptions) {
		return Files.exists(file,linkOptions) && Files.isRegularFile(file, linkOptions) && Files.isReadable(file);
	}
}
