package org.jbits.circularbuffer;

public class CircularBufferHeader {
	public static final long HEADER_SIZE =  Long.SIZE*3/Byte.SIZE;
	private long minor;
	private long major;
	private long base;
	protected CircularBufferHeader(long minor, long major, long base) {
		this.minor = minor;
		this.major = major;
		this.base = base;
	}
	public long getMinor() {
		return minor;
	}
	public long getMajor() {
		return major;
	}
	public long getBase() {
		return base;
	}
}
