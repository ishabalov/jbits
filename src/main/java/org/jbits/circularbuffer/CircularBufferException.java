package org.jbits.circularbuffer;

public class CircularBufferException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 656306407405082871L;

	public CircularBufferException(String message, Throwable cause) {
		super(message, cause);
	}

	public CircularBufferException(String message) {
		super(message);
	}

}
