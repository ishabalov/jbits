package org.jbits.circularbuffer;

import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Path;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.jbits.time.TimeInterval;

public class CircularBufferGroup<T extends MergeableStatDataRecord<V>,V> implements Runnable {
	
	private CircularBufferGroupEntry<T,V> topEntry;
	private Logger logger=null;
	
	public void put(V value) throws IOException, IllegalArgumentException, IllegalAccessException, InstantiationException, CircularBufferException, InvocationTargetException {
		put(value, System.currentTimeMillis());
	}

	public void put(V value, long timestamp) throws IllegalArgumentException, IllegalAccessException, InstantiationException, IOException, CircularBufferException, InvocationTargetException {
		if (topEntry!=null) {
			topEntry.put(value, timestamp);
		}
	}
	
	public int flush() throws IllegalArgumentException, IllegalAccessException, IOException, InstantiationException {
		int ret = 0;
		if (topEntry!=null) {
			long now = System.currentTimeMillis();
			ret = topEntry.flush(now);
		}
		return ret;
	}

	public void close() throws IllegalArgumentException, IllegalAccessException, IOException, InstantiationException {
		if (topEntry!=null) {
			long now = System.currentTimeMillis();
			topEntry.close(now);
		}
	}

	
	public Collection<TimestampedRecord<T>> getRecordsForInterval(long from, long to, long now) throws IllegalArgumentException, IOException, IllegalAccessException, InstantiationException {
		if (topEntry!=null) {
			return topEntry.getRecordsForInterval(from, to, now);
		} else {
			throw new IllegalStateException("No circular buffers in the group"); 
		}
	}

	public long getMinMinor() {
		if (topEntry!=null) {
			return topEntry.getMinor();
		} else {
			throw new IllegalStateException("No circular buffers in the group"); 
		}
	}
	
	public static <T extends MergeableStatDataRecord<V>,V> CircularBufferGroup<T,V> openOrCreateCircularBufferGroup(
			Class<T> clasz, 
			Path folder, 
			String name, 
			long major, 
			long minor, 
			long base, 
			int multiplier, 
			int levels, 
			TimeInterval topLevelTimeout
		) throws IOException, InstantiationException, IllegalAccessException, NoSuchMethodException, SecurityException, IllegalArgumentException, InvocationTargetException {
		CircularBufferGroup<T,V> ret = new CircularBufferGroup<>();
		long currentMajor = major;
		long currentMinor = minor;
		CircularBufferGroupEntry<T,V> prev = null;
		for (int level=0;level<levels;level++) {
			Path levelPath = folder.resolve(name+"."+level+".cb");
			CircularBufferGroupEntry<T,V> current = CircularBufferGroupEntry.openOrCreateCircularBufferGroupEntry(clasz, levelPath, currentMajor, currentMinor, base);
			if (ret.topEntry==null) {
				ret.topEntry=current;
				current.setFlushTimeout(topLevelTimeout);
			} else {
				current.setFlushTimeout(TimeInterval.valueOf(currentMinor));
			}
			if (prev!=null) {
				prev.setNext(current);
				current.setPrev(prev);
			}
			prev=current;
			currentMajor = currentMajor * multiplier;
			currentMinor = currentMinor * multiplier;
		}
		return ret;
	}
	
	@Override
	public void run() {
		try {
			flush();
		} catch (Exception e) {
			if (logger!=null) {
				logger.log(Level.SEVERE,"Exception while circular buffer flush",e);
			}
		}
	}
	
	public void dump(PrintWriter writer) throws IOException {
		if (topEntry!=null) {
			topEntry.dump(writer);
		}
	}

}
