package org.jbits.circularbuffer;

public class TimestampedRecord<T> {
	private long timestamp;
	private long intervalBegin;
	private long intervalEnd;
	private T record;
	public TimestampedRecord(long intervalBegin, long timestamp, long intervalEnd, T record) {
		this.intervalBegin=intervalBegin;
		this.timestamp = timestamp;
		this.intervalEnd=intervalEnd;
		this.record = record;
	}
	public long getTimestamp() {
		return timestamp;
	}
	public long getIntervalBegin() {
		return intervalBegin;
	}
	public long getIntervalEnd() {
		return intervalEnd;
	}
	public T getRecord() {
		return record;
	}
}
