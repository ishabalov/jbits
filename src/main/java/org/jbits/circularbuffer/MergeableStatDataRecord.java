package org.jbits.circularbuffer;

import java.io.IOException;
import java.io.PrintWriter;

public interface MergeableStatDataRecord<V> {
	void reset();
	void put(V value);
	void merge(MergeableStatDataRecord<V> other);
	MergeableStatDataRecord<V> copy();
	void dump(PrintWriter writer) throws IOException;
	public double getRelativeCounterFor(V s);
	public int getAbsoluteCounterFor(V s);
}
