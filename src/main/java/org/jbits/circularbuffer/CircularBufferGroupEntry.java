package org.jbits.circularbuffer;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.nio.ByteBuffer;
import java.nio.channels.SeekableByteChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import org.jbits.text.TextHelper;
import org.jbits.time.TimeInterval;
import org.jbits.time.TimeIntervalFormat;

public class CircularBufferGroupEntry<T extends MergeableStatDataRecord<V>,V> {

	private static final class DataElement<V> {
		private MergeableStatDataRecord<V> data;
		private long timestamp;
		private boolean isDirty=false;
		private DataElement(MergeableStatDataRecord<V> data, long timestamp) {
			this.data = data;
			this.timestamp = timestamp;
		}
		private void dump(PrintWriter writer) throws IOException {
			if (isDirty) {
				writer.print("D ");
			} else {
				writer.print("- ");
			}
			printTimestamp(writer, null, timestamp);
			writer.print(' ');
			data.dump(writer);
		}
	}

	private DataElement<V>[] data;
	private CircularBufferGroupEntry<T,V> next;
	private CircularBufferGroupEntry<T,V> prev;

	private CircularBufferHeader header;
	private Class<? extends T> recordClass;
	private Constructor<? extends T> recordDefaultConstructor;
	private TimeInterval flushTimeout;
	private ReentrantReadWriteLock lock = new ReentrantReadWriteLock();
	private Path path;
	private boolean usePutLog = false;

	private CircularBufferGroupEntry() {}

	protected void put(V value, long timestamp) throws IOException, IllegalArgumentException, IllegalAccessException, InstantiationException, CircularBufferException, InvocationTargetException {
		long delta = System.currentTimeMillis()-timestamp;
		if (delta>=0) {
			if (delta<header.getMajor()) {
				int index = calculatePositionIndex(timestamp);
				putDataElement(index, value, timestamp);
			} else if (next!=null) {
				next.put(value, timestamp);
			}
		}
	}


	protected void setNext(CircularBufferGroupEntry<T,V> next) {
		this.next=next;
	}

	protected void setPrev(CircularBufferGroupEntry<T,V> prev) {
		this.prev=prev;
	}

	protected void setFlushTimeout(TimeInterval flushTimeout) {
		this.flushTimeout=flushTimeout;
	}

	private boolean needMergeFromPrev() {
		long now = System.currentTimeMillis();
		int startIndex = calculatePositionIndex(now);
		long barrier = now - flushTimeout.milliseconds();
		for (int index=data.length-1;index>=0;index--) {
			int effectiveInndex = (startIndex+index)%data.length;
			DataElement<V> element = data[effectiveInndex];
			if (element!=null) {
				if (element.timestamp>=barrier) {
					return false;
				}
			}
		}
		return true;
	}

	protected int flush(long now) throws IOException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		int savedHere = innerFlush(now);
		int savedNext = 0;
		if (next!=null) {
			savedNext = next.flush(now);
		}
		return savedHere + savedNext;
	}

	protected void close(long now) throws IOException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		if (next!=null) {
			next.close(now);
		}
		innerFlush(now);
	}


	private int innerFlush(long now) throws IOException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		lock.writeLock().lock();
		if (usePutLog) {
			appendPutLog("flush started");
		}
		try {
			if (prev!=null && needMergeFromPrev()) {
				if (usePutLog) {
					appendPutLog("merge records from previous");
				}
				mergeRecordsFromPrev(now);
			}
			int ret = saveDirtyRecordsToDisk();
			if (usePutLog) {
				appendPutLog("flush done");
			}
			return ret;
		} finally {
			lock.writeLock().unlock();
		}
	}

	@SuppressWarnings("unchecked")
	private int saveDirtyRecordsToDisk() throws IOException, IllegalArgumentException, IllegalAccessException {
		SeekableByteChannel channel = null;
		int numberOfDirtyBuffersSaved = 0;
		try {
			ByteBuffer recordBuffer = ByteBuffer.allocate(calculateFieldSize());
			for (int index=0;index<data.length;index++) {
				DataElement<V> element = data[index];
				if (element!=null && element.isDirty) {
					if (channel==null) {
						channel = openChannelForWrite();
					}
					recordBuffer.position(0);
					recordBuffer.putLong(element.timestamp);
					saveRecord(recordBuffer, (T)element.data);
					recordBuffer.flip();
					long filePosition = calculateFilePositionByIndexOfRecord(index);
					long currentPosition = channel.position();
					if (currentPosition!=filePosition) {
						channel.position(filePosition);
					}
					channel.write(recordBuffer);
					numberOfDirtyBuffersSaved++;
					element.isDirty=false;
				}
			}
			return numberOfDirtyBuffersSaved;
		} finally {
			if (channel!=null) {
				channel.close();
			}
		}
	}

	private void saveRecord(ByteBuffer buf, T record) throws IllegalArgumentException, IllegalAccessException {
		for (Field field:getFields()) {
			Class<?> fieldClass = field.getType();
			if (!fieldClass.isArray()) {
				putField(buf, fieldClass, record, field);
			} else {
				ArrayMetadata arrayMetadata = field.getAnnotation(ArrayMetadata.class);
				if (arrayMetadata==null) {
					throw new IllegalArgumentException("expected array size is not defined for array field "+field.getName());
				}
				int actualSize = Array.getLength(field.get(record)); 
				if (arrayMetadata.size()!=actualSize) {
					throw new IllegalArgumentException("expected array size "+arrayMetadata.size()+" is not equal to actual array size "+actualSize+" for array "+recordClass.getName()+"."+field.getName());
				}
				Class<?> componentClass = arrayMetadata.componentType();
				for (int index=0;index<arrayMetadata.size();index++) {
					if (boolean.class.equals(componentClass)) {
						if (Array.getBoolean(field.get(record),index)) {
							buf.put((byte)-1);
						} else {
							buf.put((byte)0);
						}
					} else if (float.class.equals(componentClass)) {
						buf.putFloat(Array.getFloat(field.get(record),index));
					} else if (double.class.equals(componentClass)) {
						buf.putDouble(Array.getDouble(field.get(record),index));
					} else if (byte.class.equals(componentClass)) {
						buf.put(Array.getByte(field.get(record),index));
					} else if (short.class.equals(componentClass)) {
						buf.putShort(Array.getShort(field.get(record),index));
					} else if (int.class.equals(componentClass)) {
						Object r = field.get(record);
						int v = Array.getInt(r,index);
						buf.putInt(v);
					} else if (long.class.equals(componentClass)) {
						buf.putLong(Array.getLong(field.get(record),index));
					} else {
						throw new IllegalArgumentException("unsupported component type: "+componentClass.getName()+" for field "+field.getName());
					}
				}
			}

		}
	}

	private volatile Field[] fieldsCache = null;

	private Field[] getFields() {
		if (fieldsCache!=null) {
			return fieldsCache;
		} else {
			synchronized (CircularBufferGroupEntry.class) {
				if (fieldsCache!=null) {
					return fieldsCache;
				} else {
					List<Field> list = new ArrayList<Field>();
					for (Field field:recordClass.getDeclaredFields()) {
						if (!Modifier.isTransient(field.getModifiers())) {
							field.setAccessible(true);
							list.add(field);
						}
					}
					Collections.sort(list, new Comparator<Field>() {
						@Override
						public int compare(Field o1, Field o2) {
							return o1.getName().compareTo(o2.getName());
						}
					});
					fieldsCache = list.toArray(new Field[list.size()]);
					return fieldsCache;
				}
			}
		}
	}

	private void putField(ByteBuffer buf, Class<?> fieldClass, T record, Field field) throws IllegalArgumentException, IllegalAccessException {
		if (boolean.class.equals(fieldClass)) {
			if (field.getBoolean(record)) {
				buf.put((byte)-1);
			} else {
				buf.put((byte)0);
			}
		} else if (float.class.equals(fieldClass)) {
			buf.putFloat(field.getFloat(record));
		} else if (double.class.equals(fieldClass)) {
			buf.putDouble(field.getDouble(record));
		} else if (byte.class.equals(fieldClass)) {
			buf.put(field.getByte(record));
		} else if (short.class.equals(fieldClass)) {
			buf.putShort(field.getShort(record));
		} else if (int.class.equals(fieldClass)) {
			buf.putInt(field.getInt(record));
		} else if (long.class.equals(fieldClass)) {
			buf.putLong(field.getLong(record));
		} else {
			throw new IllegalArgumentException("unsupported field type: "+fieldClass.getName()+" for field "+field.getName());
		}
	}

	private volatile int feildSizeCache = -1;

	private int calculateFieldSize() {
		if (feildSizeCache>=0) {
			return feildSizeCache;
		} else {
			synchronized (CircularBufferGroupEntry.class) {
				if (feildSizeCache>=0) {
					return feildSizeCache;
				} else {
					feildSizeCache = calculateRecordSize()+Long.SIZE/Byte.SIZE;
					return feildSizeCache;
				}
			}
		}
	}

	private int getTypeSize(Class<?> clasz, String name) {
		if (boolean.class.equals(clasz)) {
			return 1;
		} else if (float.class.equals(clasz)) {
			return Float.SIZE/Byte.SIZE;
		} else if (double.class.equals(clasz)) {
			return Double.SIZE/Byte.SIZE;
		} else if (byte.class.equals(clasz)) {
			return Byte.SIZE/Byte.SIZE;
		} else if (short.class.equals(clasz)) {
			return Short.SIZE/Byte.SIZE;
		} else if (int.class.equals(clasz)) {
			return Integer.SIZE/Byte.SIZE;
		} else if (long.class.equals(clasz)) {
			return Long.SIZE/Byte.SIZE;
		} else {
			throw new IllegalArgumentException("unsupported field type: "+clasz.getName()+" for field "+name);
		}
	}

	private int calculateRecordSize() {
		int ret = 0;
		for (Field field:getFields()) {
			Class<?> fieldClass = field.getType();
			if (!fieldClass.isArray()) {
				ret+=getTypeSize(fieldClass, field.getName());
			} else  {
				ArrayMetadata arrayMetadata = field.getAnnotation(ArrayMetadata.class);
				if (arrayMetadata==null) {
					throw new IllegalArgumentException("expected array size is not defined for array field "+field.getName());
				}
				ret+=arrayMetadata.size()*getTypeSize(arrayMetadata.componentType(), field.getName());
			}
		}
		return ret;
	}

	private long calculateFilePositionByIndexOfRecord(int index) {
		return CircularBufferHeader.HEADER_SIZE+index*calculateFieldSize();
	}

	private DataElement<V> getElementByIndex(int index, long now) {
		if (index>=0 && index<data.length) {
			DataElement<V> element = data[index];
			if (element!=null) {
				if (now-element.timestamp>header.getMajor()) {
					return null;
				} else {
					return element;
				}
			} else {
				return null;
			}
		} else {
			return null;
		}

	}

	private void mergeRecordsFromPrev(long now) throws InstantiationException, IllegalAccessException, IOException {

		// going backward in time from oldest records to newest

		List<DataElement<V>> toMerge = new ArrayList<>();

		for (int prevIndex=prev.data.length-1;prevIndex>=0;prevIndex--) {
			DataElement<V> prevElement = prev.getElementByIndex(prevIndex,now);
			if (prevElement!=null) {
				long prevPosition = prevElement.timestamp;
				int myIndex = calculatePositionIndex(prevPosition);
				DataElement<V> myElement = data[myIndex];
				if (myElement!=null) {
					if (isOutdatedDataElement(myElement)) {
						toMerge.add(prevElement);
					} else if (myElement.timestamp<prevElement.timestamp) {
						toMerge.add(prevElement);
					}
				} else {
					toMerge.add(prevElement);
				}
			}
		}
		StringWriter buf=null;
		PrintWriter wr=null;

		for (DataElement<V> prevElement:toMerge) {
			
			if (usePutLog) {
				buf = new StringWriter();
				wr = new PrintWriter(buf);
				wr.print("merge from prev:");
				wr.print(TIMESTAMP_FORMAT.format(new Date(prevElement.timestamp)));
				prevElement.data.dump(wr);
			}
			
			long prevPosition = prevElement.timestamp;
			int myIndex = calculatePositionIndex(prevPosition);
			DataElement<V> myElement = data[myIndex];
			if (myElement==null) {

				if (usePutLog) {
					wr.print("  new my element ");
				}

				@SuppressWarnings("unchecked")
				T newRecord = (T)prevElement.data.copy();
				myElement = new DataElement<V>(newRecord, prevElement.timestamp);
				myElement.isDirty=true;
				data[myIndex]=myElement;
			} else if (isOutdatedDataElement(myElement)) {

				if (usePutLog) {
					wr.print("  reset my element ");
				}
				
				myElement.data.reset();
			}
			
			if (usePutLog) {
				wr.print("merge to:");
				wr.print(TIMESTAMP_FORMAT.format(new Date(myElement.timestamp)));
				myElement.data.dump(wr);
			}

			myElement.data.merge(prevElement.data);
			if (myElement.timestamp<prevElement.timestamp) {
				myElement.timestamp=prevElement.timestamp;
			}
			myElement.isDirty=true;

			if (usePutLog) {
				wr.print("result:");
				wr.print(TIMESTAMP_FORMAT.format(new Date(myElement.timestamp)));
				myElement.data.dump(wr);
				wr.close();
				appendPutLog(buf.toString());
			}

		}
	}

	private void appendPutLog(String record) throws IOException {
		Path folder = path.getParent();
		String fileName = path.getFileName().toString()+".putlog";
		Path logPath = folder.resolve(fileName);
		try (PrintWriter pr = new PrintWriter(Files.newOutputStream(logPath,StandardOpenOption.CREATE, StandardOpenOption.APPEND))) {
			pr.println(record);
		}
	}

	private boolean isOutdatedDataElement(DataElement<V> element) {
		return element.timestamp<=System.currentTimeMillis()-header.getMajor()+header.getMinor();
	}

	private void putDataElement(int index, V value, long timestamp) throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		StringWriter buf=null;
		PrintWriter wr=null;

		try {
			if (usePutLog) {
				buf = new StringWriter();
				wr = new PrintWriter(buf);
				wr.print(TIMESTAMP_FORMAT.format(new Date()));
				wr.print('/');
				wr.print(TIMESTAMP_FORMAT.format(new Date(timestamp)));
				wr.print('|');
				wr.print(index);
				wr.print('|');
				wr.print(value);
				wr.print('|');
			}
			if (data[index]!=null) {
				if (usePutLog) {
					wr.print("before:");
					data[index].dump(wr);
					wr.print('|');
				}
				if (isOutdatedDataElement(data[index])) {
					data[index].data.reset();
					if (usePutLog) {
						wr.print("reset|");
					}
				}
			} else {
				if (usePutLog) {
					wr.print("before:null");
					wr.print('|');
				}
				T newRecord = recordDefaultConstructor.newInstance();
				data[index]=new DataElement<V>(newRecord, timestamp);
				data[index].data.reset();
			}
			data[index].data.put(value);
			data[index].timestamp=timestamp;
			data[index].isDirty=true;
			if (usePutLog) {
				wr.print("after:");
				data[index].dump(wr);
				wr.close();
				appendPutLog(buf.toString());
			}

		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	private int calculatePositionIndex(long position) {
		long d = (position - header.getBase()) % header.getMajor();
		return (int) (d / header.getMinor());
	}

	private long calculatePositionFromIndex(int index) {
		long now = System.currentTimeMillis();
		long delta = now - header.getBase();
		long numOfMajors = delta / header.getMajor();
		long offset = header.getMajor() * numOfMajors;
		long latestBase = header.getBase() + offset;
		long ret = latestBase + index * header.getMinor();
		if (ret>now) {
			ret-=header.getMajor();
		}
		return ret;
	}

	public void readHeader() throws IOException {
		lock.readLock().lock();
		try {
			try (SeekableByteChannel channel=openChannelForRead()) {
				channel.position(0);
				ByteBuffer buf = ByteBuffer.allocate(3*Long.SIZE/8);
				channel.read(buf);
				buf.position(0);
				long circularBufferBase = buf.getLong();
				long circularBufferMajor = buf.getLong();
				long circularBufferMinor = buf.getLong();
				this.header = new CircularBufferHeader(circularBufferMinor, circularBufferMajor, circularBufferBase);
			}
		} finally {
			lock.readLock().unlock();
		}
	}

	public void writeHeader() throws IOException {
		lock.writeLock().lock();
		try {
			try (SeekableByteChannel channel=openChannelForWrite()) {
				channel.position(0);
				ByteBuffer buf = ByteBuffer.allocate(3*Long.SIZE/8);
				buf.putLong(header.getBase());
				buf.putLong(header.getMajor());
				buf.putLong(header.getMinor());
				buf.position(0);
				channel.write(buf);
			}
		} finally {
			lock.writeLock().unlock();
		}
	}

	private T newRecordInstance() throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		return recordDefaultConstructor.newInstance();
	}

	private void loadData() throws IOException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		lock.readLock().lock();
		try {
			try (SeekableByteChannel channel=openChannelForRead()) {
				channel.position(CircularBufferHeader.HEADER_SIZE);
				ByteBuffer recordBuffer = ByteBuffer.allocate(calculateFieldSize());
				int index=0;
				int read = channel.read(recordBuffer);
				recordBuffer.flip();
				while (read>0 && index<data.length) {
					long timestamp=recordBuffer.getLong();
					T record = newRecordInstance();
					loadRecord(recordBuffer, record);
					if (timestamp>0) {
						data[index]=new DataElement<V>(record, timestamp);
					} else {
						data[index]=null;
					}
					index++;
					recordBuffer.flip();
					read = channel.read(recordBuffer);
					recordBuffer.flip();
				}
			}
		} finally {
			lock.readLock().unlock();
		}
	}

	private void loadField(ByteBuffer buf, T record, Class<?> fieldClass, Field field) throws IllegalArgumentException, IllegalAccessException {
		if (boolean.class.equals(fieldClass)) {
			if (buf.get()==0) {
				field.setBoolean(record, false);
			} else {
				field.setBoolean(record, true);
			}
		} else if (float.class.equals(fieldClass)) {
			field.set(record,buf.getFloat());
		} else if (double.class.equals(fieldClass)) {
			field.set(record,buf.getDouble());
		} else if (byte.class.equals(fieldClass)) {
			field.set(record,buf.get());
		} else if (short.class.equals(fieldClass)) {
			field.set(record,buf.getShort());
		} else if (int.class.equals(fieldClass)) {
			field.set(record,buf.getInt());
		} else if (long.class.equals(fieldClass)) {
			field.set(record,buf.getLong());
		} else {
			throw new IllegalArgumentException("unsupported field type: "+fieldClass.getName()+" for field "+field.getName());
		}
	}

	private void loadRecord(ByteBuffer buf, T record) throws IllegalArgumentException, IllegalAccessException {
		for (Field field:getFields()) {
			Class<?> fieldClass = field.getType();
			if (!fieldClass.isArray()) {
				loadField(buf, record, fieldClass, field);
			} else {
				ArrayMetadata arrayMetadata = field.getAnnotation(ArrayMetadata.class);
				if (arrayMetadata==null) {
					throw new IllegalArgumentException("expected array size is not defined for array field "+field.getName());
				}
				int actualSize = Array.getLength(field.get(record)); 
				if (arrayMetadata.size()!=actualSize) {
					throw new IllegalArgumentException("expected array size "+arrayMetadata.size()+" is not equal to actual array size "+actualSize+" for array "+recordClass.getName()+"."+field.getName());
				}
				Class<?> componentClass = arrayMetadata.componentType();
				Object array = Array.newInstance(componentClass, arrayMetadata.size());
				for (int index=0;index<arrayMetadata.size();index++) {
					if (boolean.class.equals(componentClass)) {
						if (buf.get()==0) {
							Array.setBoolean(array, index, false);
						} else {
							Array.setBoolean(array, index, true);
						}
					} else if (float.class.equals(componentClass)) {
						Array.set(array, index,buf.getFloat());
					} else if (double.class.equals(componentClass)) {
						Array.set(array, index,buf.getDouble());
					} else if (byte.class.equals(componentClass)) {
						Array.set(array, index,buf.get());
					} else if (short.class.equals(componentClass)) {
						Array.set(array, index,buf.getShort());
					} else if (int.class.equals(componentClass)) {
						int v=buf.getInt();
						Array.set(array, index, v);
					} else if (long.class.equals(componentClass)) {
						Array.set(array, index,buf.getLong());
					} else {
						throw new IllegalArgumentException("unsupported component type: "+componentClass.getName()+" for field "+field.getName());
					}
				}
				field.set(record, array);
			}
		}
	}


	private SeekableByteChannel openChannelForRead() throws IOException {
		return Files.newByteChannel(path, StandardOpenOption.CREATE);
	}

	private SeekableByteChannel openChannelForWrite() throws IOException {
		return Files.newByteChannel(path, StandardOpenOption.CREATE, StandardOpenOption.WRITE);
	}

	private void ensurePathExists() throws IOException {
		Path parent = path.getParent();
		Files.createDirectories(parent);
	}

	private int getCapasity() {
		return (int) (header.getMajor()/header.getMinor());
	}

	@SuppressWarnings("unchecked")
	private final void nullSafeAddRecord(List<TimestampedRecord<T>> list, int startIndex, int numberOfRecords, long now) {
		for (int index=0;index<numberOfRecords;index++) {
			DataElement<V> element = getElementByIndex(startIndex+index,now);
			long positionBegin = calculatePositionFromIndex(startIndex+index);
			long positionEnd = positionBegin + getMinor();
			if (element!=null) {
				list.add(new TimestampedRecord<T>(positionBegin, element.timestamp, positionEnd, (T)element.data));
			} else {
				list.add(new TimestampedRecord<T>(positionBegin, positionBegin+getMinor()/2, positionEnd, (T)null));
			}
		}
	}

	protected void loadRecordsForInterval(long from, long to, List<TimestampedRecord<T>> ret, long now) {
		int startIndex = calculatePositionIndex(from);
		if (startIndex<0) {
			startIndex=0;
		}
		int endIndex = calculatePositionIndex(to);
		int capacity = getCapasity();
		if (endIndex==startIndex) {
			if (to-from<=header.getMinor()) {
				nullSafeAddRecord(ret, startIndex, 1, now);
			} else {
				nullSafeAddRecord(ret, endIndex, capacity-endIndex+1,now);
				nullSafeAddRecord(ret, 0, startIndex+1,now);
			}
		} else if (endIndex>startIndex) {
			nullSafeAddRecord(ret, startIndex, endIndex-startIndex+1,now);
		} else {
			nullSafeAddRecord(ret, endIndex, capacity-endIndex+1,now);
			nullSafeAddRecord(ret, 0, startIndex+1,now);
		}

	}

	protected Collection<TimestampedRecord<T>> getRecordsForInterval(long from, long to, long now) throws IllegalArgumentException, IOException, IllegalAccessException, InstantiationException {
		if (to>now) {
			to=now;
		}
		long depth = now - header.getMajor();
		boolean toFit = to <= now &&  to >= depth;
		boolean fromFit = from < now && from >= depth;
		if (toFit && fromFit) {
			// all is here
			List< TimestampedRecord<T>> ret = new ArrayList< TimestampedRecord<T>>();
			loadRecordsForInterval(from, to, ret, now);
			return ret;
		} else if (!toFit && !fromFit) {
			// none is here
			if (next!=null) {
				return next.getRecordsForInterval(from, to, now);
			} else {
				return new ArrayList< TimestampedRecord<T>>();
			}
		} else {
			List< TimestampedRecord<T>> ret = new ArrayList< TimestampedRecord<T>>();
			loadRecordsForInterval(depth, to, ret, now);
			if (next!=null) {
				ret.addAll(next.getRecordsForInterval(from, depth, now));
			}
			return ret;
		}
	}


	@SuppressWarnings("unchecked")
	public static <T extends MergeableStatDataRecord<V>,V> CircularBufferGroupEntry<T,V> createNewCircularBufferGroupEntry(Class<T> recordClass, Path path, long major, long minor, long base) throws IOException, NoSuchMethodException, SecurityException {
		CircularBufferGroupEntry<T,V> ret = new CircularBufferGroupEntry<>();
		//		ret.buffer=new CircularByteBuffer(path);
		int cacheArraySize = (int)(major/minor);
		ret.data = new DataElement[cacheArraySize];
		ret.path = path;
		ret.recordClass = recordClass;
		ret.recordDefaultConstructor = getDefaultConstructor(recordClass);
		ret.header = new CircularBufferHeader(minor, major, base);
		ret.ensurePathExists();
		ret.writeHeader();
		return ret;
	}

	@SuppressWarnings("unchecked")
	public static <T extends MergeableStatDataRecord<V>,V> CircularBufferGroupEntry<T,V> openCircularBufferGroupEntry(Class<T> recordClass, Path path) throws IOException, InstantiationException, IllegalAccessException, NoSuchMethodException, SecurityException, IllegalArgumentException, InvocationTargetException {
		CircularBufferGroupEntry<T,V> ret = new CircularBufferGroupEntry<>();
		//		ret.buffer=new CircularByteBuffer(path);
		ret.path = path;
		ret.recordClass = recordClass;
		ret.recordDefaultConstructor = getDefaultConstructor(recordClass);
		ret.ensurePathExists();
		ret.readHeader();
		if (ret.header.getMajor()>0 && ret.header.getMinor()>0) {
			int cacheArraySize = (int)(ret.header.getMajor()/ret.header.getMinor());
			ret.data = new CircularBufferGroupEntry.DataElement[cacheArraySize];
			ret.loadData();
			return ret;
		} else {
			throw new IOException("Broken header for circular buffer '"+path+"'");
		}
	}

	private static <T> Constructor<? extends T> getDefaultConstructor(Class<T> recordClasz) throws NoSuchMethodException, SecurityException {
		Class<?>[] paramClasses = new Class[0];
		return recordClasz.getConstructor(paramClasses);
	}

	public static <T extends MergeableStatDataRecord<V>,V> CircularBufferGroupEntry<T,V> openOrCreateCircularBufferGroupEntry(Class<T> clasz, Path path, long major, long minor, long base) throws IOException, InstantiationException, IllegalAccessException, NoSuchMethodException, SecurityException, IllegalArgumentException, InvocationTargetException {
		if (Files.exists(path)) {
			return openCircularBufferGroupEntry(clasz, path);
		} else {
			return createNewCircularBufferGroupEntry(clasz, path, major, minor, base);
		}
	}

	protected long getMinor() {
		return header.getMinor();
	}

	private static final Format TIMESTAMP_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private static final Format TIME_INTERVAL_FORMAT = TimeIntervalFormat.getInstance(2, 0);

	private static final void printTimestamp(PrintWriter writer, String label, long timestamp) {
		if (!TextHelper.isNullOrEmpty(label)) {
			writer.print(label);
			writer.print(':');
		}
		if (timestamp>0)  {
			writer.print(TIMESTAMP_FORMAT.format(new Date(timestamp)));
		} else {
			writer.print("n/d");
		}
	}
	private void printTimeInterval(PrintWriter writer, String label, long timeInterval) {
		if (!TextHelper.isNullOrEmpty(label)) {
			writer.print(label);
			writer.print(':');
		}
		writer.print(TIME_INTERVAL_FORMAT.format(timeInterval));
	}

	private void dumpLocal(PrintWriter writer) throws IOException {
		writer.print("Circular Buffer Dump:'");
		writer.print(path.toString());
		writer.print("' ");
		writer.print("Records class:");
		writer.println(recordClass.getName());
		printTimestamp(writer, "Base", header.getBase());
		writer.print(' ');
		printTimeInterval(writer, "Major", header.getMajor());
		writer.print(' ');
		printTimeInterval(writer, "Minor", header.getMinor());
		writer.println();
		for (int i=0;i<data.length;i++) {
			writer.print(i);
			writer.print(' ');
			DataElement<V> d = data[i];
			if (d!=null) {
				d.dump(writer);
				writer.println();
			} else {
				writer.println("no data");
			}
		}

	}

	protected void dump(PrintWriter writer) throws IOException {
		dumpLocal(writer);
		if (next!=null) {
			writer.println();
			next.dump(writer);
		}
	}


}
