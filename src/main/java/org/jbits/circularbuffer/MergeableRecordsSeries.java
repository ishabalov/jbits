package org.jbits.circularbuffer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

public class MergeableRecordsSeries<T extends MergeableStatDataRecord<V>,V> {
	private long base;
	private long step;
	private MergeableStatDataRecord<V>[] series;

	@SuppressWarnings("unchecked")
	public MergeableRecordsSeries(Date from, Date to, int numberOfSteps) {
		this.base = from.getTime();
		this.step = (to.getTime()-from.getTime())/numberOfSteps;
		series = new MergeableStatDataRecord[numberOfSteps];
	}

	private int index(long timestamp) {
		int ret = (int)((timestamp-base)/step);
		if (ret>=0 && ret < series.length) {
			return ret;
		} else {
			return -1;
		}
	}

	private void merge(int index, MergeableStatDataRecord<V> value) {
		if (index>=0 && index<series.length) {
			if (series[index]!=null) {
				series[index].merge(value);
			} else {
				series[index]=value.copy();
			}
		}
		
	}
	
	public void addValue(long from, long to, MergeableStatDataRecord<V> value) {
		if (value!=null) {
			int fromIndex = index(from);
			int toIndex = index(to);
			if (fromIndex==toIndex) {
				merge(fromIndex, value);
			} else {
				for (int index=fromIndex;index<toIndex;index++) {
					merge(index, value);
				}
			}
		}
	}

	@SuppressWarnings("unchecked")
	public Collection<Entry<T>> getEntries() {
		Collection<Entry<T>> ret = new ArrayList<>();
		for (int index=0;index<series.length;index++) {
			ret.add(new Entry<T>(base, step, index, (T)series[index]));
		}
		return ret;
	}

	public static final class Entry<T extends MergeableStatDataRecord<?>> {
		private long timestamp;
		private T value;
		private Entry (long base, long step, int index, T value) {
			this.timestamp = base+step*index;
			this.value=value;
		}
		public long getTimestamp() {
			return timestamp;
		}
		public T getValue() {
			return value;
		}
	}

}
