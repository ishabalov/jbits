package org.jbits.circularbuffer;

import java.io.IOException;
import java.io.PrintWriter;

import org.jbits.math.MathHelper;

public class MinMaxAverageFloatStatData implements MergeableStatDataRecord<Float> {
	private float minValue;
	private float maxValue;
	private float firstValue;
	private float lastValue;
	private int counter;
	private float accummulatedValue;

	public MinMaxAverageFloatStatData() {
		reset();
	}

	@Override
	public void reset() {
		firstValue = Float.NaN;
		lastValue = Float.NaN;
		minValue = Float.NaN;
		maxValue = Float.NaN;
		counter = 0;
		accummulatedValue = Float.NaN;
	}

	@Override
	public void merge(MergeableStatDataRecord<Float> other) {
		if (other!=null && MinMaxAverageFloatStatData.class.isAssignableFrom(other.getClass())) {
			MinMaxAverageFloatStatData o = (MinMaxAverageFloatStatData)other;
			minValue = MathHelper.floatMinIgnoreNaN(minValue,o.minValue);
			maxValue = MathHelper.floatMaxIgnoreNaN(maxValue,o.maxValue);
			if (Float.isNaN(firstValue)) {
				firstValue=o.firstValue;
			}
			if (Float.isNaN(lastValue)) {
				lastValue=o.lastValue;
			}
			counter+=o.counter;
			accummulatedValue=MathHelper.floatSumIgnoreNaN(accummulatedValue,o.accummulatedValue);
		} else if (other!=null) {
			throw new IllegalArgumentException("can merge only instance of "+MinMaxAverageFloatStatData.class);
		}
	}

	@Override
	public MinMaxAverageFloatStatData copy() {
		MinMaxAverageFloatStatData ret = new MinMaxAverageFloatStatData();
		ret.accummulatedValue=accummulatedValue;
		ret.counter=counter;
		ret.firstValue=firstValue;
		ret.lastValue=lastValue;
		ret.maxValue=maxValue;
		ret.minValue=minValue;
		return ret;
	}

	@Override
	public void put(Float value) {
		if (value!=null) {
			minValue = MathHelper.floatMinIgnoreNaN(minValue,value);
			maxValue = MathHelper.floatMaxIgnoreNaN(maxValue,value);
			if (Float.isNaN(firstValue)) {
				firstValue = value;
			}
			lastValue = value;
			accummulatedValue=MathHelper.floatSumIgnoreNaN(accummulatedValue,value);
			counter++;
		}
	}

	public float getMinValue() {
		return minValue;
	}

	public float getMaxValue() {
		return maxValue;
	}

	public int getCounter() {
		return counter;
	}

	public float getAccummulatedValue() {
		return accummulatedValue;
	}

	public float getFirstValue() {
		return firstValue;
	}

	public float getLastValue() {
		return lastValue;
	}
	public float getAverageValue() {
		if (!Float.isNaN(accummulatedValue)) {
			return accummulatedValue/counter;
		} else {
			return Float.NaN;
		}
	}

	private void dumpValue(PrintWriter writer, String label, float value) {
		writer.print(label);
		writer.print(':');
		if (!Float.isNaN(value)) {
			writer.print(value);
		} else {
			writer.print("NaN");
		}
	}

	private void dumpValue(PrintWriter writer, String label, int value) {
		writer.print(label);
		writer.print(':');
		writer.print(value);
	}

	@Override
	public void dump(PrintWriter writer) throws IOException {
	dumpValue(writer, "Min", minValue);
		writer.print(' ');
		dumpValue(writer, "Max", maxValue);
		writer.print(' ');
		dumpValue(writer, "First", firstValue);
		writer.print(' ');
		dumpValue(writer, "Last", lastValue);
		writer.print(' ');
		dumpValue(writer, "Counter", counter);
		writer.print(' ');
		dumpValue(writer, "Accum", accummulatedValue);
		writer.print(' ');
		if (counter>0) {
			dumpValue(writer, "Average", accummulatedValue/(float)counter);
		}
	}

	@Override
	public double getRelativeCounterFor(Float s) {
		throw new UnsupportedOperationException("relative values in sot applicable to this class");
	}

	@Override
	public int getAbsoluteCounterFor(Float s) {
		throw new UnsupportedOperationException("absolute values in sot applicable to this class");
	}
}
