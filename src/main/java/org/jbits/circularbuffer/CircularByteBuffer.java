package org.jbits.circularbuffer;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SeekableByteChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.concurrent.Callable;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import org.jbits.time.TimeInterval;

public class CircularByteBuffer {
	private Path path;
	private AtomicReference<CircularBufferDataHolder> data = new AtomicReference<>();
	private ReentrantReadWriteLock lock = new ReentrantReadWriteLock();
	private ScheduledExecutorService executor = null;
	private AtomicLong cacheHits = new AtomicLong();
	private AtomicLong cacheMiss = new AtomicLong();
	private AtomicLong flushCount = new AtomicLong();

	public static class CircularByteBufferSlise {
		private CircularBufferDataHolder holder;
		private ByteBuffer slise;
		private int positionOffset;
		private CircularByteBufferSlise(CircularBufferDataHolder holder, long filePosition, int size) {
			this.holder=holder;
			this.positionOffset = (int)(filePosition-holder.filePosition);
			this.slise = holder.buffer.slice();
			this.slise.position(positionOffset);
		}
		public long getLong() {
			return slise.getLong();
		}
		public byte get() {
			return slise.get();
		}
		public short getShort() {
			return slise.getShort();
		}
		public int getInt() {
			return slise.getInt();
		}
		public float getFloat() {
			return slise.getFloat();
		}
		public double getDouble() {
			return slise.getDouble();
		}
		private void dirty() {
			holder.isDirty.set(true);
		}
		public ByteBuffer put(byte b) {
			dirty();
			return slise.put(b);
		}
		public ByteBuffer putShort(short value) {
			dirty();
			return slise.putShort(value);
		}
		public ByteBuffer putInt(int value) {
			dirty();
			return slise.putInt(value);
		}
		public ByteBuffer putLong(long value) {
			dirty();
			return slise.putLong(value);
		}
		public ByteBuffer putFloat(float value) {
			dirty();
			return slise.putFloat(value);
		}
		public ByteBuffer putDouble(double value) {
			dirty();
			return slise.putDouble(value);
		}
		public void position(int newPosition) {
			slise.position(positionOffset+newPosition);
		}
		public void lock() {
			holder.owner.lock.writeLock().lock();
		}
		public void unlock() {
			holder.owner.lock.writeLock().unlock();
		}
		public void flush(TimeInterval interval) throws IOException {
			holder.flush(interval);
		}
	}

	private static class CircularBufferDataHolder {
		private CircularByteBuffer owner;
		private AtomicBoolean isDirty=new AtomicBoolean(false);
		private ByteBuffer buffer;
		private long filePosition;
		private CircularBufferDataHolder(CircularByteBuffer owner, ByteBuffer buffer, long filePosition) {
			this.owner=owner;
			this.buffer = buffer;
			this.filePosition = filePosition;
		}
		private boolean contain(long start, long size) {
			return filePosition<=start && (filePosition+buffer.capacity())>=(start+size);
		}

		private AtomicReference<ScheduledFuture<Boolean>> updateFeature = new AtomicReference<>();

		private void internalFlush()  throws IOException {
			if (isDirty.get()) {
				try (SeekableByteChannel channel=owner.openChannelForWrite()) {
					buffer.position(0);
					channel.position(filePosition);
					channel.write(buffer);
					buffer.position(0);
					isDirty.set(false);
					owner.flushCount.incrementAndGet();
				}
			}
		}

		private void flushNow() throws IOException {
			owner.lock.writeLock().lock();
			try {
				ScheduledFuture<Boolean> feature = updateFeature.get();
				if (feature!=null && !feature.isDone()) {
					feature.cancel(false);
					updateFeature.set(null);
				}
				internalFlush();
			} finally {
				owner.lock.writeLock().unlock();
			}
		}

		private void flush(TimeInterval interval) throws IOException {
			if (interval!=null && owner.executor!=null) {
				owner.lock.writeLock().lock();
				try {
					ScheduledFuture<Boolean> feature = updateFeature.get();
					if (feature==null || (feature!=null && feature.isDone())) {
						updateFeature.set( owner.executor.schedule(new Callable<Boolean>() {

							@Override
							public Boolean call() throws Exception {
								owner.lock.writeLock().lock();
								try {
									internalFlush();
									return true;
								} finally {
									owner.lock.writeLock().unlock();
								}
							}
						}, interval.milliseconds(), TimeUnit.MILLISECONDS));
					}
				} finally {
					owner.lock.writeLock().unlock();
				}
			} else {
				flushNow();
			}
		}
	}
	
	private TimeInterval bufferTimeToLive = null;
	
	private AtomicReference<ScheduledFuture<Boolean>> bufferEvictionFeature = new AtomicReference<>();

	private CircularBufferDataHolder loadBuffer(long position, int size) throws IOException {
		try (SeekableByteChannel channel=openChannelForRead()) {
			channel.position(position);
			ByteBuffer buf = ByteBuffer.allocate(size);
			channel.read(buf);
			buf.position(0);
			ScheduledFuture<Boolean> feature = bufferEvictionFeature.get();
			if (feature!=null && !feature.isDone()) {
				feature.cancel(false);
				bufferEvictionFeature.set(null);
			}
			if (bufferTimeToLive!=null) {
				feature = executor.schedule(new Callable<Boolean>(){

					@Override
					public Boolean call() throws Exception {
						CircularBufferDataHolder holder = data.get();
						if (holder!=null) {
							data.set(null);
							holder.flushNow();
						}
						return true;
					}}, bufferTimeToLive.milliseconds(), TimeUnit.MILLISECONDS);
			}
			return new CircularBufferDataHolder(this,buf,position);
		}

	}

	public CircularByteBufferSlise getBuffer(long position, int size) throws IOException {
		lock.readLock().lock();
		try {
			CircularBufferDataHolder currentData = data.get();
			if (currentData!=null) {
				if (currentData.contain(position, size)) {
					cacheHits.incrementAndGet();
					return new CircularByteBufferSlise(currentData, position, size);
				} else {
					cacheMiss.incrementAndGet();
					if (currentData.isDirty.get()) {
						lock.readLock().unlock();
						lock.writeLock().lock();
						try {
							currentData.flushNow();
						} finally {
							lock.readLock().lock();
							lock.writeLock().unlock();
						}
					}
					currentData = loadBuffer(position, size);
					data.set(currentData);
					return new CircularByteBufferSlise(currentData, position, size);
				}
			} else {
				cacheMiss.incrementAndGet();
				currentData = loadBuffer(position, size);
				data.set(currentData);
				return new CircularByteBufferSlise(currentData, position, size);
			}
		} finally {
			lock.readLock().unlock();
		}
	}

	public CircularBufferHeader readHeader() throws IOException {
		lock.readLock().lock();
		try {
			try (SeekableByteChannel channel=openChannelForRead()) {
				channel.position(0);
				ByteBuffer buf = ByteBuffer.allocate(3*Long.SIZE/8);
				channel.read(buf);
				buf.position(0);
				long circularBufferBase = buf.getLong();
				long circularBufferMajor = buf.getLong();
				long circularBufferMinor = buf.getLong();
				return new CircularBufferHeader(circularBufferMinor, circularBufferMajor, circularBufferBase);
			}
		} finally {
			lock.readLock().unlock();
		}
	}

	public void writeHeader(CircularBufferHeader header) throws IOException {
		lock.writeLock().lock();
		try {
			try (SeekableByteChannel channel=openChannelForWrite()) {
				channel.position(0);
				ByteBuffer buf = ByteBuffer.allocate(3*Long.SIZE/8);
				buf.putLong(header.getBase());
				buf.putLong(header.getMajor());
				buf.putLong(header.getMinor());
				buf.position(0);
				channel.write(buf);
			}
		} finally {
			lock.writeLock().unlock();
		}
	}

	private SeekableByteChannel openChannelForRead() throws IOException {
		return Files.newByteChannel(path, StandardOpenOption.CREATE);
	}

	private SeekableByteChannel openChannelForWrite() throws IOException {
		return Files.newByteChannel(path, StandardOpenOption.CREATE, StandardOpenOption.WRITE);
	}

	public CircularByteBuffer(Path path) {
		this.path=path;
	}

	public void close() throws IOException {
		CircularBufferDataHolder currentData = data.get();
		if (currentData!=null) {
			currentData.flushNow();
		}
	}

	public TimeInterval getBufferTimeToLive() {
		return bufferTimeToLive;
	}

	public void setBufferTimeToLive(TimeInterval bufferTimeToLive) {
		this.bufferTimeToLive = bufferTimeToLive;
	}

	public void setExecutor(ScheduledExecutorService executor) {
		this.executor = executor;
	}

	public long getCacheHits() {
		return cacheHits.get();
	}

	public long getCacheMiss() {
		return cacheMiss.get();
	}

	public long getFlushCount() {
		return flushCount.get();
	}

	public void resetStat() {
		cacheHits.set(0);
		cacheMiss.set(0);
		flushCount.set(0);
	}
}
