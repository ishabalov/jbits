package org.jbits.text;

public class ConfigParserProperty {
	private String name;
	private String value;
	protected ConfigParserProperty(String name, String value) {
		this.name = name;
		this.value = value;
	}
	public String getName() {
		return name;
	}
	public String getValue() {
		return value;
	}
}
