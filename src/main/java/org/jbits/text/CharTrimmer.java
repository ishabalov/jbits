package org.jbits.text;

public interface CharTrimmer {
	boolean trim(char c);
}
