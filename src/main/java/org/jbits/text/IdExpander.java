package org.jbits.text;

public interface IdExpander {
	Iterable<Object> expandId(String str);
}
