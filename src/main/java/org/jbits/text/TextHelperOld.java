package org.jbits.text;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.IOUtils;
import org.apache.commons.io.LineIterator;
import org.jbits.time.TimeInterval;
import org.jbits.utils.ComparatorHelper;


public class TextHelperOld {

	public static final String parseField(String field, String body) {
		return parseFieldByPrefixAndSuffix("<" + field + ">", "</" + field + ">", body);
	}

	public static final boolean parseBooleanField(String field, String body) {
		return body.indexOf("<" + field + "/>") >= 0;
	}

	public static final String parseFieldByPrefixAndSuffix(String prefix, String suffix, String body) {
		int beginIndex = body.indexOf(prefix);
		if (beginIndex >= 0) {
			int endIndex = body.indexOf(suffix, beginIndex + prefix.length());
			if (endIndex >= 0) {
				return body.substring(beginIndex + prefix.length(), endIndex);
			} else {
				return body.substring(beginIndex + prefix.length());
			}
		} else {
			return null;
		}
	}

	public static final String stripFileExtention(String fileName) {
		int p = fileName.lastIndexOf('.');
		if (p>=0) {
			return fileName.substring(0,p);
		} else {
			return fileName;
		}
	}

	public static final String stripSuffix(String str, String suffix) {
		return str.substring(0,str.length()-suffix.length());
	}

	public static final boolean isNullOrEmpty(String str) {
		return str==null || str.isEmpty();
	}

	public static final boolean isEmptyButNotNull(String str) {
		return str!=null && str.isEmpty();
	}

	public static final String nullSafeTrim(String str) {
		if (isNullOrEmpty(str)) {
			return str;
		} else {
			return str.trim();
		}
	}

	public static final String nullSafeToString(Object obj) {
		if (obj!=null) {
			return obj.toString();
		} else {
			return null;
		}
	}
	
	public static final String nullSafeToUpperCase(String str) {
		if (isNullOrEmpty(str)) {
			return str;
		} else {
			return str.toUpperCase();
		}
	}

	public static final String nullSafeToLowerCase(String str) {
		if (isNullOrEmpty(str)) {
			return str;
		} else {
			return str.toLowerCase();
		}
	}

	public static final boolean nullSafeContainsIgnoreCase(String containsIn, String contains) {
		if (!isNullOrEmpty(containsIn) && !isNullOrEmpty(contains)) {
			return containsIn.toLowerCase().contains(contains.toLowerCase());
		} else {
			return false;
		}
	}

	public static final String nullSafeUrlEncode(String str, String codec) throws UnsupportedEncodingException {
		if (isNullOrEmpty(str)) {
			return str;
		} else {
			return URLEncoder.encode(str, codec);
		}
	}

	public static final String nullSafeUrlEncode(String str) throws UnsupportedEncodingException {
		return nullSafeUrlEncode(str, "UTF-8");
	}

	public static final String nullSafeUrlDecode(String str, String codec) throws UnsupportedEncodingException {
		if (isNullOrEmpty(str)) {
			return str;
		} else {
			return URLDecoder.decode(str, codec);
		}
	}

	public static final String nullSafeUrlDecode(String str) throws UnsupportedEncodingException {
		return nullSafeUrlDecode(str, "UTF-8");
	}

	private static final Comparator<String> PLAIN_STRING_COMPARATOR = new Comparator<String>() {
		@Override
		public int compare(String o1, String o2) {
			return o1.compareTo(o2);
		}

	};
	private static final Comparator<String> PLAIN_STRING_IGNORE_CASE_COMPARATOR = new Comparator<String>() {
		@Override
		public int compare(String o1, String o2) {
			return o1.compareToIgnoreCase(o2);
		}
	};
	public static final Pattern CARDINAL = Pattern.compile("[0-9]+");
	public static final Pattern REAL = Pattern.compile("-?[0-9]*\\.[0-9]+");
	private static final Comparator<String> SMART_STRING_IGNORE_CASE_COMPARATOR = new Comparator<String>() {
		@Override
		public int compare(String s1, String s2) {
			if (REAL.matcher(s1).matches() && REAL.matcher(s2).matches()) {
				Double d1=Double.valueOf(s1);
				Double d2=Double.valueOf(s2);
				return d1.compareTo(d2);
			} else { 
				return s1.compareToIgnoreCase(s2);
			}
		}
	};

	public static final int nullSafeCompare(String s1, String s2) {
		return ComparatorHelper.nullSafeCompare(s1, s2, PLAIN_STRING_COMPARATOR);
	}

	public static final int nullSafeCompareIgnoreCase(String s1, String s2) {
		return ComparatorHelper.nullSafeCompare(s1, s2, PLAIN_STRING_IGNORE_CASE_COMPARATOR);
	}
	public static final int nullSafeSmartCompareIgnoreCase(String s1, String s2) {
		return ComparatorHelper.nullSafeCompare(s1, s2, SMART_STRING_IGNORE_CASE_COMPARATOR);
	}

	private static final String SMART_STRING_SPLIT_BY = "[ \\(\\)\\,\\:\\-\\+\\/]";
	private static final Comparator<String> SMART_WORDS_STRING_IGNORE_CASE_COMPARATOR = new Comparator<String>() {
		@Override
		public int compare(String s1, String s2) {
			String[] split1 = s1.split(SMART_STRING_SPLIT_BY);
			String[] split2 = s2.split(SMART_STRING_SPLIT_BY);
			int index=0;
			while (index<split1.length&&index<split2.length) {
				int result = nullSafeSmartCompareIgnoreCase(split1[index], split2[index]);
				if (result==0) {
					index++;
				} else {
					return result;
				}
			}
			if (index>=split1.length && index>=split2.length) {
				return 0;
			} else if (index>=split2.length) {
				return 1;
			} else {
				return -1;
			}
		}
	};
	public static final int nullSafeSmartWordsCompareIgnoreCase(String s1, String s2) {
		return ComparatorHelper.nullSafeCompare(s1, s2, SMART_WORDS_STRING_IGNORE_CASE_COMPARATOR);
	}

	public static final boolean nullSafeEquals(String s1, String s2) {
		if (s1!=null&&s2!=null) {
			return s1.equals(s2);
		} else {
			return false;
		}
	}
	public static final boolean nullSafeEqualsIgnoreCase(String s1, String s2) {
		if (s1!=null&&s2!=null) {
			return s1.equalsIgnoreCase(s2);
		} else {
			return false;
		}
	}

	public static final String stripFileName(String path) {
		int p = path.lastIndexOf('/');
		if (p>=0) {
			return path.substring(0,p);
		} else {
			return "";
		}
	}

	public static final class Contact {

		public Contact(String email, String name) {
			this.email = email;
			this.name = name;
		}
		private String email;
		private String name;
		public String getEmail() {
			return email;
		}
		public String getName() {
			return name;
		}
		public boolean equals(Contact other) {
			return this.email.equals(other.email) && this.name.equals(other.name);
		}	
	}

	public static final Contact parseEmailValue(String emailStr) {
		String name=null;
		String email=null;
		if (emailStr != null && !emailStr.isEmpty()) {
			if (emailStr.contains("<") && emailStr.contains("@")) {// extract name that comes before < email address>
				StringTokenizer tokenizer = new StringTokenizer(emailStr, "<>");
				if (tokenizer.hasMoreTokens()) {
					name = tokenizer.nextToken();
				}
				if (tokenizer.hasMoreTokens()) {
					email = tokenizer.nextToken();
				}
			} else {
				name = emailStr;// this is probably an email address without name in front of it
				email = emailStr;
			}
			return new Contact(email, name);
		}
		return null;
	}


	public static final String transformAllSegmentsExclusive(String content, String prefix, String suffix, Transformer transformer) {
		int currentPos = 0;
		StringBuilder ret = new StringBuilder();
		int begin = content.indexOf(prefix,currentPos);
		while (currentPos<content.length()&&begin>=0) {
			int end = content.indexOf(suffix,begin+prefix.length());
			String left;
			String mid;
			left = content.substring(currentPos,begin+prefix.length());
			ret.append(left);
			if (end>=0) {
				mid = content.substring(begin+prefix.length(),end);
				ret.append(transformer.transform(mid));
				ret.append(suffix);
				currentPos = end+suffix.length();
			} else {
				mid = content.substring(begin+prefix.length());
				ret.append(transformer.transform(mid));
				currentPos = content.length();
			}
			begin = content.indexOf(prefix,currentPos);
		}
		if (currentPos<content.length()) {
			ret.append(content.substring(currentPos));
		}
		return ret.toString();
	}	


	public static final String transformAllSegmentsRegexpInclusive(String str, String prefixPattern, String suffixPattern, Transformer transformer) {
		Pattern prefix = Pattern.compile(prefixPattern);
		Pattern suffix = Pattern.compile(suffixPattern);
		Matcher prefixMatcher = prefix.matcher(str);
		Matcher suffixMatcher = suffix.matcher(str);
		int currentBegin = 0;
		StringBuilder ret = new StringBuilder();
		while (currentBegin<str.length()&&prefixMatcher.find(currentBegin)) {
			int pStart = prefixMatcher.start();
			int pEnd = prefixMatcher.end();
			if (suffixMatcher.find(pEnd)) {
				int sEnd = suffixMatcher.end();
				ret.append(str.substring(currentBegin,pStart));
				ret.append(transformer.transform(str.substring(pStart,sEnd)));
				currentBegin=sEnd;
			} else {
				ret.append(str.substring(currentBegin,pStart));
				ret.append(transformer.transform(str.substring(pStart)));
				currentBegin=str.length();
			}
		}
		if (currentBegin<str.length()) {
			ret.append(str.substring(currentBegin));
		}
		return ret.toString();
	}

	public static final String transformAllSegmentsRegexpExclusive(String str, String prefixPattern, String suffixPattern, Transformer transformer) {
		Pattern prefix = Pattern.compile(prefixPattern);
		Pattern suffix = Pattern.compile(suffixPattern);
		Matcher prefixMatcher = prefix.matcher(str);
		Matcher suffixMatcher = suffix.matcher(str);
		int currentBegin = 0;
		StringBuilder ret = new StringBuilder();
		while (currentBegin<str.length()&&prefixMatcher.find(currentBegin)) {
			int pEnd = prefixMatcher.end();
			if (suffixMatcher.find(pEnd)) {
				int sStart = suffixMatcher.start();
				int sEnd = suffixMatcher.end();
				ret.append(str.substring(currentBegin,pEnd));
				ret.append(transformer.transform(str.substring(pEnd,sStart)));
				ret.append(str.substring(sStart,sEnd));
				currentBegin=sEnd;
			} else {
				ret.append(str.substring(currentBegin,pEnd));
				ret.append(transformer.transform(str.substring(pEnd)));
				currentBegin=str.length();
			}
		}
		if (currentBegin<str.length()) {
			ret.append(str.substring(currentBegin));
		}
		return ret.toString();
	}

	public static final String transformAllSegmentsInclusive(String content, String prefix, String suffix, Transformer transformer) {
		int currentPos = 0;
		StringBuilder ret = new StringBuilder();
		int begin = content. indexOf(prefix,currentPos);
		while (currentPos<content.length()&&begin>=0) {
			int end = content.indexOf(suffix,begin+prefix.length());
			String left;
			String mid;
			left = content.substring(currentPos,begin);
			ret.append(left);
			if (end>=0) {
				mid = content.substring(begin,end+suffix.length());
				ret.append(transformer.transform(mid));
				currentPos = end+suffix.length();
			} else {
				mid = content.substring(begin);
				ret.append(transformer.transform(mid));
				currentPos = content.length();
			}
			begin = content.indexOf(prefix,currentPos);
		}
		if (currentPos<content.length()) {
			ret.append(content.substring(currentPos));
		}
		return ret.toString();
	}

	public static final String nullSafeQuotedString(String str) {
		if (str!=null) {
			return "\""+str.toString()+"\"";
		} else {
			return "";
		}
	}
	public static final String nullSafeNonQuotedObject(Object obj) {
		if (obj!=null) {
			return ""+obj.toString()+"";
		} else {
			return "";
		}
	}

	public static final void parseAllLines(File file,LineParser parser) throws IOException {
		Reader r = null;
		try {
			r = new FileReader(file);
			LineIterator it = IOUtils.lineIterator(r);
			while (it.hasNext()) {
				String line = it.next().trim();
				if (!line.isEmpty()&&!(line.charAt(0)=='#')) {
					parser.parseLine(line);
				}
			}
		} finally {
			if (r!=null) {
				r.close();
			}
		}
	}

	public static final void parseAllLines(String str,LineParser parser) {
		for (String line:str.split("[\\n\\r]")) {
			if (line!=null) {
				parser.parseLine(line.trim());
			}
		}
	}


	private static final Pattern NUMBER_PATTERN=Pattern.compile("[ \\t]*[0-9]+[ \\t]*");
	private static final Pattern RANGE_PATTERN=Pattern.compile("[ \\t]*[0-9]+[ \\t]*\\-[ \\t]*[0-9]+[ \\t]*");

	public static final Collection<Integer> expandStringId(String str) {
		List<Integer> ret = new ArrayList<Integer>();

		for (String split:str.split("\\,")) {
			if (split!=null) {
				split=split.trim();
				if (NUMBER_PATTERN.matcher(split).matches()) {
					ret.add(Integer.valueOf(split));
				} else if (RANGE_PATTERN.matcher(split).matches()) {
					String range[] = split.split("\\-");
					int start = Integer.valueOf(range[0].trim());
					int end = Integer.valueOf(range[1].trim());
					for (int id=start;id<=end;id++) {
						ret.add(Integer.valueOf(id));
					}
				} else if (!split.isEmpty()) {
					throw new IllegalArgumentException("illegal id string '"+split+"' for input '"+str+"'");
				}
			}
		}
		return ret;
	}

	public static final Map<String,String> parsePropertiesLine(String line) {
		Map<String,String> ret = new HashMap<String,String>();
		if (line!=null&&!line.isEmpty()) {
			line=line.replaceAll("\\\\\\;", "%%%%%");
			for (String split:line.split(";")) {
				if (split!=null) {
					split=split.trim().replaceAll("%%%%%", ";");
					String pair[]=split.split("=");
					ret.put(pair[0], pair[1]);
				}
			}
		}
		return ret;
	}

	public static final String stringPropertyValue(Map<String,String> properties, String name, String defaultValue) {
		String value = properties.get(name);
		if (value!=null) {
			return value;
		} else {
			return defaultValue;
		}
	}
	public static final String stringPropertyValue(Map<String,String> properties, String name) {
		return stringPropertyValue(properties, name,null);
	}

	public static final TimeInterval timeIntervalPropertyValue(Map<String,String> properties, String name, TimeInterval defaultValue) {
		String value = properties.get(name);
		if (value!=null) {
			return TimeInterval.valueOf(value);
		} else {
			return defaultValue;
		}
	}	
	public static final TimeInterval timeIntervalPropertyValue(Map<String,String> properties, String name) {
		return timeIntervalPropertyValue(properties, name, null);
	}

	public static final Long longPropertyValue(Map<String,String> properties, String name, Long defaultValue) {
		String value = properties.get(name);
		if (value!=null) {
			return Long.valueOf(value);
		} else {
			return defaultValue;
		}
	}
	public static final Long longPropertyValue(Map<String,String> properties, String name) {
		return longPropertyValue(properties, name,null);
	}
	public static final Integer integerPropertyValue(Map<String,String> properties, String name, Integer defaultValue) {
		String value = properties.get(name);
		if (value!=null) {
			return Integer.valueOf(value);
		} else {
			return defaultValue;
		}
	}	
	public static final Integer integerPropertyValue(Map<String,String> properties, String name) {
		return integerPropertyValue(properties, name,null);
	}

	public static final Float floatPropertyValue(Map<String,String> properties, String name, Float defaultValue) {
		String value = properties.get(name);
		if (value!=null) {
			return Float.valueOf(value);
		} else {
			return defaultValue;
		}
	}
	public static final Float floatPropertyValue(Map<String,String> properties, String name) {
		return floatPropertyValue(properties, name, null);
	}
	public static final Double doublePropertyValue(Map<String,String> properties, String name, Double defaultValue) {
		String value = properties.get(name);
		if (value!=null) {
			return Double.valueOf(value);
		} else {
			return defaultValue;
		}
	}
	public static final Double doublePropertyValue(Map<String,String> properties, String name) {
		return doublePropertyValue(properties, name, null);
	}

	public static final Boolean booleanValue(String value, Boolean defaultValue) {
		if (value!=null) {
			if ("true".equalsIgnoreCase(value) || "yes".equalsIgnoreCase(value)) {
				return Boolean.TRUE;
			} else {
				return Boolean.FALSE;
			}
		} else {
			return defaultValue;
		}
	}

	public static final Boolean booleanPropertyValue(Map<String,String> properties, String name) {
		String value = properties.get(name);
		return booleanValue(value, null);
	}
	public static final boolean booleanPropertyValue(Map<String,String> properties, String name, boolean defaultValue) {
		Boolean ret = booleanPropertyValue(properties, name);
		if (ret!=null) {
			return ret.booleanValue();
		} else {
			return defaultValue;
		}
	}

	public static final DecimalFormat getNumberFormat() {
		return getNumberFormat(3,3);
	}

	public static final DecimalFormat getNumberFormat(int grouping, int decimal) {
		DecimalFormat ret = new DecimalFormat();
		if (grouping>0) {
			ret.setGroupingSize(grouping);
			ret.setGroupingUsed(true);
		}
		if (decimal>0) {
			ret.setMinimumFractionDigits(decimal);
		}
		return ret;
	}
	private static final Format DEFAULT_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z");

	public static final String dateToString(Date date) {
		return DEFAULT_DATE_FORMAT.format(date);
	}
	public static final Date stringToDate(String str) throws ParseException {
		return (Date)DEFAULT_DATE_FORMAT.parseObject(str);
	}

	public static final Pattern[] asPatternsArray(String[] strs, int flags) {
		Pattern[] ret = new Pattern[strs.length];
		for (int i=0;i<strs.length;i++) {
			ret[i]=Pattern.compile(strs[i], flags);
		}
		return ret;
	}

	public static final boolean matchAny(String str, Pattern[] patterns) {
		for (Pattern p:patterns) {
			Matcher m = p.matcher(str);
			if (m.matches()) {
				return true;
			}
		}
		return false;
	}

	public static Map<String,String> parseAllLinesAsProperties(String str) {
		final Map<String,String> ret = new HashMap<String,String>();
		parseAllLines(str, new LineParser() {

			@Override
			public void parseLine(String line) {
				if (!isNullOrEmpty(line)) {
					String[] split=line.split("=");
					if (split.length==1) {
						ret.put(nullSafeTrim(split[0]),null);
					} else if (split.length==2) {
						ret.put(nullSafeTrim(split[0]),nullSafeTrim(split[1]));
					} else {
						throw new RuntimeException("Unparseable properties string:'"+line+"'");
					}
				}
			}
		});
		return ret;
	}

	public static String replaceAllFilesystemSpecialChar(String string, char repacement) {
		StringBuilder ret = new StringBuilder();
		for (char c:string.toCharArray()) {
			switch (c) {
				case '/':
				case '\\':
				case '?':
				case '*':
				case '"':
				case '%':
					ret.append(repacement);
					break;
				default:
					ret.append(c);
			}
		}
		return ret.toString();
	}

	public static final String renderAsDelimitedString(Iterator<?> it, char delim) {
		StringBuilder ret = new StringBuilder();
		while (it.hasNext()) {
			ret.append(it.next());
			if (it.hasNext()) {
				ret.append(delim);
			}
		}
		return ret.toString();
	}

	public static final String renderAsDelimitedString(Collection<?> it, char delim) {
		return renderAsDelimitedString(it.iterator(), delim);
	}

	public static final String renderAsDelimitedString(Object[] it, char delim) {
		StringBuilder ret = new StringBuilder();
		for (int i=0;i<it.length;i++) {
			ret.append(it[i]);
			if (i<it.length-1) {
				ret.append(delim);
			}
		}
		return ret.toString();
	}

	public static final String nullSafeToString(Object obj, String defaultValue) {
		if (obj==null) {
			return defaultValue;
		} else {
			return obj.toString();
		}
	}

	public static final int nullSafeToInt(Object obj, int defaultValue) {
		if (obj==null) {
			return defaultValue;
		} else {
			return Integer.valueOf(obj.toString());
		}
	}

	public static final TimeInterval nullSafeToTimeInterval(Object obj, TimeInterval defaultValue) {
		if (obj==null) {
			return defaultValue;
		} else {
			return TimeInterval.valueOf(obj.toString());
		}
	}

	public static final boolean nullSafeToBooleanExtended(Object obj, boolean defaultValue) {
		if (obj!=null) {
			String val = obj.toString().toLowerCase();
			switch (val) {
			case "yes":
			case "true":
			case "1":
				return true;
			default:
				return false;
			}
		} else {
			return defaultValue;
		}
	}
}
