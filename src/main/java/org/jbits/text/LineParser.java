package org.jbits.text;

public interface LineParser {
	void parseLine(String line);
}
