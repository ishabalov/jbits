package org.jbits.text;

import java.awt.Color;
import java.text.DecimalFormat;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

public enum StandardFormatters implements Formatter<Object>  {
	Default,
	NullSafeDefaultEmptyNull {
		@Override
		public String format(Object o) {
			return DEFAULT_NULL_SAFE_EMPTY_NULL.format(o);
		}
	},
	NullSafeDefault {
		@Override
		public String format(Object o) {
			return DEFAULT_NULL_SAFE.format(o);
		}
	},
	ShortDate {
		@Override
		public String format(Object o) {
			return preferredFormatFallToDefault(Date.class, DEFAULT_SHORT_DATE, o);
		}
	},
	LongDate {
		@Override
		public String format(Object o) {
			return preferredFormatFallToDefault(Date.class, DEFAULT_LONG_DATE, o);
		}
	},
	LongDateWithTimezone {
		@Override
		public String format(Object o) {
			return preferredFormatFallToDefault(Date.class, DEFAULT_LONG_DATE_WITH_TIMEZONE, o);
		}
	},
	ShortSortableDate {
		@Override
		public String format(Object o) {
			return preferredFormatFallToDefault(Date.class, DEFAULT_SHORT_SORTABLE_DATE, o);
		}
	},
	LongSortableDate {
		@Override
		public String format(Object o) {
			return preferredFormatFallToDefault(Date.class, DEFAULT_LONG_SORTABLE_DATE, o);
		}
	},
	Double {
		@Override
		public String format(Object o) {
			return preferredFormatFallToDefault(Number.class, DEFAULT_DOUBLE, o);
		}
	},
	Float {
		@Override
		public String format(Object o) {
			return preferredFormatFallToDefault(Number.class, DEFAULT_FLOAT, o);
		}
	},
	Integer {
		@Override
		public String format(Object o) {
			return preferredFormatFallToDefault(Number.class, DEFAULT_INTEGER, o);
		}
	},
	Long {
		@Override
		public String format(Object o) {
			return preferredFormatFallToDefault(Number.class, DEFAULT_LONG, o);
		}
	},
	Percent {
		@Override
		public String format(Object o) {
			if (Number.class.isAssignableFrom(o.getClass())) {
				return DEFAULT_PERCENT.format((Number)o);
			} else {
				throw new IllegalArgumentException(""+o+" is not valid for percent format");
			}
		}
	},
	Color {
		@Override
		public String format(Object o) {
			if (Color.class.isAssignableFrom(o.getClass())) {
				return DEFAULT_COLOR.format((Color)o);
			} else {
				throw new IllegalArgumentException(""+o+" is not valid for color format");
			}
		}
	};

	@SuppressWarnings("unchecked")
	private static final <T> String preferredFormatFallToDefault(Class<T> conditionClass, Formatter<T> preferredFormatter, Object o) {
		if (o!=null && conditionClass.isAssignableFrom(o.getClass())) {
			return preferredFormatter.format((T)o);
		} else {
			return DEFAULT_NULL_SAFE.format(o);
		}
	}
	
	private static final class DefaultFormatter implements Formatter<Object> {
		String nullValue = "";

		private DefaultFormatter(String nullValue) {
			this.nullValue = nullValue;
		}

		@Override
		public String format(Object o) {
			if (o==null) {
				return nullValue;
			} else {
				Class<?> objectClass = o.getClass();
				if (Date.class.isAssignableFrom(objectClass)) {
					return DEFAULT_SHORT_DATE.format((Date)o);
				} else if (java.lang.Double.class.isAssignableFrom(objectClass)) {
					return DEFAULT_DOUBLE.format((Double)o);
				} else if (java.lang.Float.class.isAssignableFrom(objectClass)) {
					return DEFAULT_FLOAT.format((Float)o);
				} else if (java.lang.Integer.class.isAssignableFrom(objectClass)) {
					return DEFAULT_INTEGER.format((Integer)o);
				} else if (java.lang.Short.class.isAssignableFrom(objectClass)) {
					return DEFAULT_INTEGER.format((Integer)o);
				} else if (java.lang.Long.class.isAssignableFrom(objectClass)) {
					return DEFAULT_LONG.format((Long)o);
				} else {
					return o.toString();
				}
			}
		}
	}

	public static final Formatter<Object> DEFAULT_NULL_SAFE_EMPTY_NULL = new DefaultFormatter("");

	public static final Formatter<Object> DEFAULT_NULL_SAFE = new DefaultFormatter("null");

	public static final Formatter<Date> DEFAULT_SHORT_SORTABLE_DATE = new Formatter<Date>() {

		@Override
		public String format(Date o) {
			return SHORT_DATE_SORTABLE_FORMAT.format(o);
		}
	};

	public static final Formatter<Date> DEFAULT_LONG_SORTABLE_DATE = new Formatter<Date>() {

		@Override
		public String format(Date o) {
			return LONG_DATE_SORTABLE_FORMAT.format(o);
		}
	};

	public static final Formatter<Date> DEFAULT_SHORT_DATE = new Formatter<Date>() {

		@Override
		public String format(Date o) {
			return SHORT_DATE_FORMAT.format(o);
		}
	};

	public static final Formatter<Date> DEFAULT_LONG_DATE = new Formatter<Date>() {

		@Override
		public String format(Date o) {
			return LONG_DATE_FORMAT.format(o);
		}
	};

	public static final Formatter<Date> DEFAULT_LONG_DATE_WITH_TIMEZONE = new Formatter<Date>() {

		@Override
		public String format(Date o) {
			return LONG_DATE_FORMAT_WITH_TIMEZONE.format(o);
		}
	};
	
	public static final Formatter<Number> DEFAULT_DOUBLE = new Formatter<Number>() {

		@Override
		public String format(Number o) {
			return FLOAT_FORMAT.format(o.doubleValue());
		}
	};

	public static final Formatter<Number> DEFAULT_FLOAT = new Formatter<Number>() {

		@Override
		public String format(Number o) {
			return FLOAT_FORMAT.format(o.floatValue());
		}
	};

	public static final Formatter<Number> DEFAULT_PERCENT = new Formatter<Number>() {

		@Override
		public String format(Number o) {
			return PERCENT_FORMAT.format(o.floatValue());
		}
	};
	
	public static final Formatter<Number> DEFAULT_INTEGER = new Formatter<Number>() {

		@Override
		public String format(Number o) {
			return CARDINAL_FORMAT.format(o.intValue());
		}
	};
	public static final Formatter<Number> DEFAULT_LONG = new Formatter<Number>() {

		@Override
		public String format(Number o) {
			return CARDINAL_FORMAT.format(o.longValue());
		}
	};

	public static final Formatter<Color> DEFAULT_COLOR = new Formatter<Color>() {

		@Override
		public String format(Color o) {
			return String.format("#%02X%02X%02X", 0xff&o.getRed(), 0xff&o.getGreen(), 0xff&o.getBlue());
		}
	};

	public static final Formatter<String> STRING = new Formatter<String>() {

		@Override
		public String format(String o) {
			return o;
		}
	};

	public static final Formatter<String> NULL_SAFE_STRING = new Formatter<String>() {

		@Override
		public String format(String o) {
			if (o!=null) {
				return o;
			} else {
				return "";
			}
		}
	};
	
	private static final Format PERCENT_FORMAT = new DecimalFormat("#,##0.00%");
	private static final Format FLOAT_FORMAT = new DecimalFormat("#,##0.00");
	private static final Format CARDINAL_FORMAT = new DecimalFormat("#,##0");
	private static final Format SHORT_DATE_FORMAT = new SimpleDateFormat("dd-MMM-yyyy");
	private static final Format SHORT_DATE_SORTABLE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");
	private static final Format LONG_DATE_SORTABLE_FORMAT = new SimpleDateFormat("yyyy-MM-dd+HH-mm-ss");
	private static final Format LONG_DATE_FORMAT = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
	private static final Format LONG_DATE_FORMAT_WITH_TIMEZONE = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss z");

	public static final class DecimalFormatter<T extends Number> implements Formatter<T> {

		private DecimalFormatter(int numDigits, boolean useSeparator) {
			this.format=new DecimalFormat();
			this.format.setMaximumFractionDigits(numDigits);
			this.format.setGroupingUsed(useSeparator);
		}
		
		private DecimalFormat format;
		@Override
		public String format(T o) {
			return format.format(o);
		}
		
	}
	
	public static final Formatter<Number> decimalFormatter(int numOfDigits, boolean useSeparator) {
		return new DecimalFormatter<>(numOfDigits,useSeparator);
	}
	
	public static final Formatter<?> wrappedFormat(final Format format) {
		return new Formatter<Object>() {

			@Override
			public String format(Object o) {
				return format.format(o);
			}

		};
	}

	@Override
	public String format(Object o) {
		return o.toString();
	}
}
