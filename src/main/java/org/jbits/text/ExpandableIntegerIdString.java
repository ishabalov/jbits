package org.jbits.text;

import java.util.Collection;

public class ExpandableIntegerIdString {
	private Collection<Integer> idList = null;
	
	private ExpandableIntegerIdString(Collection<Integer> ids) {
		this.idList=ids;
	}
	
	public static final ExpandableIntegerIdString valueOf(String str) {
		return new ExpandableIntegerIdString(TextHelperOld.expandStringId(str));
	}
	
	public Collection<Integer> getIdList() {
		return idList;
	}

}
