package org.jbits.text;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ConfigParser {

	private int currentPosition = 0;
	private char[] buffer;
	public static final String ROOT="*";


	private List<ConfigParserEntry> config = new ArrayList<>();
	private Map<String,ConfigParserEntry> configCatalog = new HashMap<>();

	private void newConfigParserEntry(String id) {
		ConfigParserEntry entry = new ConfigParserEntry(id); 
		config.add(entry);
		configCatalog.put(id,entry);
	}

	private void newProperty(String name, String value) {
		if (config.isEmpty()) {
			newConfigParserEntry(ROOT);
		}
		config.get(config.size()-1).addConfigParserProperty(new ConfigParserProperty(name,value));
	}

	private boolean eof() {
		return currentPosition>=buffer.length;
	}

	private char current() {
		if (currentPosition>=0 && currentPosition<buffer.length) {
			return buffer[currentPosition];
		} else {
			return 0;
		}
	}
	private char next() {
		if (currentPosition>=-1 && currentPosition<buffer.length-1) {
			return buffer[currentPosition+1];
		} else {
			return 0;
		}
	}
	private void skipForward(boolean obeyComments) {
		if (obeyComments) {
			if (current()=='#') {
				while (current()=='#') {
					while (!eof() && current()!='\n') {
						currentPosition++;
					}
					if (current()=='\n') {
						currentPosition++;
					}
				}
			} else {
				currentPosition++;
			}
		} else {
			currentPosition++;
		}
	}

	private String substring(int fromIncluding, int toExcluding) {
		return String.valueOf(buffer, fromIncluding, toExcluding-fromIncluding);
	}

	private void skipToChar(char c, boolean obeyComments) {
		while (!eof() && current()!=c) {
			skipForward(obeyComments);
		}
	}
	private void skipToCharEscaping(char c) {
		while (!eof() && current()!=c) {
			skipForward(false);
			if (current()=='\\' && next()==c) {
				currentPosition+=2;
			}
		}
	}

	private boolean isCurrentCharBlank() {
		char c = current();
		return c==' ' || c=='\t' || c=='\n' || c=='\r'; 
	}

	private void skipToNonBlankChar(boolean obeyComments) {
		while (!eof() && (isCurrentCharBlank() || (obeyComments && current()=='#'))) {
			skipForward(obeyComments);
		}
	}	

	private void parseAll() {
		while (!eof()) {
			skipToNonBlankChar(true);
			if (!eof()) {
				if (current()=='[') {
					int b = currentPosition;
					skipToChar(']', false);
					int e = currentPosition;
					String id = substring(b+1,e);
					newConfigParserEntry(id);
					currentPosition++;
				} else {
					int nb = currentPosition;
					skipToChar('=', false);
					int ne = currentPosition;
					String name = substring(nb, ne).trim();
					currentPosition++;
					skipToNonBlankChar(false);
					int vb = currentPosition;
					String value;
					if (current()=='"' || current()=='\'') {
						char stop = current();
						currentPosition++;
						skipToCharEscaping(stop);
						int ve=currentPosition;
						value = substring(vb+1, ve);
					} else {
						skipToChar('\n', false);
						int ve=currentPosition;
						value = substring(vb, ve);
					}
					currentPosition++;
					newProperty(name, value.trim());
				}
			}
		}
	}

	private ConfigParser(String config) {
		buffer = config.toCharArray();
		parseAll();
	}

	public static final ConfigParser parse(String config) {
		return new ConfigParser(config);
	}

	public IdExpander getSimpleIdExpander() {
		return new IdExpander() {

			@Override
			public Iterable<Object> expandId(String str) {
				List<Object> ret = new ArrayList<Object>();
				ret.add(str);
				return ret;
			}
		};
	}

	public List<ConfigParserEntry> getEntries(IdExpander expander) {
		if (expander!=null) {
			List<ConfigParserEntry> ret = new ArrayList<ConfigParserEntry>();
			for (ConfigParserEntry e:config) {
				for (Object expandedId:expander.expandId(e.getId())) {
					ret.add(new ConfigParserEntry(expandedId.toString(),e.getProperties()));
				}
			}
			return ret;
		} else {
			return config;
		}
	}

	public List<ConfigParserEntry> getEntries() {
		return getEntries(null);
	}
	
	public ConfigParserEntry getEntry(String id) {
		return configCatalog.get(id);
	}
}
