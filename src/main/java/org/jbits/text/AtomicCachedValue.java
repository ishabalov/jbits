package org.jbits.text;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Supplier;

public class AtomicCachedValue<T> extends AtomicReference<T> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4988437454900512002L;
	
	private final Supplier<? extends T> supplier;

	public AtomicCachedValue(Supplier<? extends T> supplier) {
		Objects.requireNonNull(supplier);
		this.supplier = supplier;
	}

	public T computeIfAbsent() {
		return computeIfAbsent(supplier);
	}

	public T computeIfAbsent(Supplier<? extends T> supplier) {
		Objects.requireNonNull(supplier);
		T ret = get();
		if (ret==null) {
			synchronized(this) {
				ret = get();
				if (ret==null) {
					ret = supplier.get();
					set(ret);
				}
			}
		}
		return ret;
	}
	
}
