package org.jbits.text;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ConfigParserEntry {
	private String id;
	private List<ConfigParserProperty> properties = new ArrayList<ConfigParserProperty>();
	private Map<String,ConfigParserProperty> propertiesCatalog = new HashMap<>();
	protected ConfigParserEntry(String id, List<ConfigParserProperty> properties) {
		this.id = id;
		this.properties=properties;
		for (ConfigParserProperty p:properties) {
			propertiesCatalog.put(p.getName(), p);
		}
	}
	protected ConfigParserEntry(String id) {
		this.id = id;
	}
	public String getId() {
		return id;
	}
	public List<ConfigParserProperty> getProperties() {
		return properties;
	}
	public Map<String,String> getPropertiesMap() {
		Map<String,String> ret = new HashMap<String, String>();
		for (ConfigParserProperty p:getProperties()) {
			ret.put(p.getName(), p.getValue());
		}
		return ret;
	}
	public String getProperty(String name, String defaultValue) {
		ConfigParserProperty ret = propertiesCatalog.get(name);
		if (ret!=null) {
			return ret.getValue();
		} else {
			return defaultValue;
		}
	}
	public String getProperty(String name) {
		return getProperty(name, null);
	}
	
	protected void addConfigParserProperty(ConfigParserProperty p) {
		properties.add(p);
		propertiesCatalog.put(p.getName(), p);
	}

	public boolean deepEquals(ConfigParserEntry other) {
		if (id.equals(other.id) && properties.size()==other.properties.size()) {
			List<ConfigParserProperty> a = new ArrayList<ConfigParserProperty>();
			a.addAll(properties);
			Collections.sort(a, new Comparator<ConfigParserProperty>(){

				@Override
				public int compare(ConfigParserProperty o1, ConfigParserProperty o2) {
					return o1.getName().compareTo(o2.getName());
				}});
			List<ConfigParserProperty> b = new ArrayList<ConfigParserProperty>();
			b.addAll(other.properties);
			Collections.sort(b, new Comparator<ConfigParserProperty>(){

				@Override
				public int compare(ConfigParserProperty o1, ConfigParserProperty o2) {
					return o1.getName().compareTo(o2.getName());
				}});
			for (int index=0;index<a.size();index++) {
				ConfigParserProperty aa = a.get(index);
				ConfigParserProperty bb = b.get(index);
				if (aa.getName().equals(bb.getName()) && aa.getValue().equals(bb.getValue())) {
					continue;
				} else {
					return false;
				}
			}
			return true;
		} else {
			return false;
		}
	}
}
