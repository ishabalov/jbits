package org.jbits.text;

public interface Charset {
	public String getName();
	public Object getCharset();
}
