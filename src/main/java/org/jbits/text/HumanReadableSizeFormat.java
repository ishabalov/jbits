package org.jbits.text;

import java.text.DecimalFormat;
import java.text.FieldPosition;
import java.text.Format;
import java.text.ParsePosition;

public class HumanReadableSizeFormat extends Format {

	private static DecimalFormat df = null;

	private static final long serialVersionUID = -843015584891616690L;
	private static final HumanReadableSizeFormat instance = new HumanReadableSizeFormat();
	public static synchronized final HumanReadableSizeFormat getInstance() {
		if( df == null ) {
			df = new DecimalFormat();
			df.setGroupingUsed(true);
			df.setMinimumFractionDigits(1);
			df.setMaximumFractionDigits(3);
		}
		return instance;
	}

	private static final class SizeUnit {
		protected double value;
		protected String unit;
		protected SizeUnit(double value, String unit) {
			this.value=value;
			this.unit=unit;
		}
	}
	private static final SizeUnit units[] = {
		new SizeUnit(1,"B"),
		new SizeUnit(1E3,"K"),
		new SizeUnit(1E6,"M"),
		new SizeUnit(1E9,"G"),
		new SizeUnit(1E12,"T")
	};

	@Override
	public StringBuffer format(Object obj, StringBuffer toAppendTo, FieldPosition pos) {
		double val = (Long)obj;
		for (SizeUnit unit:units) {
			if (val/unit.value<1000) {
				appendFormat(val,unit,toAppendTo);
				return toAppendTo;
			}
		}
		appendFormat(val,units[units.length-1],toAppendTo);
		return toAppendTo;
	}

	private void appendFormat(double val, SizeUnit unit, StringBuffer toAppendTo) {
		toAppendTo.append(df.format(val/unit.value));
		toAppendTo.append(unit.unit);
	}

	@Override
	public Object parseObject(String source, ParsePosition pos) {
		throw new RuntimeException("Method parseObjects is not implemented");
	}


}
