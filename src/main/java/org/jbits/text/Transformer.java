package org.jbits.text;

public interface Transformer {
	String transform(String str);
}
