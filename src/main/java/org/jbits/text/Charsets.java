package org.jbits.text;

public enum Charsets implements Charset {
	UTF_8 {
		public String getName() {
			return "utf-8";
		}

		@Override
		public Object getCharset() {
			return this;
		}
	}
}
