package org.jbits.text;

public interface Formatter<T> {
	String format(T o);
}
