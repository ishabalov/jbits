package org.jbits.newick;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.jbits.text.StandardFormatters;

public class NewickTreeNode {
	private String name;
	private double distanceToParent;
	private double supportValue = Double.NaN;
	private NewickTreeNode parent;
	private List<NewickTreeNode> children = new ArrayList<>();

	private Map<String,String> properties;


	private static final NewickTreeNode newNode(NewickTreeNode parent, String name, Map<String,String> properties, double distance, double supportValue, List<NewickTreeNode> children) {
		NewickTreeNode ret = new NewickTreeNode(parent, name, distance, supportValue, children);
		ret.setProperties(properties);
		return ret;
	}

	public static final NewickTreeNode newEmptyNode() {
		return newNode((NewickTreeNode)null, "", new HashMap<String,String>(), 0.0, Double.NaN, new ArrayList<NewickTreeNode>());
	}
	public static final NewickTreeNode newRootNodeWithChildren(List<NewickTreeNode> children) {
		return newNode((NewickTreeNode)null, "", new HashMap<String,String>(), 0.0, Double.NaN, children);
	}

	protected void setProperties(Map<String,String> properties) {
		this.properties = properties;
	}

	protected NewickTreeNode(NewickTreeNode parent, String name, double distance, double supportValue, List<NewickTreeNode> children) {
		this.parent = parent;
		this.name = name;
		this.distanceToParent = distance;
		this.supportValue = supportValue;
		this.children = children;
		for (NewickTreeNode c:children) {
			c.parent = this;
		}
	}

	public String getName() {
		return name;
	}

	public double getDistanceToParent() {
		return distanceToParent;
	}

	public double getSupportValue() {
		return supportValue;
	}

	public List<NewickTreeNode> getChildren() {
		return children;
	}

	public boolean hasProperty(String name) {
		return properties.containsKey(name);
	}

	public String getProperty(String name) {
		return properties.get(name);
	}

	public NewickTreeNode getParent() {
		return parent;
	}


	public Map<String, String> getProperties() {
		return properties;
	}

	public double getDistanceToTop() {
		if (parent==null) {
			return 0.0;
		} else {
			return distanceToParent + parent.getDistanceToTop();
		}
	}

	public List<NewickTreeNode> getChildrensFiltered(NewickNodeFilter filter) {
		List<NewickTreeNode> ret = new ArrayList<>();
		for (NewickTreeNode child:children) {
			if (filter.accept(child)) {
				ret.add(child);
			}
			ret.addAll(child.getChildrensFiltered(filter));
		}
		return ret;
	}

	public boolean isLeaf() {
		return children.isEmpty();
	}

	public int countLeafChildren() {
		if (isLeaf()) {
			return 1;
		} else {
			int ret = 0;
			for (NewickTreeNode child:children) {
				ret+=child.countLeafChildren();
			}
			return ret;
		}
	}


	public boolean hasChild(String name) {
		if (name.equals(this.name)) {
			return true;
		}
		for (NewickTreeNode child:children) {
			if (name.equals(child.name) || child.hasChild(name)) {
				return true;
			}
		}
		return false;
	}

	public void setProperty(String name, String value) {
		properties.put(name, value);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NewickTreeNode other = (NewickTreeNode) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder ret = new StringBuilder();
		ret.append(name+":"+StandardFormatters.Float.format(supportValue)+":"+StandardFormatters.Float.format(distanceToParent)+"\n");
		for (NewickTreeNode c:children) {
			ret.append(c.toString());
		}
		return ret.toString();
	}

	public String asString(List<String> properties) {
		StringBuilder ret = new StringBuilder();
		appendString(ret,properties);
		ret.append(';');
		return ret.toString();
	}

	private void appendString(StringBuilder buffer, List<String> extraProperties) {
		if (!children.isEmpty()) {
			buffer.append('(');
			Iterator<NewickTreeNode> it = children.iterator();
			while (it.hasNext()) {
				NewickTreeNode child = it.next();
				child.appendString(buffer,extraProperties);
				if (it.hasNext()) {
					buffer.append(',');
				}
			}
			buffer.append(')');
		}
		buffer.append(getName());
		if (!Double.isNaN(supportValue)) {
			buffer.append(':');
			buffer.append(supportValue);
		}
		buffer.append(':');
		buffer.append(distanceToParent);
	}

}
