package org.jbits.newick;

public interface NewickNodeFilter {
	boolean accept(NewickTreeNode node);
}