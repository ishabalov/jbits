package org.jbits.newick;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jbits.io.IoHelper;
import org.jbits.text.TextHelper;


public class NewickTreeParser {

	private String buffer;
	private int position;
	private double defaultDistance;

	private AtomicInteger nextAnonymousNumber;

	public NewickTreeParser(String buffer, double defaultDistance) {
		this.buffer = buffer;
		this.position = 0;
		this.defaultDistance = defaultDistance;
		this.nextAnonymousNumber = new AtomicInteger();
	}

	private NewickTreeParser(String buffer, double defaultDistance, AtomicInteger nextAnonymousNumber) {
		this.buffer = buffer;
		this.position = 0;
		this.defaultDistance = defaultDistance;
		this.nextAnonymousNumber = nextAnonymousNumber;
	}

	protected char current() {
		if (buffer.length()>position) {
			return buffer.charAt(position);
		} else {
			return 0;
		}
	}

	protected NewickTreeParser next() {
		position++;
		return this;
	}

	private String extrachGroupSegment() {
		int start = position;
		if ('('==buffer.charAt(position)) {
			int stack = 1;
			position++;
			while (stack!=0 && position<buffer.length()) {
				if ('('==buffer.charAt(position)) {
					stack++;
				} else if (')'==buffer.charAt(position)) {
					stack--;
				}
				position++;
			}
			if (stack==0) {
				return buffer.substring(start,position);
			} else {
				throw new IllegalArgumentException("Incorrect nhs format:'"+buffer+"'");
			}
		} else {
			return null;
		}
	}
	private String extractHeaderSegment() {
		int start = position;
		int pos = buffer.indexOf(',', position);
		if (pos>0) {
			position=pos+1;
			return buffer.substring(start,pos);
		} else {
			position = buffer.length();
			return buffer.substring(start);
		}
	}

	private static final Pattern HEADER_1 = Pattern.compile("(\\D[\\w\\-\\|\\+\\*\\@\\$\\%\\&]*)(\\:([\\d\\-\\.E]+))?(\\:([\\d\\-\\.E]+))?",Pattern.DOTALL);
	private static final Pattern HEADER_2 = Pattern.compile("([\\d\\-\\.E]+)(\\:([\\d\\-\\.E]+))?",Pattern.DOTALL);
	//	private static final Pattern PROP_SPLITTER = Pattern.compile("[\\:]",Pattern.DOTALL);
	//	private static final Pattern PROP = Pattern.compile("([^=^\\s]+)\\s*\\=(.+)",Pattern.DOTALL);

	public List<NewickTreeNode> parse(NewickTreeNode parent) {
		List<NewickTreeNode> ret = new ArrayList<>();
		while (hasMore()) {
			List<NewickTreeNode> children;
			String groupSegment = extrachGroupSegment();
			if (groupSegment!=null) {
				NewickTreeParser groupParser = new NewickTreeParser(groupSegment.substring(1, groupSegment.length()-1),defaultDistance,nextAnonymousNumber);
				children = groupParser.parse(null);
			} else {
				children = new ArrayList<>();
			}
			String headerSegment = extractHeaderSegment();
			if (TextHelper.isEmptyButNotNull(headerSegment)) {
				ret.add(new NewickTreeNode(parent, nextAnonymousName(), defaultDistance, Double.NaN, children));
			} else {
				Matcher headerMatcher = HEADER_1.matcher(headerSegment);
				if (headerMatcher.matches()) {
					String name = headerMatcher.group(1);
					double distance;
					double supportValue;
					if (headerMatcher.group(5)!=null) {
						distance = Double.valueOf(headerMatcher.group(5));
						if (headerMatcher.group(3)!=null) {
							supportValue = Double.valueOf(headerMatcher.group(3));
						} else {
							supportValue = Double.NaN;
						}
					} else {
						supportValue = Double.NaN;
						if (headerMatcher.group(3)!=null) {
							distance = Double.valueOf(headerMatcher.group(3));
						} else {
							distance = defaultDistance;
						}
					}
					ret.add(new NewickTreeNode(parent, name, distance, supportValue, children));
				} else {
					headerMatcher = HEADER_2.matcher(headerSegment);
					if (headerMatcher.matches()) {
						String name = nextAnonymousName();
						double distance;
						double supportValue;
						if (headerMatcher.group(3)!=null) {
							distance = Double.valueOf(headerMatcher.group(3));
							if (headerMatcher.group(1)!=null) {
								supportValue = Double.valueOf(headerMatcher.group(1));
							} else {
								supportValue = Double.NaN;
							}
						} else {
							supportValue = Double.NaN;
							if (headerMatcher.group(1)!=null) {
								distance = Double.valueOf(headerMatcher.group(3));
							} else {
								distance = defaultDistance;
							}
						}
						ret.add(new NewickTreeNode(parent, name, distance, supportValue, children));
					} else {
						throw new IllegalArgumentException("Incorrect header:'"+headerSegment+"'");
					}
				}
			}
		}
		return ret;
	}


	//	private static final String NHX_PROP = "&&NHX";
	//	private NewickTreeNode newNewickTreeNode(NewickTreeNode parent, String name, double distance, String properties, List<NewickTreeNode> children) {
	//		NewickTreeNode ret = new NewickTreeNode(parent, name, distance, children);
	//		String props[] = PROP_SPLITTER.split(properties);
	//		boolean isNHX = props.length>0 && NHX_PROP.equals(props[0]);
	//		Map<String,String> parsedProperties = new HashMap<>();
	//		for (String propStr:props) {
	//			if (!TextHelper.isNullOrEmpty(propStr)) {
	//				Matcher propMatcher = PROP.matcher(propStr);
	//				if (propMatcher.matches()) {
	//					String propName = propMatcher.group(1);
	//					String propValue = propMatcher.group(2);
	//					if (!isNHX || !parseNHSProperies(ret, propName, propValue)) {
	//						parsedProperties.put(propName, propValue);
	//					}
	//				} else {
	//					parsedProperties.put(propStr, "");
	//				}
	//			}
	//		}
	//		ret.setProperties(parsedProperties);
	//		return ret;
	//	}

	//	private Boolean toBooleanYN(String YN) {
	//		switch (YN.toUpperCase()) {
	//		case "Y": return Boolean.TRUE;
	//		case "N": return Boolean.FALSE;
	//		default: throw new RuntimeException("Illegal Y/N value:'"+YN+"'");
	//		}
	//	}

	//	public static final String P_BOOT = "B";
	//	public static final String P_SPECIES = "S";
	//	public static final String P_TAXON = "T";
	//	public static final String P_EC = "E";
	//	public static final String P_DUP = "D";
	//	public static final String P_ORT = "O";
	//	public static final String P_SUP_ORT = "SO";
	//	public static final String P_LIKE = "L";
	//	public static final String P_WORSE = "Sw";
	//	public static final String P_DEF_COLL = "So";
	//	
	//	
	//	private boolean parseNHSProperies(NewickTreeNode ret, String propName, String propValue) {
	//		switch (propName) {
	//		case P_BOOT:
	//			ret.setBootstrapValue(Integer.valueOf(propValue));
	//			return true;
	//		case P_SPECIES:
	//			ret.setSpeciesName(propValue);
	//			return true;
	//		case P_TAXON:
	//			ret.setNcbiTaxonomyId(Integer.valueOf(propValue));
	//			return true;
	//		case P_EC:
	//			ret.setEcNumber(propValue);
	//			return true;
	//		case P_DUP:
	//			ret.setIsDuplicationEvent(toBooleanYN(propValue));
	//			return true;
	//		case P_ORT:
	//			ret.setOrthologous(Integer.valueOf(propValue));
	//			return true;
	//		case P_SUP_ORT:
	//			ret.setSuperOrthologous(Integer.valueOf(propValue));
	//			return true;
	//		case P_LIKE:
	//			ret.setLogLikelihoodValue(Float.valueOf(propValue));
	//			return true;
	//		case P_WORSE:
	//			ret.setWorseIfSubtree(toBooleanYN(propValue));
	//			return true;
	//		case P_DEF_COLL:
	//			ret.setDefaultCollapse(toBooleanYN(propValue));
	//			return true;
	//		default:
	//			return false;
	//		}
	//	}

	private String nextAnonymousName() {
		return String.format("n%d", nextAnonymousNumber.incrementAndGet());
	}

	private boolean hasMore() {
		return position<buffer.length();
	}

	public static final void main(String[] args) throws IOException {
		Path in = Paths.get("/tmp/test.tree");
		String def = IoHelper.readFile(in,StandardCharsets.UTF_8);
		//		def = TextHelper.trimEtherEnd(def, '\n',';',' ');
		//		String input =	"(X1Cocvi1:9.6E-4[&&NHX:S=Cocvi1],(X2CocheC5_3:1.4E-4[&&NHX:S=CocheC5_3],X3CocheC4_1:1.4E-4[&&NHX:S=CocheC4_1])X4a30:0.00424[&&NHX:S=a30],X5Cocca1:0.00113[&&NHX:S=Cocca1])X6a25:0.003[&&NHX:S=a25],X7Cocmi1:0.00575[&&NHX:S=Cocmi1],X8Cocmi1:0.00575[&&NHX:S=Cocmi1]";
		//		long started = PerformanceTimerHelper.getNanotime();
		NewickTreeParser p = new NewickTreeParser(def,0.0);
		List<NewickTreeNode> roots = p.parse(null);
		List<NewickTreeNode> allLeaves = new ArrayList<>(); 
		//		PerformanceTimerHelper.writeIfElapsed("parser time:", started, 0, " ms.", System.out);
		for (NewickTreeNode root:roots) {
			allLeaves.addAll(root.getChildrensFiltered(new NewickNodeFilter() {

				@Override
				public boolean accept(NewickTreeNode node) {
					return node.isLeaf();
				}
			}));

			Path out = Paths.get("/tmp").resolve(root.getName());
			String str = root.asString(new ArrayList<String>());
			IoHelper.saveStringToFile(out, str);


		}
		for (NewickTreeNode n:allLeaves) {
			System.out.println(n.getName()+":"+n.getDistanceToTop());
		}
	}
}
