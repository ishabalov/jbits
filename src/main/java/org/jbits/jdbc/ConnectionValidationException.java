package org.jbits.jdbc;

import java.sql.SQLException;

public class ConnectionValidationException extends SQLException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2152436271889430859L;

	public ConnectionValidationException() {
	}

	public ConnectionValidationException(String reason) {
		super(reason);
	}

	public ConnectionValidationException(Throwable cause) {
		super(cause);
	}

	public ConnectionValidationException(String reason, String SQLState) {
		super(reason, SQLState);
	}

	public ConnectionValidationException(String reason, Throwable cause) {
		super(reason, cause);
	}

	public ConnectionValidationException(String reason, String SQLState, int vendorCode) {
		super(reason, SQLState, vendorCode);
	}

	public ConnectionValidationException(String reason, String sqlState, Throwable cause) {
		super(reason, sqlState, cause);
	}

	public ConnectionValidationException(String reason, String sqlState, int vendorCode, Throwable cause) {
		super(reason, sqlState, vendorCode, cause);
	}

}
