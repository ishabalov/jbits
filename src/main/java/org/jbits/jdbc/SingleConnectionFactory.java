package org.jbits.jdbc;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

import org.jbits.text.ConfigParserEntry;

public class SingleConnectionFactory extends AbstractConnectionsFactory {
	
	public SingleConnectionFactory(String url, String user, String password) {
		super(url,user,password);
	}

	public SingleConnectionFactory(ConfigParserEntry configEntry) {
		super(configEntry);
	}
	
	private AtomicReference<Connection> connection = new AtomicReference<>();
	private AtomicInteger counter  = new AtomicInteger(); 

	public int getCounter() {
		return counter.get();
	}

	@Override
	public Connection getOrCreateConnection() throws SQLException {
		Connection ret = connection.get();
		if (ret==null) {
			ret = createConnection();
			connection.set(ret);
		}
		counter.incrementAndGet();
		return ret;
	}

	@Override
	public void release(Connection connection) {
		counter.decrementAndGet();
	}
}
