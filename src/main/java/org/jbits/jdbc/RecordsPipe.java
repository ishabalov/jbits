package org.jbits.jdbc;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public interface RecordsPipe {
        void beforeSelectStatementExecute(PreparedStatement statement) throws SQLException;
        void beforeUpdateStatementExecute(PreparedStatement statement, ResultSet resultset) throws SQLException;
}
