package org.jbits.jdbc;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public interface QueryPreparer {
	 void beforeStatementExecute(PreparedStatement statement) throws SQLException;
}
