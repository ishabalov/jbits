package org.jbits.jdbc.mapping;

import java.sql.ResultSet;
import java.sql.SQLException;

public enum FieldAdapter {
	generic;
	public Object getObject(String name, ResultSet resultset) throws SQLException {
		return resultset.getObject(name);
	}
}
