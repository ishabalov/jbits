package org.jbits.jdbc.mapping;

public class MappingException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3089155436136347311L;

	public MappingException() {
	}

	public MappingException(String message) {
		super(message);
	}

	public MappingException(Throwable cause) {
		super(cause);
	}

	public MappingException(String message, Throwable cause) {
		super(message, cause);
	}

	public MappingException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
