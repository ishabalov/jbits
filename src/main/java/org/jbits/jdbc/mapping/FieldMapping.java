package org.jbits.jdbc.mapping;

public class FieldMapping {
	private String columnName;
	private String fieldName;
	private Class<?> fieldClass;
	private FieldAdapter adapter;
	protected FieldMapping(String columnName, String fieldName, Class<?> fieldClass, FieldAdapter adapter) {
		this.columnName = columnName;
		this.fieldName = fieldName;
		this.fieldClass = fieldClass;
		this.adapter = adapter;
	}
	public String getColumnName() {
		return columnName;
	}
	public String getFieldName() {
		return fieldName;
	}
	public FieldAdapter getAdapter() {
		return adapter;
	}
	public Class<?> getFieldClass() {
		return fieldClass;
	}
}
