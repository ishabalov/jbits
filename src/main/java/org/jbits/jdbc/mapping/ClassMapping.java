package org.jbits.jdbc.mapping;

import java.util.ArrayList;

import org.jbits.text.TextHelper;

public class ClassMapping {
	private Class<?> clasz;
	private String getByIdSql;
	private String idColumnName;
	private FieldMapping[] fields;

	public ClassMapping(Class<?> clasz) {
		Mapped mapped = clasz.getAnnotation(Mapped.class);
		if (mapped!=null) {
			this.clasz=clasz;
			loadFields();
			String byIdQuery = mapped.byIdQuery();
			if (TextHelper.isNullOrEmpty(byIdQuery)) {
				String tableName = mapped.value();
				if (TextHelper.isNullOrEmpty(tableName)) {
					tableName = clasz.getSimpleName();
				}
				this.getByIdSql = String.format("select * from %s where %s=?",tableName, idColumnName);
			} else {
				this.getByIdSql = byIdQuery;
			}
		} else {
			throw new MappingException("Unmapped class '"+clasz.getName()+"'");
		}
	}

	private void loadFields() {
		ArrayList<FieldMapping> list = new ArrayList<>();
		for (java.lang.reflect.Field f:clasz.getFields()) {
			String columnName;
			Field fieldAnnotation = f.getAnnotation(Field.class);
			if (fieldAnnotation!=null) {
				columnName = fieldAnnotation.value();
				if (TextHelper.isNullOrEmpty(columnName)) {
					columnName = f.getName();
				}
				list.add(new FieldMapping(columnName, f.getName(), f.getType(), fieldAnnotation.convert()));
				if (f.getAnnotation(Id.class)!=null) {
					idColumnName = columnName;
				}
			}

		}
	}

	public Class<?> getClasz() {
		return clasz;
	}
	public String getByIdSql() {
		return getByIdSql;
	}
	public FieldMapping[] getFields() {
		return fields;
	}
}
