package org.jbits.jdbc;

import java.sql.Connection;
import java.sql.SQLException;

public interface ConnectionsFactory {
	public Connection getOrCreateConnection() throws SQLException;
	public void release(Connection connection);
}
