package org.jbits.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface RecordsConsumer extends QueryPreparer {
    void consume(ResultSet resultset) throws SQLException;
}
