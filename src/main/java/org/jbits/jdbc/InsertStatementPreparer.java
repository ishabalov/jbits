package org.jbits.jdbc;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public interface InsertStatementPreparer extends QueryPreparer {
        void afterStatementExecute(PreparedStatement statement) throws SQLException;
}
