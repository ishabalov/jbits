package org.jbits.jdbc;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SimpleSelectQueryConnectionvalidator implements ConnectionValidator {

	private String query;
	public SimpleSelectQueryConnectionvalidator(String query) {
		this.query = query;
	}
	@Override
	public void beforeStatementExecute(PreparedStatement statement) throws SQLException {
	}
	@Override
	public String getSql() {
		return query;
	}
	@Override
	public boolean isResultsetValid(ResultSet resultset) throws SQLException {
		return true;
	}

}
