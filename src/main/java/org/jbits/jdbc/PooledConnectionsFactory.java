package org.jbits.jdbc;

import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.SQLException;

import org.jbits.collections.ObjectsFactory;
import org.jbits.collections.ObjectsPool;
import org.jbits.reflect.ReflectionHelper;
import org.jbits.text.ConfigParserEntry;
import org.jbits.text.TextHelper;
import org.jbits.time.TimeInterval;

public class PooledConnectionsFactory extends AbstractConnectionsFactory implements ObjectsFactory<ConnectionDelegate> {

	private ObjectsPool<ConnectionDelegate> pool;
	private TimeInterval poolGetTimeout;
	private TimeInterval connectionValidationInterval;
	private ConnectionValidator connectionValidator;
	private boolean validateConnectionBeforeCheckout; 

	public PooledConnectionsFactory(String url, String user, String password, int poolSize, TimeInterval expirationTimeout, TimeInterval poolGetTimeout, TimeInterval connectionValidationInterval, ConnectionValidator connectionValidator, boolean validateConnectionBeforeCheckout) {
		super(url,user,password);
		this.poolGetTimeout = poolGetTimeout;
		this.connectionValidationInterval = connectionValidationInterval;
		this.connectionValidator = connectionValidator;
		this.validateConnectionBeforeCheckout = validateConnectionBeforeCheckout;
	}
	public PooledConnectionsFactory(String url, String user, String password, int poolSize, TimeInterval expirationTimeout, TimeInterval poolGetTimeout) {
		this(url,user,password,poolSize,expirationTimeout,poolGetTimeout,null,null,false);
	}

	public PooledConnectionsFactory(ConfigParserEntry configEntry) throws SecurityException, NoSuchMethodException, IllegalArgumentException, InstantiationException, IllegalAccessException, InvocationTargetException, ClassNotFoundException {
		super(configEntry);
		int poolSize = TextHelper.nullSafeToInt(configEntry.getProperty("poolSize"),Integer.MAX_VALUE);
		TimeInterval expirationTimeout = TextHelper.nullSafeToTimeInterval(configEntry.getProperty("expirationTimeout"),TimeInterval.MAX_VALUE_INTERVAL);
		this.poolGetTimeout = TextHelper.nullSafeToTimeInterval(configEntry.getProperty("poolGetTimeout"),TimeInterval.MAX_VALUE_INTERVAL);
		this.connectionValidationInterval = TextHelper.nullSafeToTimeInterval(configEntry.getProperty("connectionValidationInterval"),null);
		ConnectionValidator connectionValidator = null;
		String connectionValidatorClass = configEntry.getProperty("connectionValidatorClass");
		if (connectionValidatorClass==null) {
			String connectionValidatorType = configEntry.getProperty("connectionValidatorType"); 
			if (!TextHelper.isNullOrEmpty(connectionValidatorType)) {
				connectionValidator = JdbcHelper.getConnectionvalidatorByType(connectionValidatorType);
			} else {
				String connectionValidatorSql = configEntry.getProperty("connectionValidatorSql");
				if (!TextHelper.isNullOrEmpty(connectionValidatorSql)) {
					connectionValidator = new SimpleSelectQueryConnectionvalidator(connectionValidatorSql);
				}
			}
		} else {
			connectionValidator = (ConnectionValidator) ReflectionHelper.newInstance(connectionValidatorClass);
		}
		this.connectionValidator = connectionValidator;
		this.validateConnectionBeforeCheckout = TextHelper.nullSafeToBooleanExtended(configEntry.getProperty("validateConnectionBeforeCheckout"),false);
		this.pool = new ObjectsPool<ConnectionDelegate>(this, poolSize, expirationTimeout);
	}

	@Override
	public Connection getOrCreateConnection() throws SQLException {
		if (validateConnectionBeforeCheckout && connectionValidator!=null) {
			long started = System.currentTimeMillis();
			while (true) {
				ConnectionDelegate ret = null;
				try {
					ret = pool.getOrCreate(poolGetTimeout);
					if (ret.needValidation(connectionValidationInterval)) {
						JdbcHelper.validateConnection(ret, connectionValidator);
						return ret;
					} else {
						return ret;
					}
				} catch (Exception e) {
					pool.discardObject(ret);
					// ignore exception, try 1 more time
					try {
						Thread.sleep(TimeInterval.SEC);
					} catch (InterruptedException ex) {
						// just interrupted
					}
				}
				if (System.currentTimeMillis()>started+poolGetTimeout.milliseconds()) {
					throw new ConnectionValidationException("Unable to obtain valid connection within requested interval");
				}
			}
		} else {
			return pool.getOrCreate(poolGetTimeout);
		}
	}

	public void validateAllConnections() {
		if (connectionValidator!=null) {
			int max = pool.getPoolFree();
			for (int i=0;i<max;i++) {
				ConnectionDelegate ret = null;
				try {
					ret = pool.getOrCreate(poolGetTimeout);
					if (ret.needValidation(connectionValidationInterval)) {
						JdbcHelper.validateConnection(ret, connectionValidator);
					}
					pool.release(ret);
				} catch (Exception e) {
					pool.discardObject(ret);
					// ignore exception, just discard connection
				}
			}
		}
	}

	public boolean isDatabaseValid() throws SQLException {
		if (connectionValidator!=null) {
			ConnectionDelegate ret = null;
			try {
				ret = pool.getOrCreate(poolGetTimeout);
				JdbcHelper.validateConnection(ret, connectionValidator);
				pool.release(ret);
				return true;
			} catch (Exception e) {
				pool.discardObject(ret);
				// ignore exception, just discard connection
				throw new SQLException(e);
			}
		} else {
			return true;
		}
	}

	@Override
	public void release(Connection connection) {
		pool.release((ConnectionDelegate)connection);
	}

	@Override
	public ConnectionDelegate create() {
		try {
			return createConnection();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void discard(ConnectionDelegate connection) {
		try {
			connection.discard();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
}
