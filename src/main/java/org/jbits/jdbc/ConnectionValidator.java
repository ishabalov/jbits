package org.jbits.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface ConnectionValidator extends QueryPreparer {
	String getSql();
	boolean isResultsetValid(ResultSet resultset) throws SQLException;
}
