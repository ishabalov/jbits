package org.jbits.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicReference;

import org.jbits.jdbc.mapping.ClassMapping;
import org.jbits.jdbc.mapping.FieldMapping;
import org.jbits.jdbc.mapping.MappingException;
import org.jbits.reflect.ReflectionHelper;

public class JdbcHelper {

	public static final void executeSelectStatement(ConnectionsFactory cf, String query, RecordsConsumer consumer) throws SQLException {
		try (Connection c = cf.getOrCreateConnection()) {
			executeSelectStatement(c, query, consumer);
		}
	}

	protected static final void executeSelectStatement(Connection c, String query, RecordsConsumer consumer) throws SQLException {
		try (PreparedStatement s = c.prepareStatement(query, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY, ResultSet.CLOSE_CURSORS_AT_COMMIT)) {
			s.setFetchDirection(ResultSet.FETCH_FORWARD);
			s.setFetchSize(Integer.MIN_VALUE); // Stupid MySQL implementation of JDBC requires fetch size set to -2,147,483,648
			consumer.beforeStatementExecute(s);
			try (ResultSet r = s.executeQuery()) {
				boolean hasMoreRows = r.next();
				while(hasMoreRows) {
					consumer.consume(r);
					hasMoreRows = r.next();
				}
			}
		}
	}

	protected static final void validateConnection(Connection c, ConnectionValidator validator) throws SQLException {
		try (PreparedStatement s = c.prepareStatement(validator.getSql(), ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY, ResultSet.CLOSE_CURSORS_AT_COMMIT)) {
			s.setFetchDirection(ResultSet.FETCH_FORWARD);
			s.setFetchSize(Integer.MIN_VALUE); // Stupid MySQL implementation of JDBC requires fetch size set to -2,147,483,648
			validator.beforeStatementExecute(s);
			try (ResultSet r = s.executeQuery()) {
				boolean hasMoreRows = r.next();
				while(hasMoreRows) {
					if (!validator.isResultsetValid(r)) {
						throw new ConnectionValidationException("Connection failed validation");
					}
					hasMoreRows = r.next();
				}
			}
		}
	}

	public static final int executeUpdateStatement(ConnectionsFactory connectionFactory, String query, QueryPreparer preparer) throws SQLException {
		try (Connection c = connectionFactory.getOrCreateConnection()) {
			try (PreparedStatement s = c.prepareStatement(query, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY, ResultSet.CLOSE_CURSORS_AT_COMMIT)) {
				if (preparer!=null) {
					preparer.beforeStatementExecute(s);
				}
				int ret = s.executeUpdate();
				if (preparer!=null && InsertStatementPreparer.class.isAssignableFrom(preparer.
						getClass())) {
					((InsertStatementPreparer)preparer).afterStatementExecute(s);
				}
				return ret;
			}
		}
	}	

	public static final int executePipeStatement(ConnectionsFactory selectConnectionFactory, String selectQuery, ConnectionsFactory updateConnectionFactory, String updateQuery, RecordsPipe pipe) throws SQLException {
		int ret = 0;
		try (Connection selectConnection = selectConnectionFactory.getOrCreateConnection()) {
			try (PreparedStatement select = selectConnection.prepareStatement(selectQuery, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY, ResultSet.CLOSE_CURSORS_AT_COMMIT)) {
				select.setFetchDirection(ResultSet.FETCH_FORWARD);
				select.setFetchSize(Integer.MIN_VALUE); // Stupid MySQL implementation of JDBC requires fetch size set to -2,147,483,648
				pipe.beforeSelectStatementExecute(select);
				try (ResultSet selectResult = select.executeQuery()) {
					boolean hasMoreRows = selectResult.next();
					if (hasMoreRows) {
						try (Connection updateConnection = updateConnectionFactory.getOrCreateConnection()) {
							while(hasMoreRows) {
								try (PreparedStatement update = updateConnection.prepareStatement(updateQuery, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY, ResultSet.CLOSE_CURSORS_AT_COMMIT)) {
									if (pipe!=null) {
										pipe.beforeUpdateStatementExecute(update,selectResult);
									}
									ret += update.executeUpdate();
								}
								hasMoreRows = selectResult.next();
							}
						}
					}
				}
			}
		}
		return ret;
	}

	public static final String HSQLDB = "hsqldb";
	public static final String ORACLE = "oracle";
	public static final String DB2 = "db2";
	public static final String MYSQL = "mysql";
	public static final String MSSQL = "mssql";
	public static final String POSTGRESQL = "postgresql";
	public static final String INGRES = "ingres";
	public static final String DERBY = "derby";
	public static final String H2 = "h2";

	public static final ConnectionValidator getConnectionvalidatorByType(String type) {
		switch (type) {
		case HSQLDB:
			return new SelectOneConnectionValidator("select 1 from INFORMATION_SCHEMA.SYSTEM_USERS");
		case ORACLE:
			return new SelectOneConnectionValidator("select 1 from dual");
		case DB2:
			return new SelectOneConnectionValidator("select 1 from sysibm.sysdummy1");
		case MYSQL:
		case MSSQL:
		case POSTGRESQL:
		case INGRES:
		case H2:
			return new SelectOneConnectionValidator("select 1");
		case DERBY:
			return new SelectOneConnectionValidator("values 1");
		default:
			throw new RuntimeException("Illegal database type '"+type+"'");
		}
	}

	private static class SelectOneConnectionValidator implements ConnectionValidator {
		private String sql;

		private SelectOneConnectionValidator(String sql) {
			this.sql = sql;
		}

		@Override
		public void beforeStatementExecute(PreparedStatement statement) throws SQLException {
		}

		@Override
		public String getSql() {
			return sql;
		}

		@Override
		public boolean isResultsetValid(ResultSet resultset) throws SQLException {
			return resultset.getInt(1)==1;
		}

	}

	private static final Map<Class<?>,ClassMapping> CLASS_MAPPINGS = new ConcurrentHashMap<>();

	private static final ClassMapping getOrCreateClassMapping(Class<?> clasz) {
		ClassMapping ret = CLASS_MAPPINGS.get(clasz);
		if (ret == null) {
			ret = new ClassMapping(clasz);
			CLASS_MAPPINGS.put(clasz, ret);
		}
		return ret;
	}

	private static final String getSelectByIdSql(Class<?> clasz) {
		return getOrCreateClassMapping(clasz).getByIdSql();
	}
	private static final <T> T createAndMapEntity(Class<T> clasz, ResultSet r)  {
		try {
			T ret = ReflectionHelper.newInstance(clasz);
			for (FieldMapping f:getOrCreateClassMapping(clasz).getFields()){
				Object val = ReflectionHelper.castTo(f.getAdapter().getObject(f.getColumnName(),r),f.getFieldClass());
				ReflectionHelper.setField(ret, f.getFieldName(), val);
			}
			return ret;
		} catch (Exception e) {
			throw new MappingException(e);
		}
	}

	public static final <T> T loadMappedObjectById(ConnectionsFactory cf, final Class<T> clasz, final Object id) throws SQLException {
		final AtomicReference<T> ret = new AtomicReference<T>(); 
		executeSelectStatement(cf, getSelectByIdSql(clasz), new RecordsConsumer() {

			@Override
			public void beforeStatementExecute(PreparedStatement statement) throws SQLException {
				statement.setObject(1, id);
			}

			@Override
			public void consume(ResultSet resultset) throws SQLException {
				ret.set(createAndMapEntity(clasz,resultset));
			}
		});
		return ret.get();
	}
}