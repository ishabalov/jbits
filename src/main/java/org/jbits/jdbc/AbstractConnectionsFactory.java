package org.jbits.jdbc;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import org.jbits.text.ConfigParserEntry;

public abstract class AbstractConnectionsFactory implements ConnectionsFactory {

	private String url;
	private String user;
	private String password;

	public AbstractConnectionsFactory(String url, String user, String password) {
		this.url = url;
		this.user = user;
		this.password = password;
	}

	public AbstractConnectionsFactory(ConfigParserEntry configEntry) {
		this.url = configEntry.getProperty("url");
		this.user = configEntry.getProperty("user");
		this.password = configEntry.getProperty("password");
	}

	protected ConnectionDelegate createConnection() throws SQLException {
		Properties props = new Properties();
		props.setProperty("user", user);
		props.setProperty("password", password);
		return new ConnectionDelegate(this, DriverManager.getConnection(url, props));
	}


}
