package org.jbits.time;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class CalendarHelper {

	public static final Format DEFAULT_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

	public static enum RollMode {
		NoRoll, RollUp, RollDown
	};

	public static final Calendar today(RollMode roll) {
		return toRollMode(Calendar.getInstance(),roll);
	}

	public static final Calendar toRollMode(Calendar cal, RollMode roll) {
		if (roll.equals(RollMode.RollUp)) {
			cal.set(Calendar.HOUR_OF_DAY, 23);
			cal.set(Calendar.MINUTE, 59);
			cal.set(Calendar.SECOND, 59);
			cal.set(Calendar.MILLISECOND, 999);
		} else if (roll.equals(RollMode.RollDown)) {
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 0);
			cal.set(Calendar.MILLISECOND, 0);
		}
		return cal;
	}

	public static final Calendar fromDate(Date date, RollMode roll) {
		Calendar ret = GregorianCalendar.getInstance();
		ret.setTime(date);
		return toRollMode(ret, roll);
	}
	
	public static final Calendar today() {
		return today(RollMode.NoRoll);
	}

	public static final Calendar yesturday() {
		Calendar ret = today(RollMode.NoRoll);
		ret.roll(Calendar.DATE, false);
		return ret;
	}

	public static final void rollForward(Calendar cal, TimeInterval interval) {
		TimeInterval.Fractions fractions = interval.fractions();
		cal.add(Calendar.DAY_OF_MONTH,fractions.getDays());
		cal.add(Calendar.HOUR,fractions.getHours());
		cal.add(Calendar.MINUTE,fractions.getMinutes());
		cal.add(Calendar.MILLISECOND,fractions.getMilliseconds());
	}
	public static final void rollBacward(Calendar cal, TimeInterval interval) {
		TimeInterval.Fractions fractions = interval.fractions();
		cal.add(Calendar.DAY_OF_MONTH,-fractions.getDays());
		cal.add(Calendar.HOUR,-fractions.getHours());
		cal.add(Calendar.MINUTE,-fractions.getMinutes());
		cal.add(Calendar.MILLISECOND,-fractions.getMilliseconds());
	}

	public static final String formatDateByDefaultFormat(Date date) {
		return DEFAULT_DATE_FORMAT.format(date);
	}

	public static final String formatNowByDefaultFormat() {
		return DEFAULT_DATE_FORMAT.format(today().getTime());
	}

	public static final String now() {
		Calendar cal = Calendar.getInstance();
		return DEFAULT_DATE_FORMAT.format(cal.getTime());
	}

	public static final Date currentDate() {
		return Calendar.getInstance().getTime();
	}
	
	public static final Date dateFromTimestamp(long utc) {
		Date ret = new Date(utc);
		return ret;
	}

}
