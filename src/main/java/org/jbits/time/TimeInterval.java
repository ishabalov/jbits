package org.jbits.time;

import java.io.Serializable;
import java.text.ParseException;
import java.time.Duration;
import java.util.Date;

public class TimeInterval implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2666098644813852229L;
	public static final long ZERO = 0L;
	public static final TimeInterval ZERO_INTERVAL = new TimeInterval(ZERO);

	public static final long MSEC = 1L;
	public static final long MSEC_NANO = 1000000L;
	public static final TimeInterval MSEC_INTERVAL = new TimeInterval(MSEC);

	public static final long SEC = 1000L * MSEC;
	public static final long SEC_NANO = 1000L * MSEC_NANO;
	public static final TimeInterval SEC_INTERVAL = new TimeInterval(SEC);

	public static final long MIN = 60L * SEC;
	public static final long MIN_NANO = 60L * SEC_NANO;
	public static final TimeInterval MIN_INTERVAL = new TimeInterval(MIN);

	public static final long HOUR = 60L * MIN;
	public static final long HOUR_NANO = 60L * MIN_NANO;
	public static final TimeInterval HOUR_INTERVAL = new TimeInterval(HOUR);

	public static final long DAY = 24L * HOUR;
	public static final long DAY_NANO = 24L * HOUR_NANO;
	public static final TimeInterval DAY_INTERVAL = new TimeInterval(DAY);

	public static final long WEEK = 7L * DAY;
	public static final long WEEK_NANO = 7L * DAY_NANO;
	public static final TimeInterval WEEK_INTERVAL = new TimeInterval(WEEK);

	public static final long DECADE = 10L * DAY;
	public static final long DECADE_NANO = 10L * DAY_NANO;
	public static final TimeInterval DECADE_INTERVAL = new TimeInterval(DECADE);

	public static final long QUATER = 90L * DAY;
	public static final long QUATER_NANO = 90L * DAY_NANO;
	public static final TimeInterval QUATER_INTERVAL = new TimeInterval(QUATER);

	public static final long MAX_VALUE = Long.MAX_VALUE;
	public static final TimeInterval MAX_VALUE_INTERVAL = new TimeInterval(MAX_VALUE);
	public static final TimeInterval ETERNITY = MAX_VALUE_INTERVAL;

	private final long interval;

	private TimeInterval(long interval) {
		this.interval = interval;
	}

	public long milliseconds() {
		return interval;
	}

	public int seconds() {
		return (int)(interval/1000);
	}

	public static final TimeInterval valueOf(String value) {
		if (value != null) {
			try {
				return new TimeInterval((Long) TimeIntervalFormat.getDefaultInstance().parseObject(value));
			} catch (ParseException e) {
				throw new RuntimeException(e);
			}
		} else {
			return null;
		}
	}
	public static final TimeInterval valueOf(long value) {
		return new TimeInterval(value);
	}
	public String toString() {
		TimeIntervalFormat fmt = TimeIntervalFormat.getDefaultInstance();
		return fmt.format(this);
	}

	public boolean longerThanThisInThePast(Date date) {
		return date.getTime()+milliseconds()<=System.currentTimeMillis();
	}

	public static final class Fractions {
		private int days;
		private int hours;
		private int minutes;
		private int seconds;
		private int milliseconds;
		private Fractions(int days, int hours, int minutes, int seconds,
				int milliseconds) {
			this.days = days;
			this.hours = hours;
			this.minutes = minutes;
			this.seconds = seconds;
			this.milliseconds = milliseconds;
		}
		public int getDays() {
			return days;
		}
		public int getHours() {
			return hours;
		}
		public int getMinutes() {
			return minutes;
		}
		public int getSeconds() {
			return seconds;
		}
		public int getMilliseconds() {
			return milliseconds;
		}
	}

	public Fractions fractions() {
		long val = milliseconds();
		int days = (int)(val / DAY);
		val = val - (days * DAY);
		int hours = (int)(val / HOUR);
		val = val - (hours * HOUR);
		int minutes = (int)(val / MIN);
		val = val - (minutes * MIN);
		int seconds = (int)(val / SEC);
		val = val - (seconds * SEC);
		return new Fractions(days, hours, minutes, seconds, (int)val);
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (interval ^ (interval >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TimeInterval other = (TimeInterval) obj;
		if (interval != other.interval)
			return false;
		return true;
	}

	public static final TimeInterval max(TimeInterval... a) {
		if (a==null) {
			return null;
		} else {
			TimeInterval ret = null;
			for (TimeInterval x:a) {
				if (x!=null) {
					if (ret==null) {
						ret = x;
					} else {
						if (x.interval>ret.interval) {
							ret = x;
						}
					}
				}
			}
			return ret;
		}
	}

	public static final TimeInterval min(TimeInterval... a) {
		if (a==null) {
			return null;
		} else {
			TimeInterval ret = null;
			for (TimeInterval x:a) {
				if (x!=null) {
					if (ret==null) {
						ret = x;
					} else {
						if (x.interval<ret.interval) {
							ret = x;
						}
					}
				}
			}
			return ret;
		}
	}

	public long nanoseconds() {
		return interval*1_000_000;
	}

	public Duration toDuration() {
		return Duration.ofMillis(interval);
	}

	public static final TimeInterval of(Duration d) {
		return new TimeInterval(d.toMillis());
	}
}
