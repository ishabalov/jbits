package org.jbits.time;

import java.text.DecimalFormat;
import java.text.FieldPosition;
import java.text.Format;
import java.text.ParsePosition;
import java.time.Duration;
import java.util.StringTokenizer;

public class TimeIntervalFormat extends Format {

	private static final long serialVersionUID = -2604045854413578823L;

	public static final TimeIntervalFormat INSTANCE = new TimeIntervalFormat();
	public static final TimeIntervalFormat INSTANCE_SHORT = getInstance(2,0);
	public static final TimeIntervalFormat INSTANCE_MEDIUM = getInstance(4,0);

	
	public static final TimeIntervalFormat getDefaultInstance() {
		return INSTANCE;
	}

	public static final TimeIntervalFormat getInstance(int numberOfFields, int numberOfMillisecondsDigits) {
		TimeIntervalFormat ret = new TimeIntervalFormat();
		ret.numberOfFields=numberOfFields;
		DecimalFormat fmt = new DecimalFormat();
		fmt.setGroupingUsed(false);
		fmt.setMaximumFractionDigits(numberOfMillisecondsDigits);
		ret.secondsFormatter = fmt;
		return ret;
	}
	
	public static final String format(Duration d) {
		return INSTANCE.format(TimeInterval.of(d));
	}
	public static final String formatShort(Duration d) {
		return INSTANCE_SHORT.format(TimeInterval.of(d));
	}
	public static final String formatMedium(Duration d) {
		return INSTANCE_MEDIUM.format(TimeInterval.of(d));
	}
	
	private TimeIntervalFormat(){};
	
	private int numberOfFields = Integer.MAX_VALUE;
	
	private Format secondsFormatter = null;
	
	@Override
	public StringBuffer format(Object obj, StringBuffer toAppendTo, FieldPosition pos) {
		int fields = numberOfFields;
		long input;
		if (obj instanceof TimeInterval) {
			input = ((TimeInterval)obj).milliseconds();
		} else {
			input = (Long) obj;
		}
		if (input<0) {
			input=-input;
			toAppendTo.append('-');
		}
		if (hasField(input, TimeInterval.DAY)) {
			input = appendField(input, 'd', TimeInterval.DAY, toAppendTo);
			fields--;
			if (input > 0) {
				toAppendTo.append(' ');
			}
		}
		if (fields<=0) {
			return toAppendTo;
		}
		if (hasField(input, TimeInterval.HOUR)) {
			input = appendField(input, 'h', TimeInterval.HOUR, toAppendTo);
			fields--;
			if (input > 0) {
				toAppendTo.append(' ');
			}
		}
		if (fields<=0) {
			return toAppendTo;
		}
		if (hasField(input, TimeInterval.MIN)) {
			input = appendField(input, 'm', TimeInterval.MIN, toAppendTo);
			fields--;
			if (input > 0) {
				toAppendTo.append(' ');
			}
		}
		if (fields<=0) {
			return toAppendTo;
		}
		float sec = (float)input / (float)TimeInterval.SEC;
		if (secondsFormatter!=null) {
			toAppendTo.append(secondsFormatter.format(sec));
		} else {
			toAppendTo.append(sec);
		}
		toAppendTo.append('s');
		return toAppendTo;
	}

	private long appendField(long input, char chr, long unit, StringBuffer buf) {
		long val = input / unit;
		long ret = input % unit;
		if (val > 0) {
			buf.append(val);
			buf.append(chr);
		}
		return ret;
	}

	private boolean hasField(long input, long unit) {
		long val = input / unit;
		return val > 0;
	}

	@Override
	public Object parseObject(String source, ParsePosition pos) {
		long ret = 0L;
		int startingPos = 0;
		if (pos != null) {
			startingPos = pos.getIndex();
			pos.setIndex(source.length());
		}
		StringTokenizer t = new StringTokenizer(source.toLowerCase().substring(startingPos), " :,+");
		while (t.hasMoreTokens()) {
			String s = t.nextToken();
			if (s != null) {
				if (isValidDef(s)) {
					ret = ret + parseDef(s);
				} else if (t.hasMoreTokens()) {
					String ss = t.nextToken();
					ret = ret + parseDef(s, ss);
				}
			}
		}
		return ret;
	}

	private static final class IntervalData {
		protected String longDef;
		protected String shortDef;
		protected long value;

		protected IntervalData(String shortDef, String longDef, long value) {
			this.longDef = longDef;
			this.shortDef = shortDef;
			this.value = value;
		}
	}

	private static IntervalData intervalsTable[] = { new IntervalData("ms", "msec", TimeInterval.MSEC),
		new IntervalData("ms", "millisecond", TimeInterval.MSEC), new IntervalData("s", "sec", TimeInterval.SEC),
		new IntervalData("m", "min", TimeInterval.MIN), new IntervalData("h", "hour", TimeInterval.HOUR),
		new IntervalData("d", "day", TimeInterval.DAY), new IntervalData("w", "week", TimeInterval.WEEK), };

	private static boolean isValidNumber(String def) {
		for (char c : def.toCharArray()) {
			if (!Character.isDigit(c) && '.' != c) {
				return false;
			}
		}
		return true;
	}

	public static boolean isValidDef(String def) {
		if (def != null) {
			for (IntervalData t : intervalsTable) {
				if (def.endsWith(t.shortDef)) {
					def = def.substring(0, def.length() - t.shortDef.length());
					return isValidNumber(def);
				}
			}
		}
		return false;
	}

	public static boolean isValidDef(String def, String param) {
		if (def != null && param != null && isValidNumber(def)) {
			for (IntervalData t : intervalsTable) {
				if (param.startsWith(t.longDef) || param.startsWith(t.shortDef)) {
					return true;
				}
			}
		}
		return false;
	}

	public static long parseDef(String def) {
		assert def != null;
		for (IntervalData t : intervalsTable) {
			if (def.endsWith(t.shortDef)) {
				def = def.substring(0, def.length() - t.shortDef.length());
				float val = Float.parseFloat(def);
				val = val * t.value;
				return Math.round(val);
			}
		}
		return 0L;
	}

	public static long parseDef(String value, String timeunit) {
		for (IntervalData t : intervalsTable) {
			if (timeunit.startsWith(t.longDef) || timeunit.startsWith(t.shortDef)) {
				double val = Double.parseDouble(value);
				val = val * t.value;
				return Math.round(val);
			}
		}
		throw new IllegalStateException("Unrecognized time unit:" + timeunit);
	}

}
